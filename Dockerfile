FROM openjdk:11.0.10-jre-buster

COPY target/manifold-*.jar ./manifold.jar

ENTRYPOINT ["java", "-jar", "manifold.jar"]