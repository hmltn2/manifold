# Package org.crossref.manifold.api

REST APIs that expose kernel functionality, such as accessing the Item Graph directly. Most APIs will be provided by
modules, not the Kernel.