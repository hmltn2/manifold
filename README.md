# Crossref Manifold

<img src="doc/graphics/celebration.png" width="200px" alt="Manifold logo" align="right">

- Comprehensive [Developer Docs](https://crossref.gitlab.io/manifold/) — read these first!
- [SONAR Code Quality](https://sonarcloud.io/dashboard?id=org.crossref%3Amanifold)

Crossref Manifold is the API where research objects meet. It keeps track of metadata and connections between research
objects. It accepts data from Crossref members, Crossref Agents, and other members of the scholarly community and brings
them together in one single research graph.

Crossref Manifold combines data from a range of sources and makes it available in a range of formats. It will power a
number of future APIs, such as the REST API, Content Negotiation, Funder Registry, Cited-By, Event Data and more
besides.

The Crossref Manifold is made up of a Kernel layer, which handles Item Trees, Item Graph, Relationship types, internal
Events. The Kernel also provides REST APIs which use this language.

Product features are implemented as Modules. These are responsible for translating data formats and vocabularies in and
out of the Kernel, reporting, etc.

**See the [Developer Docs](https://crossref.gitlab.io/manifold/) for loads more info.**

## How to run

This is a Spring Boot application. You can run it in IntelliJ, or at the command line with:

```shell
mvn spring-boot:run
```

Various configuration options are available in the Kernel and the various modules. They include:

## Kernel

- `DB_URI` - MySQL URL

## Packages

- `DUMP_SQL`  - Dump SQL database on shutdown to this URL.
- `UNIXML_INGEST_SNAPSHOT` - Ingest UNIXML .tar.gz snapshot

## Metrics

The application collects and exposes open metrics at http://localhost:8080/actuator/prometheus/.

A docker-compose setup is included for running Prometheus and Grafana for easy access to the metrics in development. To
run it use:

```shell
docker-compose up -d
```

- Prometheus: http://localhost:4490
- Grafana: http://localhost:4491
    - default user: admin
    - default pass: admin

## Tests

The project includes unit and integration tests under `src/test/kotlin` and `src/test-integration/kotlin` respectively.
Right-clicking on any of these directories provides the option to run the included tests. Integration tests need an initial setup though.

### Unit tests

To execute the unit tests we can run the maven test goal from within our IDE or as `mvn test` in the CLI.

Alternatively we can right-click on `src/test/kotlin` and select Run 'Tests in kotlin'.

There are not further dependencies for running the unit tests.

### Integration tests

Integration tests require a set of services to be present in the development environment. The [Integration](https://gitlab.com/crossref/integration) project can take care of that.

Using the Integration project one can set up a fully working environment for integration testing:

```shell
git clone https://gitlab.com/crossref/integration.git
cd integration
./integration up -var-file manifold-ci.tfvars
```

To run the tests we need to provide an appropriate DB_URI env var value.
For now the integration tests are not configured to run as maven goals in the pom file, so they will not run with maven verify through the IDE.

We can still right-click on `src/test-integration/kotlin` and select Run 'Tests in kotlin' though.
In that case make sure you have configured your DB_URI env var.

To run the integration tests in the CLI we can use:

```shell
DB_URI="jdbc:postgresql://localhost:5432/nexus?user=nexus&password=nexus" mvn test-compile failsafe:integration-test failsafe:verify
```
