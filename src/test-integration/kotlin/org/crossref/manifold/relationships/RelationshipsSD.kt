package org.crossref.manifold.relationships

import com.fasterxml.jackson.databind.ObjectMapper
import io.cucumber.java.en.Then
import io.cucumber.java.en.When
import io.cucumber.spring.CucumberContextConfiguration
import org.crossref.manifold.api.relationship.RelationshipsResponse
import org.crossref.manifold.common.ApiResponseContext
import org.crossref.manifold.common.Statements
import org.crossref.manifold.db.Constants
import org.crossref.manifold.itemgraph.Configuration.MAX_RETURNED_ROWS
import org.junit.jupiter.api.Assertions
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.annotation.Import
import org.springframework.http.MediaType
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.TestPropertySource
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultHandlers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@CucumberContextConfiguration
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
@TestPropertySource(
    properties = [
        "${MAX_RETURNED_ROWS}=5",
        "${Constants.BUFFER}.${Constants.RELATIONSHIP_ASSERTION}=true",
        "${Constants.BUFFER}.${Constants.PROPERTY_ASSERTION}=true",
        "${Constants.BUFFER}.${Constants.RELATIONSHIP_ASSERTION}.${org.crossref.manifold.util.Constants.INITIAL_DELAY}=0",
        "${Constants.BUFFER}.${Constants.RELATIONSHIP_ASSERTION}.${org.crossref.manifold.util.Constants.FIXED_DELAY}=1",
        "${Constants.BUFFER}.${Constants.PROPERTY_ASSERTION}.${org.crossref.manifold.util.Constants.INITIAL_DELAY}=0",
        "${Constants.BUFFER}.${Constants.PROPERTY_ASSERTION}.${org.crossref.manifold.util.Constants.FIXED_DELAY}=1",
    ])
@Import(ApiResponseContext.Configuration::class)
class RelationshipsSD(private val apiResponseContext: ApiResponseContext, private val mapper: ObjectMapper) {
    @When("all relationships are requested")
    fun all_relationships_are_requested() {
        apiResponseContext.perform(get("/v1/relationships/"))
    }

    @When("relationships for item {string} are requested")
    fun relationships_for_item_are_requested(itemId: String) {
        relationships_for_are_requested("item", itemId)
    }

    @When("relationships for subject {string} are requested")
    fun relationships_for_subject_are_requested(subjectId: String) {
        relationships_for_are_requested("subject", subjectId)
    }

    @When("relationships for object {string} are requested")
    fun relationships_for_object_are_requested(objectId: String) {
        relationships_for_are_requested("object", objectId)
    }

    @When("{int} relationships for item {string} are requested")
    fun relationships_for_item_are_requested(rows: Int, itemId: String) {
        apiResponseContext.perform(get("/v1/relationships/").param("item-id", itemId).param("rows", rows.toString()))
    }

    @When("the first {int} relationships for item {string} are requested")
    fun the_first_relationships_for_item_are_requested(rows: Int, itemId: String) {
        apiResponseContext.perform(
            get("/v1/relationships/").param("item-id", itemId).param("rows", rows.toString())
                .param("cursor", "*")
        )
    }

    @When("the next {int} relationships for item {string} are requested")
    fun the_next_relationships_for_item_are_requested(rows: Int, itemId: String) {
        val nextCursor = readRelationshipsResponse().message.nextCursor
        apiResponseContext.perform(
            get("/v1/relationships/").param("item-id", itemId).param("rows", rows.toString())
                .param("cursor", nextCursor)
        )
    }

    @When("relationships for relationship type {string} are requested")
    fun relationships_for_relationship_type_are_requested(relationshipType: String) {
        apiResponseContext.perform(get("/v1/relationships/").param("relationship-type", relationshipType))
    }

    @Then("the following relationships are returned from the item graph")
    fun the_following_relationships_are_returned_from_the_item_graph(statements: List<Statements.RelationshipStatement>) {
        apiResponseContext.andDo(MockMvcResultHandlers.print()).andExpect(status().isOk).andExpect(
            content().contentType(MediaType.APPLICATION_JSON)
        )
        val relationshipsResponse = readRelationshipsResponse()
        Assertions.assertEquals(statements.size, relationshipsResponse.message.relationships.size)
        statements.forEach {
            Assertions.assertTrue(
                relationshipsResponseContainsStatementOnce(
                    relationshipsResponse,
                    it
                )
            )
        }
    }

    @Then("{int} relationships are returned from the item graph")
    fun relationships_are_returned_from_the_item_graph(rows: Int) {
        apiResponseContext.andDo(MockMvcResultHandlers.print()).andExpect(status().isOk).andExpect(
            content().contentType(MediaType.APPLICATION_JSON)
        )
        val relationshipsResponse = readRelationshipsResponse()
        Assertions.assertTrue(relationshipsResponse.message.relationships.size == rows)
    }

    @Then("HTTP status {string} is returned")
    fun http_status_is_returned(httpStatus: String) {
        apiResponseContext.andDo(MockMvcResultHandlers.print()).andExpect(status().isOk).andExpect(
            content().contentType(MediaType.APPLICATION_JSON)
        )
        val relationshipsResponse = readRelationshipsResponse()
        Assertions.assertTrue(relationshipsResponse.status.name == httpStatus)
    }

    @Then("total row count {int} is returned")
    fun total_row_count_is_returned(totalCount: Int) {
        apiResponseContext.andDo(MockMvcResultHandlers.print()).andExpect(status().isOk).andExpect(
            content().contentType(MediaType.APPLICATION_JSON)
        )
        val relationshipsResponse = readRelationshipsResponse()
        Assertions.assertTrue(relationshipsResponse.message.totalResults == totalCount)
    }

    @Then("message type {string} is returned")
    fun message_type_is_returned(messageType: String) {
        apiResponseContext.andDo(MockMvcResultHandlers.print()).andExpect(status().isOk).andExpect(
            content().contentType(MediaType.APPLICATION_JSON)
        )
        val relationshipsResponse = readRelationshipsResponse()
        Assertions.assertTrue(relationshipsResponse.messageType.getLabel() == messageType)
    }

    private fun readRelationshipsResponse(): RelationshipsResponse =
        mapper.readValue(apiResponseContext.andReturn().response.contentAsString, RelationshipsResponse::class.java)

    private fun relationshipsResponseContainsStatementOnce(
        relationshipsResponse: RelationshipsResponse,
        relationshipStatement: Statements.RelationshipStatement,
    ): Boolean =
        relationshipsResponse.message.relationships.count {
            it.fromItem.identifiers.map { itemIdentifierView -> itemIdentifierView.identifier }
                .contains(relationshipStatement.subjectId)
                    && it.toItem.identifiers.map { itemIdentifierView -> itemIdentifierView.identifier }
                .contains(relationshipStatement.objectId)
                    && it.relationshipType == relationshipStatement.relationshipType
        } == 1

    private fun relationships_for_are_requested(identifierType: String, identifierId: String) {
        apiResponseContext.perform(get("/v1/relationships/").param("$identifierType-id", identifierId))
    }

}
