package org.crossref.manifold.relationships

import io.cucumber.junit.platform.engine.Constants
import org.junit.platform.suite.api.ConfigurationParameter
import org.junit.platform.suite.api.SelectClasspathResource
import org.junit.platform.suite.api.Suite

@Suite
@SelectClasspathResource("features/relationships")
@ConfigurationParameter(
    key = Constants.GLUE_PROPERTY_NAME,
    value = "org.crossref.manifold.relationships"
            + ",org.crossref.manifold.common"
)
class RunRelationshipsTestsIT
