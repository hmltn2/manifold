package org.crossref.manifold.common

import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.context.annotation.Bean
import org.springframework.test.web.servlet.*

class ApiResponseContext(private val mockMvc: MockMvc) : ResultActions {
    @AutoConfigureMockMvc
    class Configuration {
        @Bean
        fun apiResponseContext(mockMvc: MockMvc) = ApiResponseContext(mockMvc)
    }

    private lateinit var currentResponse: ResultActions

    override fun andExpect(matcher: ResultMatcher): ResultActions = currentResponse.andExpect(matcher)

    override fun andDo(handler: ResultHandler): ResultActions = currentResponse.andDo(handler)

    override fun andReturn(): MvcResult = currentResponse.andReturn()

    fun perform(requestBuilder: RequestBuilder): ResultActions {
        currentResponse = mockMvc.perform(requestBuilder)
        return currentResponse
    }
}
