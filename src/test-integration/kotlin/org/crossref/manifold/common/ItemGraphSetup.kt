package org.crossref.manifold.common

import io.cucumber.java.After
import io.cucumber.java.Before
import io.cucumber.java.en.Given
import io.cucumber.java.en.Then
import org.crossref.manifold.ingestion.Envelope
import org.crossref.manifold.ingestion.EnvelopeBatch
import org.crossref.manifold.ingestion.EnvelopeBatchProvenance
import org.crossref.manifold.itemgraph.ItemGraph
import org.crossref.manifold.itemgraph.ItemTreeAssertion
import org.crossref.manifold.itemgraph.MergeStrategy
import org.crossref.manifold.itemgraph.Resolver
import org.crossref.manifold.itemtree.Identifier
import org.crossref.manifold.itemtree.Item
import org.crossref.manifold.itemtree.Relationship
import org.crossref.manifold.itemtree.logger
import org.junit.Assert
import org.junit.jupiter.api.Assertions
import org.springframework.jdbc.core.JdbcTemplate

class ItemGraphSetup(
    private val itemGraph: ItemGraph,
    private val jdbcTemplate: JdbcTemplate,
    private val resolver: Resolver
) {
    companion object {
        private val PROVENANCE = EnvelopeBatchProvenance("Crossref Test", "1234")
    }

    // By convention, scenarios start with an empty item graph.
    @Before
    fun truncateItemGraphTables() {
        jdbcTemplate.execute("TRUNCATE TABLE item_identifier RESTART IDENTITY CASCADE")
        jdbcTemplate.execute("TRUNCATE TABLE envelope_batch RESTART IDENTITY CASCADE")
        jdbcTemplate.execute("TRUNCATE TABLE item RESTART IDENTITY CASCADE")
        jdbcTemplate.execute("TRUNCATE TABLE item_tree_reachability RESTART IDENTITY CASCADE")
        jdbcTemplate.execute("TRUNCATE TABLE relationship_assertion_buffer RESTART IDENTITY CASCADE")
        jdbcTemplate.execute("TRUNCATE TABLE property_assertion_buffer RESTART IDENTITY CASCADE")
    }

    /**
     * Make sure that there are no PostgreSQL stored functions running in the background between tests.
     */
    @After
    fun waitForBuffer() {
        waitForBufferToStopRunning()
    }

    @Given("the following relationship assertions in the item graph")
    fun the_following_relationship_assertions_in_the_item_graph(statements: List<Statements.RelationshipAssertionStatement>) {
        val envelopes = statements.map { statement ->
            val item = Item()
                .withIdentifier(Identifier(statement.subjectId))
                .withRelationship(
                    Relationship(
                        statement.relationshipType,
                        Item().withIdentifier(Identifier(statement.objectId))
                    )
                )

            val assertion = ItemTreeAssertion(
                statement.assertedAt,
                MergeStrategy.NAIVE,
                Item().withIdentifier(Identifier(statement.assertedBy))
            )

            Envelope(listOf(item), assertion)
        }

        itemGraph.assert(
            EnvelopeBatch(
                envelopes, PROVENANCE
            )
        )

        waitForRABuffer()
    }

    @Given("the following property assertions in the item graph")
    fun the_following_property_assertions_in_the_item_graph(statements: List<Statements.PropertyAssertionStatement>) {

        val envelopes = statements.map { statement ->
            val item = Item()
                .withIdentifier(Identifier(statement.subjectId))
                .withPropertiesFromMap(mapOf(statement.property to statement.value))

            val assertion = ItemTreeAssertion(
                statement.assertedAt,
                MergeStrategy.NAIVE,
                Item().withIdentifier(Identifier(statement.assertedBy))
            )

            Envelope(listOf(item), assertion)
        }

        itemGraph.assert(
            EnvelopeBatch(
                envelopes, PROVENANCE
            )
        )

        waitForPABuffer()
    }

    /**
     * This one specifically asserts all the property values in one assertion.
     */
    @Given("the following multi-valued property assertion in the item graph")
    fun the_following_multi_property_assertions_in_the_item_graph(statements: List<Statements.PropertyAssertionStatement>) {

        Assert.assertTrue(
            "All statements must be asserted by the same party",
            statements.map { it.assertedBy }.toSet().count() == 1
        )

        Assert.assertTrue(
            "All subject ids should be the same",
            statements.map { it.subjectId }.toSet().count() == 1
        )

        val assertedBy = statements.first().assertedBy
        val subjectId = statements.first().subjectId
        val assertedAt = statements.first().assertedAt

        val allProperties = statements.map { it.property to it.value }.toMap()
        val item = Item()
            .withIdentifier(Identifier(subjectId))
            .withPropertiesFromMap(allProperties)

        val assertion =
            ItemTreeAssertion(assertedAt, MergeStrategy.NAIVE, Item().withIdentifier(Identifier(assertedBy)))

        itemGraph.assert(
            EnvelopeBatch(
                listOf(Envelope(listOf(item), assertion)), PROVENANCE
            )
        )

        waitForPABuffer()
    }


    /**
     * This Given doesn't do anything other than serve as a placeholder to make tests clear when there is no prior data
     * in the item graph.
     * The clearing is done by [truncateItemGraphTables].
     */
    @Given("an empty Item Graph database")
    fun empty_item_graph() = Unit


    @Then("the following relationship statements are found in the item graph")
    fun the_item_graph_contains_current_relationship_statements(
        expectedStatements: List<Statements.RelationshipAssertionStatement>
    ) {
        expectedStatements.forEach {
            val foundStatements = itemGraph.getRelationshipStatements(
                Identifier(it.subjectId),
                it.relationshipType,
                Identifier(it.objectId),
                Identifier(it.assertedBy),
                current = it.current,
                state = true
            )

            Assertions.assertEquals(it.count, foundStatements.count())
        }
    }

    @Then("the following property statements are found in the item graph")
    fun the_item_graph_contains_current_property_statements(
        expectedStatements: List<Statements.PropertyAssertionStatement>
    ) {
        expectedStatements.forEach { statement ->
            val foundStatements = itemGraph.getPropertyStatements(
                Identifier(statement.subjectId),
                Identifier(statement.assertedBy),
                statement.current
            )
            val matching = foundStatements.filter {
                it.values.get(statement.property)?.asText() == statement.value
            }

            Assertions.assertEquals(statement.count, matching.count())
        }
    }

    @Given("the following Items in the Item Graph")
    fun the_following_items_are_in_the_item_graph(items: List<Statements.Item>) {
        items.forEach { item ->
            val resolvedItem = resolver.resolveRO(Identifier(item.identifier))
            Assertions.assertNotNull(resolvedItem, "Expected to resolved item ${item}")
            val createdItem = resolver.resolveRW(resolvedItem)
            Assertions.assertNotNull(createdItem, "Expected to find item with identifier ${item}")
        }
    }

    fun waitForRABuffer() {
        // Wait for the relationship assertion buffer to empty.
        waitForBuffer("property_assertion_buffer")
    }

    fun waitForPABuffer() {
        // Wait for the property assertion buffer to empty.
        waitForBuffer("relationship_assertion_buffer")
    }

    private fun waitForBuffer(bufferTable: String) {
        var remainingBufferSize: Int
        var retries = 0
        do {
            Thread.sleep(1000)
            remainingBufferSize =
                jdbcTemplate.queryForObject("SELECT COUNT(*) FROM $bufferTable", Int::class.java)!!
        } while (remainingBufferSize > 0 && ++retries < 60)
        Assertions.assertEquals(0, remainingBufferSize, "Expected buffer to be empty")
    }

    /**
     * Wait until all current buffer processing functions exit.
     */
    fun waitForBufferToStopRunning() {
        var numRunning: Int
        var retries = 0
        do {
            Thread.sleep(1000)
            numRunning =
                jdbcTemplate.queryForObject("SELECT COUNT(*)\n" +
                        "FROM pg_stat_activity\n" +
                        "WHERE state = 'active'\n" +
                        "AND query LIKE 'SELECT * FROM process_buffered%';", Int::class.java)!!
        } while (numRunning > 0 && ++retries < 30)
        Assertions.assertEquals(0, numRunning, "Expected no process_buffered to be running")
    }
}
