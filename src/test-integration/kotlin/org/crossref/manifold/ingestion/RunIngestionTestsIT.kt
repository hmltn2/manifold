package org.crossref.manifold.ingestion

import io.cucumber.junit.platform.engine.Constants
import org.junit.platform.suite.api.ConfigurationParameter
import org.junit.platform.suite.api.SelectClasspathResource
import org.junit.platform.suite.api.Suite

@Suite
@SelectClasspathResource("features/ingestion")
@ConfigurationParameter(
    key = Constants.GLUE_PROPERTY_NAME,
    value = "org.crossref.manifold.ingestion" +
            ",org.crossref.manifold.common"
)
class RunIngestionTestsIT
