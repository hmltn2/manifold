package org.crossref.manifold.ingestion

import io.cucumber.java.Before
import io.cucumber.java.en.Given
import io.cucumber.java.en.When
import io.cucumber.spring.CucumberContextConfiguration
import org.crossref.manifold.bulk.BulkIngestionService
import org.crossref.manifold.common.ItemGraphSetup
import org.crossref.manifold.db.Constants
import org.crossref.manifold.itemgraph.ItemTreeAssertion
import org.crossref.manifold.itemgraph.MergeStrategy
import org.crossref.manifold.itemtree.Identifier
import org.crossref.manifold.itemtree.Item
import org.crossref.manifold.itemtree.Relationship
import org.crossref.manifold.rendering.Configuration
import org.junit.jupiter.api.TestInstance
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.TestPropertySource
import java.io.File
import java.time.OffsetDateTime


@CucumberContextConfiguration
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestPropertySource(
    properties = [
        "${Constants.BUFFER}.${Constants.RELATIONSHIP_ASSERTION}=true",
        "${Constants.BUFFER}.${Constants.PROPERTY_ASSERTION}=true",
        "${Constants.BUFFER}.${Constants.RELATIONSHIP_ASSERTION}.${org.crossref.manifold.util.Constants.INITIAL_DELAY}=0",
        "${Constants.BUFFER}.${Constants.RELATIONSHIP_ASSERTION}.${org.crossref.manifold.util.Constants.FIXED_DELAY}=1",
        "${Constants.BUFFER}.${Constants.PROPERTY_ASSERTION}.${org.crossref.manifold.util.Constants.INITIAL_DELAY}=0",
        "${Constants.BUFFER}.${Constants.PROPERTY_ASSERTION}.${org.crossref.manifold.util.Constants.FIXED_DELAY}=1",
    ])
class EnvelopeBatchIngestionSD(
    private val itemGraphSetup: ItemGraphSetup,
    ) {
    @Autowired
    lateinit var ingester: BulkIngestionService

    val logger: Logger = LoggerFactory.getLogger(this::class.java)

    // This is used for constructing entries in a bulk file.
    var envelopesInBatch = mutableListOf<EnvelopeBatch>()

    var currentFile: File? = null

    @Autowired
    lateinit var tempFiles: TempFile

    @Given("a bulk import file called {string}")
    fun a_bulk_import_file(name: String) {
        // This step gets a bulk file ready.
        envelopesInBatch.clear()
        currentFile = tempFiles.get(name)
    }

    @Given("{string} asserts relationship {string} {string} {string} in the bulk file")
    fun the_bulk_file_asserts_relationship(
        assertedBy: String,
        subj: String,
        relationship: String,
        obj: String
    ) {

        val envelope = Envelope(
            listOf(
                Item().withIdentifier(Identifier(subj))
                    .withRelationship(
                        Relationship(
                            relationship, Item()
                                .withIdentifier(Identifier(obj))
                        )
                    )
            ),
            ItemTreeAssertion(
                OffsetDateTime.now(), MergeStrategy.NAIVE,
                Item().withIdentifier(Identifier(assertedBy))
            )
        )


        val envelopeBatch = EnvelopeBatch(
            listOf(envelope),
            EnvelopeBatchProvenance("Crossref Test", "1234")
        )

        envelopesInBatch.add(envelopeBatch)
    }

    @Given("{string} asserts property {string} {string}: {string} in the bulk file")
    fun the_bulk_file_asserts_property(
        assertedBy: String, subj: String,
        property: String,
        value: String,
    ) {

        val envelope = Envelope(
            listOf(
                Item().withIdentifier(Identifier(subj))
                    .withPropertiesFromMap(mapOf(property to value))
            ),
            ItemTreeAssertion(
                OffsetDateTime.now(),
                MergeStrategy.NAIVE,
                Item().withIdentifier(Identifier(assertedBy))
            )
        )


        val envelopeBatch = EnvelopeBatch(
            listOf(envelope),
            EnvelopeBatchProvenance("Crossref Test", "1234")
        )

        envelopesInBatch.add(envelopeBatch)
    }

    /**
     * Write the bulk file we've been accumulating.
     */
    fun writeBulkFile(file: File) {
        EnvelopeBatchArchiveWriter(file).use {
            envelopesInBatch.forEachIndexed { i, envelopeBatch ->
                it.add("TEST-$i.XML", listOf(envelopeBatch))
            }
        }

        logger.info("Written temporary bulk XML file: ${file.absolutePath} ${file.name}")
    }

    @When("the bulk import is triggered for {string}")
    fun the_bulk_import_is_triggered(filename: String) {
        val file = tempFiles[filename]
        writeBulkFile(file)
        ingester.ingestFiles(listOf(file))

        itemGraphSetup.waitForRABuffer()
    }
}