package org.crossref.manifold.rendering

import com.fasterxml.jackson.dataformat.xml.XmlMapper
import io.cucumber.java.Before
import io.cucumber.java.en.Given
import io.cucumber.java.en.Then
import io.cucumber.java.en.When
import io.cucumber.spring.CucumberContextConfiguration
import org.crossref.manifold.api.Feed
import org.crossref.manifold.common.ApiResponseContext
import org.crossref.manifold.common.ItemGraphSD
import org.crossref.manifold.common.ItemGraphSetup
import org.crossref.manifold.common.Statements
import org.crossref.manifold.db.Constants.BUFFER
import org.crossref.manifold.db.Constants.PROPERTY_ASSERTION
import org.crossref.manifold.db.Constants.RELATIONSHIP_ASSERTION
import org.crossref.manifold.ingestion.Envelope
import org.crossref.manifold.ingestion.EnvelopeBatch
import org.crossref.manifold.ingestion.EnvelopeBatchProvenance
import org.crossref.manifold.itemgraph.Collections.Companion.COLLECTION_RELATIONSHIP_TYPE
import org.crossref.manifold.itemgraph.ItemGraph
import org.crossref.manifold.itemgraph.ItemTreeAssertion
import org.crossref.manifold.itemgraph.MergeStrategy
import org.crossref.manifold.itemgraph.Resolver
import org.crossref.manifold.itemtree.*
import org.crossref.manifold.itemtree.Properties
import org.crossref.manifold.rendering.Configuration.RENDERING
import org.crossref.manifold.rendering.Configuration.STALE_QUEUE_PUSH
import org.crossref.manifold.util.Constants.FIXED_DELAY
import org.crossref.manifold.util.Constants.INITIAL_DELAY
import org.crossref.manifold.util.logDuration
import org.json.JSONObject
import org.junit.jupiter.api.Assertions
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.annotation.Import
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.TestPropertySource
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultHandlers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import software.amazon.awssdk.services.sqs.SqsAsyncClient
import software.amazon.awssdk.services.sqs.model.GetQueueAttributesRequest
import software.amazon.awssdk.services.sqs.model.GetQueueUrlRequest
import software.amazon.awssdk.services.sqs.model.QueueAttributeName
import java.net.URI
import java.net.URLDecoder
import java.net.URLEncoder
import java.nio.charset.StandardCharsets
import java.time.OffsetDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.*

@CucumberContextConfiguration
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
@TestPropertySource(
    properties = [
        "$BUFFER.$RELATIONSHIP_ASSERTION=true",
        "$BUFFER.$PROPERTY_ASSERTION=true",
        "$BUFFER.$RELATIONSHIP_ASSERTION.$INITIAL_DELAY=0",
        "$BUFFER.$RELATIONSHIP_ASSERTION.$FIXED_DELAY=1",
        "$BUFFER.$PROPERTY_ASSERTION.$INITIAL_DELAY=0",
        "$BUFFER.$PROPERTY_ASSERTION.$FIXED_DELAY=1",
        // Give us a chance to truncate the tables before the stale flags are picked up.
        "$RENDERING.$STALE_QUEUE_PUSH.$INITIAL_DELAY=5",
        // Stale loop should run faster than production as we've got smaller volumes of data, and we wait for it.
        // In [reset] we wait for this to complete.
        "$RENDERING.$STALE_QUEUE_PUSH.$FIXED_DELAY=1"
    ]
)
@Import(ApiResponseContext.Configuration::class)
class RenderingSD(
    @Autowired
    val renderStatusDao: RenderStatusDao,

    @Autowired
    val renderedItemDao: RenderedItemDao,

    @Autowired
    val renderedItemStorageDao: RenderedItemStorageDao,

    @Autowired
    val resolver: Resolver,

    @Autowired
    val jdbcTemplate: JdbcTemplate,

    @Autowired
    val itemGraph: ItemGraph,

    @Autowired
    val sqsClient: SqsAsyncClient,

    @Value("\${$RENDERING.$STALE_QUEUE_PUSH}") val renderQueue: String,

    @Autowired
    val itemTreeRenderer: ItemTreeRenderer,

    val itemGraphSetup: ItemGraphSetup,

    private val apiResponseContext: ApiResponseContext,
) {

    val logger: Logger = LoggerFactory.getLogger(this::class.java)
    private val npTemplate: NamedParameterJdbcTemplate = NamedParameterJdbcTemplate(jdbcTemplate)
    val mapper = XmlMapper()

    @Before
    fun beforeEach() {
        resetRendering()
    }

    /**
     * Reset the rendering system.
     * No items will be marked stale, pending render, or rendered.
     * The item tree reachability index isn't touched.
     */
    fun resetRendering() {
        // Remove all render flags, including anything stale.
        // This will avoid any extra items being added to the queue.
        jdbcTemplate.update("TRUNCATE item_render_status RESTART IDENTITY CASCADE")

        // Wait for queue to report empty.
        waitForQueueToEmpty()

        // Remove all rendered items.
        jdbcTemplate.update("TRUNCATE rendered_item RESTART IDENTITY CASCADE")

        // And any stale flags that were set by a lingering render loop.
        jdbcTemplate.update("TRUNCATE item_render_status RESTART IDENTITY CASCADE")
    }

    fun getQueueLength(): Int? =
         sqsClient.getQueueAttributes(
            GetQueueAttributesRequest.builder()
                .queueUrl(
                    sqsClient.getQueueUrl(
                        GetQueueUrlRequest.builder().queueName(renderQueue).build(),
                    ).get().queueUrl())
                .attributeNames(QueueAttributeName.APPROXIMATE_NUMBER_OF_MESSAGES).build())
            .join()
            .attributes()[QueueAttributeName.APPROXIMATE_NUMBER_OF_MESSAGES]
            ?.toInt()

    fun waitForQueueToEmpty() {
        var queueLength: Int?
        var retries = 0
        do {
            queueLength = getQueueLength()
            logger.info("Waiting for render queue to emtpy. Size: $queueLength")
            if (queueLength != 0) {
                Thread.sleep(1000)
            }
        } while (queueLength != 0 && ++retries < 30)
        Assertions.assertEquals(0, queueLength, "Timed out waiting for queue length to be 0. Was: $queueLength")
    }

    /**
     * Tries to resolve an item by its identifier or primary key, and
     * asserts that the item has been resolved
     * @return the resolved [Item]
     */
    fun resolveAndCheckItem(identifier: String): Item {
        val resolvedItem: Item = identifier.toLongOrNull()?.let {
            resolver.resolveRO(Item(pk = identifier.toLong()))
        } ?: resolver.resolveRO(Identifier(identifier))
        Assertions.assertNotNull(resolvedItem, "Expected to find item with identifier $identifier")
        Assertions.assertNotNull(resolvedItem.pk, "Expected to find pk for $identifier")
        return resolvedItem
    }

    /**
     * Tries to resolve a string representation of [ContentType], and
     * asserts that the the [contentType] has been resolved
     * @return the resolved Item
     */
    fun resolveAndCheckContentType(contentType: String): ContentType {
        val resolvedContentType = ContentType.fromMimeType(contentType)
        Assertions.assertNotNull(
            resolvedContentType!!,
            "Expected to be able to resolve $contentType value to ContentType enum"
        )
        return resolvedContentType
    }

    /**
     * Continually tried to find a RenderedItem for the @param[item] and @param[contentType]
     * supplied, tries 30 times, each time waiting 1 second, and then gives up.
     */
    fun waitAndCheckForRenderedItems(item: Item, contentType: ContentType): Collection<RenderedItem> {
        var renderedItems: Collection<RenderedItem> = emptyList()
        var retries = 0

        do {
            logger.info("Searching for rendered content type for Item ${item.pk}...")
            renderedItems = renderedItemDao.getHistoricalRenderedItems(item.pk!!, contentType.mimeType)
            if (renderedItems.isEmpty()) {
                Thread.sleep(1000)
            }
        } while (renderedItems.isEmpty() && ++retries < 30)


        Assertions.assertFalse(renderedItems.isEmpty(), "Expected to find rendered items for $item and $contentType")
        for (renderedItem in renderedItems) {
            Assertions.assertNotEquals(
                "",
                renderedItem?.pointer,
                "Expected to find rendered item pointer for $item and $contentType"
            )
            Assertions.assertNotEquals(
                "",
                renderedItem?.hash,
                "Expected to find rendered item hash for $item and $contentType"
            )

        }

        return renderedItems
    }

    fun addItemToCollection(identifier: String, collection: String, assertedBy: String) {
        val envelope = Envelope(
            listOf(
                Item().withIdentifier(Identifier(identifier))
                    .withRelationship(
                        Relationship(
                            "collection",
                            Item()
                                .withIdentifier(Identifier(collection)),
                        ),
                    ),
            ),
            ItemTreeAssertion(
                OffsetDateTime.now(),
                MergeStrategy.NAIVE,
                Item().withIdentifier(Identifier(assertedBy)),
            ),
        )

        val envelopeBatch = EnvelopeBatch(
            listOf(envelope),
            EnvelopeBatchProvenance("Crossref Test", "1234"),
        )

        itemGraph.assert(envelopeBatch)

        itemGraphSetup.waitForRABuffer()
    }

    fun checkEtagForVersion(version: Int, contentType: String) {
        val result = apiResponseContext.andDo(MockMvcResultHandlers.print())
            .andExpect(status().isOk)
            .andReturn()

        val eTag = result.response.getHeader("ETag")
        val identifier = result.request.requestURI?.split("/v1/items/")?.last()

        Assertions.assertNotNull(identifier, "Expected to find identifier from request")

        val resolvedItem = resolveAndCheckItem(identifier!!)
        val resolvedContentType = resolveAndCheckContentType(contentType)

        val renderedItem = renderedItemDao.getRenderedItemHistory(resolvedItem.pk!!, resolvedContentType)
            .drop(version)
            .firstOrNull()
        val hash = renderedItem?.hash

        Assertions.assertEquals("\"" + hash + "\"", eTag, "Expected api $eTag to match the stored hash $hash")
    }

    fun checkDataForVersion(version: Int, contentType: String) {
        val result = apiResponseContext.andDo(MockMvcResultHandlers.print())
            .andExpect(status().isOk)
            .andReturn()

        val identifier = result.request.requestURI?.split("/v1/items/")?.last()
        val content = result.response.contentAsString

        Assertions.assertNotNull(identifier, "Expected to find identifier from request")

        val resolvedContentType = resolveAndCheckContentType(contentType)
        val resolvedItem = resolveAndCheckItem(identifier!!)
        val renderedItem = renderedItemStorageDao.fetchContent(
            renderedItemDao.getRenderedItemHistory(resolvedItem.pk!!, resolvedContentType).drop(version).first(),
        )

        Assertions.assertEquals(
            renderedItem.content,
            content,
            "Expected api $content to match the stored blob ${renderedItem.content}"
        )
    }

    fun checkLastModifiedForVersion(version: Int, contentType: String) {
        val result = apiResponseContext.andDo(MockMvcResultHandlers.print())
            .andExpect(status().isOk)
            .andReturn()

        val identifier = result.request.requestURI?.split("/v1/items/")?.last()
        val apiLastModified = result.response.getHeader("Last-Modified")

        val resolvedContentType = resolveAndCheckContentType(contentType)

        Assertions.assertNotNull(identifier, "Expected to find identifier from request")

        val resolvedItem = resolveAndCheckItem(identifier!!)
        val renderedItem =
            renderedItemDao.getRenderedItemHistory(resolvedItem.pk!!, resolvedContentType).drop(version).firstOrNull()
        val storeLastModified = renderedItem?.updatedAt
        val formatter =
            DateTimeFormatter.ofPattern("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.US).withZone(ZoneId.of("GMT"))
        val lastModified = formatter.format(storeLastModified)

        Assertions.assertEquals(
            lastModified,
            apiLastModified,
            "Expected api Last-Modified $apiLastModified to match the stored last modified $lastModified"
        )
    }

    fun getApiResponseAsFeed(): Feed {
        val result = apiResponseContext.andDo(MockMvcResultHandlers.print())
            .andExpect(status().isOk)
            .andReturn()

        return mapper.readValue(result.response.contentAsString, Feed::class.java)
    }

    @Given("all items have been allowed to render and are no longer stale")
    fun all_items_no_longer_stale() {
        waitForNoneStale();
    }

    /**
     * Clear the 'render status' table (i.e. clear all stale flags) and the rendered_item table (i.e. results of
     * rendering).
     * This lets us ingest documents in the first part of a scenario and then clear their stale statuses.
     * We can then test how staleness and rendering changes with the 'When' clause.
     *
     * The Item Tree reachability table is out of scope here. We want to keep that intact during a scenario (it's
     * cleared at the start of each scenario).
     */
    @Given("no Items have been rendered or marked stale yet")
    fun no_items_have_been_rendered_yet() {
        logger.info("Truncating rendered_item and item_render_status")
        resetRendering()
    }

    @Given("the following Items have the following stored rendered representations")
    fun the_following_Items_have_the_following_stored_renderer_representations(items: List<Statements.Item>) {
        all_items_are_marked_not_stale()
        no_items_have_been_rendered_yet()
        items.forEach { item ->
            val identifier = item.identifier
            val contentType = item.contentType
            val resolvedItem = resolver.resolveRO(Identifier(identifier))
            val createdItem = resolver.resolveRW(resolvedItem).withProperties(
                listOf(
                    Properties(
                        jsonObjectFromMap(mapOf("title-long" to listOf(mapOf("value" to item.title)))),
                    ),
                ),
            )

            Assertions.assertNotNull(createdItem, "Expected to find item with identifier $identifier")
            Assertions.assertNotNull(createdItem.pk, "Expected to find pk for $createdItem")

            Assertions.assertNotNull(
                contentType!!,
                "Expected Content Type to be non null, it should be a mimiType resolvable by ContentType"
            )

            addItemToCollection(identifier, "https://id.crossref.org/collections/work", "https://ror.org/02twcfp32")
            itemTreeRenderer.renderContentType(createdItem, contentType)
        }
    }

    @Then("the following Items are marked as stale")
    fun the_following_items_are_marked_as_stale(items: List<Statements.Item>) {
        items.forEach { item ->
            val resolvedItem = resolveAndCheckItem(item.identifier)
            val status = renderStatusDao.getRenderStatus(resolvedItem.pk!!)
            Assertions.assertNotNull(status, "Expected to find render status for ${resolvedItem.pk}")
            Assertions.assertTrue(status.stale)
        }
    }

    /**
     * Check the rendered status of a list of items using the 'Identifier' and 'Rendered?' fields.
     * This can check for the presence or absence of rendered items.
     * This waits for the render queue / loop to run, so it can wait for a row to appear for True.
     * For False it's checking for the absence, so that's less rigorous (e.g. a delay in rendering might be missed).
     * Therefore, put any 'false' ones at the end of the scenario after any delays have run.
     */
    @Then("Items are or aren't rendered")
    fun the_following_items_are_rendered(items: List<Statements.Item>) {

        // Then wait for all the stale flags to have been processed, which means the renderer has seen them all.
        waitForNoneStale()

        items.forEach { item ->
            val resolvedItem = resolveAndCheckItem(item.identifier)

            when (item.rendered) {
                true -> {
                    // True, wait for the row to appear.
                    val renders = waitAndCheckForRenderedItems(resolvedItem, item.contentType!!)

                    if (item.numRenders != null) {
                        Assertions.assertEquals(item.numRenders, renders.count(), "Expected number of renders")
                    }
                }
                false -> {
                    // False, don't wait. This is less rigorous.
                    val result = renderedItemDao.getRenderedItems(resolvedItem.pk!!)
                    Assertions.assertTrue(
                        result.isEmpty(),
                        "Expected to find no renders for ${item.identifier}. Found $result."
                    )
                }
                else -> {
                    Assertions.fail("Expected Render status to be true or false.")
                }
            }
        }
    }

    /**
     * Wait for all stale flags to be set false.
     */
    private fun waitForNoneStale() {
        var numStale = 0
        var retries = 0
        do {
            Thread.sleep(2000)
            numStale =
                jdbcTemplate.queryForObject("SELECT COUNT(pk) FROM item_render_status WHERE stale = TRUE", Int::class.java)!!

            logger.info("Found $numStale stale items...")

        } while (numStale > 0 && ++retries < 30)
        Assertions.assertTrue(numStale == 0, "Timed out waiting for all stale flags to be removed.")
    }


    @Then("the following Items have stale status <Stale?>")
    fun the_following_items_have_stale_status(items: List<Statements.Item>) {
        items.forEach { item ->
            val resolvedItem = resolveAndCheckItem(item.identifier)
            val status = renderStatusDao.getRenderStatus(resolvedItem.pk!!)
            Assertions.assertNotNull(status, "Expected to find render status for ${resolvedItem.pk}")
            Assertions.assertEquals(
                item.stale,
                status.stale,
                "Expected to find $item render stale status is ${item.stale}"
            )
        }
    }

    /**
     * Checking to see if an item is rendered relies on StaleItemSqsWriter and
     * StaleItemSqsReader working in the background to publish/process items to/from
     * the rendering queue.
     */
    @Then("the Item {string} is rendered as {string}")
    fun the_item_is_rendered_as(identifier: String, contentType: String) {
        val resolvedContentType = resolveAndCheckContentType(contentType)
        val resolvedItem = resolveAndCheckItem(identifier)
        waitAndCheckForRenderedItems(resolvedItem, resolvedContentType)
    }

    @Then("the ETag header should match the most recent hash in storage for {string}")
    fun the_etag_header_should_match_the_most_recent_hash_in_storage(contentType: String) {
        checkEtagForVersion(0, contentType)
    }

    @Then("the ETag header should match {int} versions old hash in storage for {string}")
    fun the_etag_header_should_match_the_version_hash_in_storage(version: Int, contentType: String) {
        checkEtagForVersion(version, contentType)
    }

    @Then("the status code should be 4XX client error")
    fun the_status_should_be_client_error() {
        apiResponseContext.andDo(MockMvcResultHandlers.print())
            .andExpect(status().is4xxClientError)
            .andReturn()
    }

    @Then("the Content-Type header should be {string}")
    fun the_content_type_header_should_be(contentType: String) {
        val result = apiResponseContext.andDo(MockMvcResultHandlers.print())
            .andExpect(status().isOk)
            .andReturn()

        val apiContentType = result.response.getHeader("Content-Type")
        Assertions.assertEquals(
            contentType,
            apiContentType,
            "Expected api Content-Type $apiContentType to match $contentType"
        )
    }

    @Then("the Link header should be {string}")
    fun the_link_header_should_be(link: String) {
        val result = apiResponseContext.andDo(MockMvcResultHandlers.print())
            .andExpect(status().isOk)
            .andReturn()

        val apiFeedLink = URLDecoder.decode(result.response.getHeader("Link"), StandardCharsets.UTF_8)
        Assertions.assertEquals(link, apiFeedLink, "Expected api Feed Link $apiFeedLink to match $link")
    }

    @Then("the Last-Modified header should match the most recent one in storage for {string}")
    fun the_last_modified_header_should_match_the_most_recent_one_in_storage(contentType: String) {
        checkLastModifiedForVersion(0, contentType)
    }

    @Then("the Last-Modified header should match {int} versions old in storage for {string}")
    fun the_last_modified_header_should_match_the_most_recent_one_in_storage(version: Int, contentType: String) {
        checkLastModifiedForVersion(version, contentType)
    }

    @Then("the returned data should match the most recent one in storage for {string}")
    fun the_returned_data_should_match_the_most_recent_one_in_storage(contentType: String) {
        checkDataForVersion(0, contentType)
    }

    @Then("the returned data should match {int} versions old data in storage for {string}")
    fun the_returned_data_should_match_version_in_storage(version: Int, contentType: String) {
        checkDataForVersion(version, contentType)
    }

    @Then("the title of the returned Item should be {string}")
    fun the_title_of_the_returned_item_should_be(title: String) {
        val result = apiResponseContext.andDo(MockMvcResultHandlers.print())
            .andExpect(status().isOk)
            .andReturn()

        val identifier = result.request.requestURI?.split("/v1/items/")?.last()
        val content = result.response.contentAsString
        val jsonObject = JSONObject(content)
        val apiTitle = jsonObject.getJSONArray("title").getString(0)

        Assertions.assertNotNull(identifier, "Expected to find identifier from request")

        Assertions.assertEquals(apiTitle, title, "Expected api title $apiTitle to match $title")
    }

    @Then("the feed has no {string} link")
    fun the_feed_has_no_link(rel: String) {
        val feed = getApiResponseAsFeed()
        Assertions.assertTrue(feed.links.none { l -> l.rel == rel })
    }

    @Then("the feed has a {string} link href prefixed with the feed endpoint {string}")
    fun the_feed_has_a_link_href_prefixed_with_the_feed_endpoint(rel: String, endpoint: String) {
        val feed = getApiResponseAsFeed()
        val expected = "http://localhost:1234$endpoint"
        Assertions.assertEquals(expected, feed.links.first { l -> l.rel == rel }.href)
    }

    @Then("the feed has a {string} link href prefixed with the feed endpoint {string} and {int} for {int}")
    fun the_feed_has_a_link_href_prefixed_with_the_feed_endpoint_and_cursor(
        rel: String,
        endpoint: String,
        nextCursor: Int,
        rows: Int,
    ) {
        val feed = getApiResponseAsFeed()
        val expected = "http://localhost:1234$endpoint?cursor=$nextCursor&rows=$rows"
        Assertions.assertEquals(expected, feed.links.first { l -> l.rel == rel }.href)
    }

    @Then("the feed has no entries")
    fun the_feed_has_no_entries() {
        val feed = getApiResponseAsFeed()
        Assertions.assertTrue(feed.entries.none())
    }

    @Then("the feed has a {string} link href prefixed with the feed endpoint {string} for {string} and {string}")
    fun the_feed_has_a_self_href_prefixed_with_the_feed_endpoint_for(rel: String, endpoint: String, identifier: String, contentType: String) {
        val feed = getApiResponseAsFeed()
        val encodedContentType = URLEncoder.encode(contentType, StandardCharsets.UTF_8)
        val expected = "http://localhost:1234$endpoint?content-type=$encodedContentType&item=$identifier"
        Assertions.assertEquals(expected, feed.links.first { l -> l.rel == rel }.href)
    }

    @Then("entry {int} has id prefixed with the item endpoint for {string} with {string} and {int}")
    fun entry_id_is_prefixed_with_the_item_endpoint_for_identifer(
        skip: Int,
        identifier: String,
        contentType: String,
        version: Int
    ) {
        val resolvedItem = resolveAndCheckItem(identifier)
        val resolvedContentType = resolveAndCheckContentType(contentType)
        val renderedItem =
            renderedItemDao.getRenderedItemHistory(resolvedItem.pk!!, resolvedContentType).drop(version - 1).first()

        Assertions.assertNotNull(renderedItem, "Expected to find a rendered item for ${resolvedItem.pk}")

        val feed = getApiResponseAsFeed()
        val prefix = "http://localhost:1234/v1/items/${resolvedItem.pk}"
        val mimeType = URLEncoder.encode(resolvedContentType.mimeType, StandardCharsets.UTF_8)
        val versionString = renderedItem.version
        val expected = "$prefix?content-type=$mimeType&version=$versionString"
        Assertions.assertEquals(expected, feed.entries.drop(skip - 1).first().id, "Expected the entry ID to be based on URL")
    }

    @Then("entry {int} has {string} link href prefixed with the item endpoint for {string} with {string} and {int}")
    fun entry_related_link_href_is_prefixed_with_the_item_endpoint_for_identifer(
        skip: Int,
        rel: String,
        identifier: String,
        contentType: String,
        version: Int
    ) {
        val resolvedItem = resolveAndCheckItem(identifier)
        val resolvedContentType = resolveAndCheckContentType(contentType)
        val renderedItem =
            renderedItemDao.getRenderedItemHistory(resolvedItem.pk!!, resolvedContentType).drop(version - 1).first()

        Assertions.assertNotNull(renderedItem, "Expected to find a rendered item for ${resolvedItem.pk}")

        val feed = getApiResponseAsFeed()
        val prefix = "http://localhost:1234/v1/items/${resolvedItem.pk}"
        val mimeType = resolvedContentType.mimeType
        val encodedMimeType = URLEncoder.encode(resolvedContentType.mimeType, StandardCharsets.UTF_8)
        val versionString = renderedItem.version
        val expected = "$prefix?content-type=$encodedMimeType&version=$versionString"
        val entry = feed.entries.drop(skip - 1).first()
        val related = entry.links.firstOrNull { l -> l.rel == "related" }

        Assertions.assertEquals(expected, related?.href, "Expected link to match for related")
        Assertions.assertEquals(rel, related?.rel, "Expected rel type to match for related")
        Assertions.assertEquals(mimeType, related?.type, "Expected mime type to match for related")
    }

    @Then("entry {int} has {string} link href prefixed with the item pointer for {string} with {string} and {int}")
    fun entry_enclosure_link_href_is_prefixed_with_the_item_pointer_for_identifer(
        skip: Int,
        rel: String,
        identifier: String,
        contentType: String,
        version: Int
    ) {
        val resolvedItem = resolveAndCheckItem(identifier)
        val resolvedContentType = resolveAndCheckContentType(contentType)
        val renderedItem =
            renderedItemDao.getRenderedItemHistory(resolvedItem.pk!!, resolvedContentType).drop(version - 1).first()

        Assertions.assertNotNull(renderedItem, "Expected to find a rendered item for ${resolvedItem.pk}")

        val feed = getApiResponseAsFeed()
        val mimeType = resolvedContentType.mimeType
        val prefix = "s3://render-bucket/${renderedItem.pointer}"
        val entry = feed.entries.drop(skip - 1).first()
        val enclosure = entry.links.firstOrNull { l -> l.rel == "enclosure" }

        Assertions.assertEquals(prefix, enclosure?.href, "Expected link to match for enclosure")
        Assertions.assertEquals(rel, enclosure?.rel, "Expected rel to match for enclosure")
        Assertions.assertEquals(mimeType, enclosure?.type, "Expected mime type to match for enclosure")
    }

    @Then("entry {int} has {string} link href prefixed with the item endpoint for {string} with type {string}")
    fun entry_alternate_link_href_is_prefixed_with_the_item_endpoint_for_identifer(
        skip: Int,
        rel: String,
        identifier: String,
        contentType: String
    ) {
        val resolvedItem = resolveAndCheckItem(identifier)
        val resolvedContentType = resolveAndCheckContentType(contentType)
        val feed = getApiResponseAsFeed()
        val prefix = "http://localhost:1234/v1/items/${resolvedItem.pk}"
        val mimeType = resolvedContentType.mimeType
        val entry = feed.entries.drop(skip - 1).first()
        val alternate = entry.links.firstOrNull { l -> l.rel == "alternate" }

        Assertions.assertEquals(prefix, alternate?.href, "Expected link to match for alternate")
        Assertions.assertEquals(rel, alternate?.rel, "Expected rel to match for alternate")
        Assertions.assertEquals(mimeType, alternate?.type, "Expected mime type to match for alternate")
    }

    @Then("entry {int} has updated equal to {int} for {string} with {string}")
    fun entry_id_has_update_equal_to_version_for_identifier_with_content_type(
        skip: Int,
        version: Int,
        identifier: String,
        contentType: String
    ) {
        val resolvedItem = resolveAndCheckItem(identifier)
        val resolvedContentType = resolveAndCheckContentType(contentType)

        val result = apiResponseContext.andDo(MockMvcResultHandlers.print())
            .andExpect(status().isOk)
            .andReturn()

        val renderedItem =
            renderedItemDao.getRenderedItemHistory(resolvedItem.pk!!, resolvedContentType).drop(version - 1).first()

        Assertions.assertNotNull(renderedItem, "Expected to find a rendered item for ${resolvedItem.pk}")

        val feed = mapper.readValue(result.response.contentAsString, Feed::class.java)
        val updatedAt = renderedItem.updatedAt.toString()
        Assertions.assertEquals(updatedAt, feed.entries.drop(skip - 1).first().updated)
    }

    @Then("entry {int} has title equal to Item {int} in {string} for {string}")
    fun entry_id_has_title_equal_to_version_for_identifier_with_content_type(
        skip: Int,
        version: Int,
        contentType: String,
        identifier: String
    ) {
        val resolvedItem = resolveAndCheckItem(identifier)
        val resolvedContentType = resolveAndCheckContentType(contentType)

        val result = apiResponseContext.andDo(MockMvcResultHandlers.print())
            .andExpect(status().isOk)
            .andReturn()

        val renderedItem =
            renderedItemDao.getRenderedItemHistory(resolvedItem.pk!!, resolvedContentType).drop(version - 1).first()

        Assertions.assertNotNull(renderedItem, "Expected to find a rendered item for ${resolvedItem.pk}")

        val feed = mapper.readValue(result.response.contentAsString, Feed::class.java)
        val titleString = "Item ${resolvedItem.pk} in ${resolvedContentType.mimeType}"
        Assertions.assertEquals(titleString, feed.entries.drop(skip - 1).first().title, "Expected title to match")
    }

    /**
     * As well marking all item_render_Status rows as stale = false
     * we need to purge the render queue for a clean test
     */
    @Given("all Items are not stale")
    fun all_items_are_marked_not_stale() {
        jdbcTemplate.update("UPDATE item_render_status SET stale = false")
    }

    @When("the feed is retrieved from the {string} endpoint")
    fun the_feed_is_retrieved_from_the_endpoint(endpoint: String) {
        apiResponseContext.perform(get(URI("http://localhost:1234$endpoint")))
    }

    @When("{int} feed items after {int} are retrieved from the {string} endpoint")
    fun feed_items_after_cursor_are_retrieed_from_the_endpoint(rows: Int, cursor: Int, endpoint: String) {
        apiResponseContext.perform(get(URI("http://localhost:1234$endpoint?rows=$rows&cursor=$cursor")))
    }

    @When("the feed for {string} and {string} is retrieved from the {string} endpoint")
    fun the_feed_for_item_and_content_type_is_retrieved_from_the_endpoint(
        identifier: String,
        contentType: String,
        endpoint: String
    ) {
        apiResponseContext.perform(get(URI("http://localhost:1234$endpoint?item=$identifier&content-type=$contentType")))
    }

    @When("{string} is retrieved from the {string} endpoint")
    fun is_retrieved_from_the_endpoint(identifier: String, endpoint: String) {
        apiResponseContext.perform(get(URI("http://localhost:1234$endpoint$identifier")))
    }

    @When("{string} is retrieved from the {string} endpoint by its PK")
    fun is_retrieved_from_the_endpoint_by_its_pk(identifier: String, endpoint: String) {
        val resolvedItem = resolveAndCheckItem(identifier)
        val pk = resolvedItem.pk
        apiResponseContext.perform(get(URI("http://localhost:1234$endpoint$pk")))
    }

    @When("retrieving a version of {string} from the {string} endpoint without a content type")
    fun retrieving_a_version_without_a_content_type(identifier: String, endpoint: String) {
        resolveAndCheckItem(identifier)
        val version = OffsetDateTime.now()
        apiResponseContext.perform(get(URI("http://localhost:1234$endpoint$identifier?version=$version")))
    }

    @When("retrieving a version of {string} from the {string} endpoint as {string} without a version")
    fun retrieving_a_version_without_a_content_type(identifier: String, endpoint: String, contentType: String) {
        resolveAndCheckItem(identifier)
        resolveAndCheckContentType(contentType)
        apiResponseContext.perform(get(URI("http://localhost:1234$endpoint$identifier?content-type=$contentType")))
    }

    @When("retrieving {string} at {int} versions old from the {string} endpoint with content type {string}")
    fun retrieving_at_versions_old_from_the_endpoint_with_content_type(
        identifier: String,
        version: Int,
        endpoint: String,
        contentType: String
    ) {
        val resolvedItem = resolveAndCheckItem(identifier)
        val resolvedContentType = resolveAndCheckContentType(contentType)
        val pk = resolvedItem.pk!!

        val versions = renderedItemDao.getRenderedItemHistory(pk, resolvedContentType)
        val renderedItem = versions.drop(version).firstOrNull()

        Assertions.assertNotNull(
            renderedItem,
            "Expected to find renderedItem $version older than the current with content type $contentType"
        )

        val renderedVersion = renderedItem?.version
        apiResponseContext.perform(get(URI("http://localhost:1234$endpoint$pk?version=$renderedVersion&content-type=$contentType")))
    }

    @When("all Items are marked as stale")
    fun all_items_are_marked_as_stale() {
        fixItemRenderStatus()
    }

    @When("{string} is added to {string} by {string} and it is marked as stale")
    fun item_is_added_to_collection_and_is_stale(item: String, collection: String, assertedBy: String) {
        addItemToCollection(item, collection, assertedBy)

        val resolvedItem = resolveAndCheckItem(item)

        val status = renderStatusDao.getRenderStatus(resolvedItem.pk!!)
        Assertions.assertNotNull(status, "Expected to find render status for ${resolvedItem.pk}")
        Assertions.assertTrue(status.stale, "Expected to find $item render status is stale")
    }

    private fun fixItemRenderStatus() {
        logDuration(logger, "updating 'stale' flags on all item render statuses.") {
            jdbcTemplate.update(
                """
                TRUNCATE item_render_status;
                INSERT INTO item_render_status ( item_pk, stale, updated_at )
                SELECT pk, true, now() FROM item;
                """
            )
        }
    }
}
