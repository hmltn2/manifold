package org.crossref.manifold.rendering

import io.cucumber.junit.platform.engine.Constants
import org.junit.platform.suite.api.ConfigurationParameter
import org.junit.platform.suite.api.SelectClasspathResource
import org.junit.platform.suite.api.Suite

@Suite
@SelectClasspathResource("features/rendering")
@ConfigurationParameter(
    key = Constants.GLUE_PROPERTY_NAME,
    value = "org.crossref.manifold.rendering" +
            ",org.crossref.manifold.common"
)
class RunRenderingTestsIT
