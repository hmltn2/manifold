Feature: Rendered items feed

  Scenario Outline: Feeds returns recent items

    Given the following Items have the following stored rendered representations 
      | Identifier                       | Content Type              |
      | https://doi.org/10.5555/12345678 | application/citeproc+json |
      | https://doi.org/10.5555/87654321 | application/citeproc+json |
    When the feed is retrieved from the "/v1/items/feed.xml" endpoint
    Then the Content-Type header should be "application/atom+xml"
    And the feed has a "self" link href prefixed with the feed endpoint "/v1/items/feed.xml"
    And the feed has a "first" link href prefixed with the feed endpoint "/v1/items/feed.xml"
    And entry <Position> has id prefixed with the item endpoint for <Identifier> with <ContentType> and <Version>
    And entry <Position> has updated equal to <Version> for <Identifier> with <ContentType>
    And entry <Position> has title equal to Item <Version> in <ContentType> for <Identifier>
    And entry <Position> has "alternate" link href prefixed with the item endpoint for <Identifier> with type <ContentType>
    And entry <Position> has "related" link href prefixed with the item endpoint for <Identifier> with <ContentType> and <Version>
    And entry <Position> has "enclosure" link href prefixed with the item pointer for <Identifier> with <ContentType> and <Version>

    Examples:
      | Position | Identifier                         | ContentType                 | Version |
      | 1        | "https://doi.org/10.5555/87654321" | "application/citeproc+json" | 1       |
      | 2        | "https://doi.org/10.5555/12345678" | "application/citeproc+json" | 1       |

  Scenario Outline: Feeds returns recent items past cursor

    Given the following Items have the following stored rendered representations 
      | Identifier                       | Content Type              |
      | https://doi.org/10.5555/12345678 | application/citeproc+json |
      | https://doi.org/10.5555/87654321 | application/citeproc+json |
      | https://doi.org/10.5555/11111111 | application/citeproc+json |
      | https://doi.org/10.5555/22222222 | application/citeproc+json |
    When <Rows> feed items after <Cursor> are retrieved from the "/v1/items/feed.xml" endpoint
    Then the Content-Type header should be "application/atom+xml"
    And the feed has a "self" link href prefixed with the feed endpoint "/v1/items/feed.xml"
    And the feed has a "first" link href prefixed with the feed endpoint "/v1/items/feed.xml"
    And the feed has a "next" link href prefixed with the feed endpoint "/v1/items/feed.xml" and <NextCursor> for <Rows>
    And entry <Position> has id prefixed with the item endpoint for <Identifier> with <ContentType> and <Version>
    And entry <Position> has updated equal to <Version> for <Identifier> with <ContentType>
    And entry <Position> has title equal to Item <Version> in <ContentType> for <Identifier>
    And entry <Position> has "alternate" link href prefixed with the item endpoint for <Identifier> with type <ContentType>
    And entry <Position> has "related" link href prefixed with the item endpoint for <Identifier> with <ContentType> and <Version>
    And entry <Position> has "enclosure" link href prefixed with the item pointer for <Identifier> with <ContentType> and <Version>

    Examples:
      | Position | Identifier                         | ContentType                 | Version | Rows | Cursor | NextCursor |
      | 1        | "https://doi.org/10.5555/87654321" | "application/citeproc+json" | 1       | 2    | 3      | 1          |
      | 2        | "https://doi.org/10.5555/12345678" | "application/citeproc+json" | 1       | 2    | 3      | 1          |
      | 1        | "https://doi.org/10.5555/22222222" | "application/citeproc+json" | 1       | 2    | 5      | 3          |
      | 2        | "https://doi.org/10.5555/11111111" | "application/citeproc+json" | 1       | 2    | 5      | 3          |

  Scenario: An empty feed

    Given the following Items have the following stored rendered representations 
      | Identifier | Content Type |
    When the feed is retrieved from the "/v1/items/feed.xml" endpoint
    Then the Content-Type header should be "application/atom+xml"
    And the feed has a "self" link href prefixed with the feed endpoint "/v1/items/feed.xml"
    And the feed has a "first" link href prefixed with the feed endpoint "/v1/items/feed.xml"
    And the feed has no "next" link
    And the feed has no entries 

  Scenario Outline: Feeds returns recent items for identifier

    Given the following Items have the following stored rendered representations 
      | Identifier                       | Content Type                                   | Title                   |
      | https://doi.org/10.5555/12345678 | application/citeproc+json                      | Article Title           |
      | https://doi.org/10.5555/12345678 | application/vnd.crossref.citeproc.elastic+json | Article Title           |
      | https://doi.org/10.5555/12345678 | application/citeproc+json                      | Article Title Updated 1 |
      | https://doi.org/10.5555/12345678 | application/vnd.crossref.citeproc.elastic+json | Article Title Updated 1 |
    When the feed for <Identifier> and <ContentType> is retrieved from the "/v1/items/feed.xml" endpoint
    Then the Content-Type header should be "application/atom+xml"
    And the feed has a "self" link href prefixed with the feed endpoint "/v1/items/feed.xml" for <Identifier> and <ContentType>
    And the feed has a "first" link href prefixed with the feed endpoint "/v1/items/feed.xml" for <Identifier> and <ContentType>
    And entry <Position> has id prefixed with the item endpoint for <Identifier> with <ContentType> and <Version>
    And entry <Position> has updated equal to <Version> for <Identifier> with <ContentType>
    And entry <Position> has title equal to Item <Version> in <ContentType> for <Identifier>
    And entry <Position> has "alternate" link href prefixed with the item endpoint for <Identifier> with type <ContentType>
    And entry <Position> has "related" link href prefixed with the item endpoint for <Identifier> with <ContentType> and <Version>
    And entry <Position> has "enclosure" link href prefixed with the item pointer for <Identifier> with <ContentType> and <Version>

    Examples:
      | Position | Identifier                         | ContentType                                      | Version |
      | 1        | "https://doi.org/10.5555/12345678" | "application/citeproc+json"                      | 1       |
      | 2        | "https://doi.org/10.5555/12345678" | "application/citeproc+json"                      | 2       |
      | 1        | "https://doi.org/10.5555/12345678" | "application/vnd.crossref.citeproc.elastic+json" | 1       |
      | 2        | "https://doi.org/10.5555/12345678" | "application/vnd.crossref.citeproc.elastic+json" | 2       |
