Feature: Rendering metadata

 Scenario: Articles are rendered in citeproc+json

  Given an empty Item Graph database
   And no Items have been rendered or marked stale yet
   And the following Items in the Item Graph
     | Identifier                       |
     | https://doi.org/10.5555/12345678 |
  And all Items are not stale
  When "https://doi.org/10.5555/12345678" is added to "https://id.crossref.org/collections/work" by "https://ror.org/02twcfp32" and it is marked as stale
  Then the Item "https://doi.org/10.5555/12345678" is rendered as "application/citeproc+json"
  And the Item "https://doi.org/10.5555/12345678" is rendered as "application/vnd.crossref.citeproc.elastic+json"
  And the following Items have stale status <Stale?>
    | Identifier                       | Stale? |
    | https://doi.org/10.5555/12345678 | false  |
