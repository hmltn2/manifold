Feature: Retrieving rendered items

  Scenario Outline: Items can be retrieved

    Given the following Items have the following stored rendered representations 
      | Identifier                       | Content Type              |
      | https://doi.org/10.5555/12345678 | application/citeproc+json |
      | https://doi.org/10.5555/87654321 | application/citeproc+json |
    When <Identifier> is retrieved from the "/v1/items/" endpoint
    Then the ETag header should match the most recent hash in storage for <Content Type>
    And the returned data should match the most recent one in storage for <Content Type>
    And the Last-Modified header should match the most recent one in storage for <Content Type>
    And the Content-Type header should be <Content Type>
    And the Link header should be <Link>

    Examples:
      | Identifier                         | Content Type                | Link                                                                                                                                  |
      | "https://doi.org/10.5555/12345678" | "application/citeproc+json" | "<http://localhost:1234/v1/items/feed.xml?content-type=application/citeproc+json&item=https://doi.org/10.5555/12345678>, rel=\"feed\"" |
      | "https://doi.org/10.5555/87654321" | "application/citeproc+json" | "<http://localhost:1234/v1/items/feed.xml?content-type=application/citeproc+json&item=https://doi.org/10.5555/87654321>, rel=\"feed\"" |

  Scenario Outline: Items can be retrieved by their PK

    Given the following Items have the following stored rendered representations 
      | Identifier                       | Content Type              |
      | https://doi.org/10.5555/12345678 | application/citeproc+json |
      | https://doi.org/10.5555/87654321 | application/citeproc+json |
    When <Identifier> is retrieved from the "/v1/items/" endpoint by its PK
    Then the ETag header should match the most recent hash in storage for <Content Type>
    And the returned data should match the most recent one in storage for <Content Type>
    And the Last-Modified header should match the most recent one in storage for <Content Type>
    And the Content-Type header should be <Content Type>

    Examples:
      | Identifier                         | Content Type                |
      | "https://doi.org/10.5555/12345678" | "application/citeproc+json" |
      | "https://doi.org/10.5555/87654321" | "application/citeproc+json" |

  Scenario Outline: Most recent items can be retrieved

    Given the following Items have the following stored rendered representations 
      | Identifier                       | Content Type              | Title                   |
      | https://doi.org/10.5555/12345678 | application/citeproc+json | Article Title           |
      | https://doi.org/10.5555/12345678 | application/citeproc+json | Article Title Updated 1 |
      | https://doi.org/10.5555/12345678 | application/citeproc+json | Article Title Updated 2 |
      | https://doi.org/10.5555/12345678 | application/citeproc+json | Article Title Updated 3 |
    When <Identifier> is retrieved from the "/v1/items/" endpoint
    Then the ETag header should match the most recent hash in storage for <Content Type>
    And the returned data should match the most recent one in storage for <Content Type>
    And the Last-Modified header should match the most recent one in storage for <Content Type>
    And the Content-Type header should be <Content Type>
    And the title of the returned Item should be <Title>

    Examples:
      | Identifier                         | Content Type                | Title                     |
      | "https://doi.org/10.5555/12345678" | "application/citeproc+json" | "Article Title Updated 3" |

  Scenario Outline: citeproc+json is favoured over application/vnd.crossref.citeproc.elastic+json

    Given the following Items have the following stored rendered representations 
      | Identifier                       | Content Type                      |
      | https://doi.org/10.5555/12345678 | application/citeproc+json         |
      | https://doi.org/10.5555/12345678 | application/vnd.crossref.citeproc.elastic+json |
      | https://doi.org/10.5555/87654321 | application/citeproc+json         |
      | https://doi.org/10.5555/87654321 | application/vnd.crossref.citeproc.elastic+json |
    When <Identifier> is retrieved from the "/v1/items/" endpoint
    Then the ETag header should match the most recent hash in storage for <Content Type>

    Examples:
      | Identifier                         | Content Type                |
      | "https://doi.org/10.5555/12345678" | "application/citeproc+json" |
      | "https://doi.org/10.5555/87654321" | "application/citeproc+json" |

  Scenario Outline: A specific item version can be retrieved

    Given the following Items have the following stored rendered representations 
      | Identifier                       | Content Type              | Title           |
      | https://doi.org/10.5555/12345678 | application/citeproc+json | Article Title 1 |
      | https://doi.org/10.5555/12345678 | application/citeproc+json | Article Title 2 |
      | https://doi.org/10.5555/12345678 | application/citeproc+json | Article Title 3 |
    When retrieving <Identifier> at <Version> versions old from the "/v1/items/" endpoint with content type <Content Type>
    Then the ETag header should match <Version> versions old hash in storage for <Content Type>
    And the returned data should match <Version> versions old data in storage for <Content Type>
    And the Last-Modified header should match <Version> versions old in storage for <Content Type>
    And the Content-Type header should be <Content Type>

    Examples:
      | Identifier                         | Content Type                | Version |
      | "https://doi.org/10.5555/12345678" | "application/citeproc+json" | 1       |
      | "https://doi.org/10.5555/12345678" | "application/citeproc+json" | 2       |

  Scenario: Versioned item retrieval needs a content type

    Given the following Items have the following stored rendered representations 
      | Identifier                       | Content Type              | Title           |
      | https://doi.org/10.5555/12345678 | application/citeproc+json | Article Title 1 |
    When retrieving a version of "https://doi.org/10.5555/12345678" from the "/v1/items/" endpoint without a content type
    Then the status code should be 4XX client error

  Scenario: Retrieving as content type needs a version

    Given the following Items have the following stored rendered representations 
      | Identifier                       | Content Type              | Title           |
      | https://doi.org/10.5555/12345678 | application/citeproc+json | Article Title 1 |
    When retrieving a version of "https://doi.org/10.5555/12345678" from the "/v1/items/" endpoint as "application/citeproc+json" without a version
    Then the status code should be 4XX client error
