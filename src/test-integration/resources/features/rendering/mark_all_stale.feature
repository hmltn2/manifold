Feature: Mark all as stale

  Sometimes, such as after a significant code update or bulk ingestion, we want to mark all Items for re-render.

  Scenario: Marking all items as stale automatically marks DOI items as stale

    Given the following Items in the Item Graph
      | Identifier                                             |
      | https://doi.org/10.46936/cpcy.proj.2019.50733/60006578 |
      | https://doi.org/10.5555/87654321                       |
      | https://doi.org/10.46936/cpcy.proj.2019.50733/60006578 |
    And the following Items have stale status <Stale?>
      | Identifier                                             | Stale? |
      | https://doi.org/10.46936/cpcy.proj.2019.50733/60006578 | false  |
      | https://doi.org/10.5555/87654321                       | false  | 
      | https://doi.org/10.46936/cpcy.proj.2019.50733/60006578 | false  |
    When all Items are marked as stale
    Then the following Items are marked as stale
      | Identifier | 
      | https://doi.org/10.46936/cpcy.proj.2019.50733/60006578 |
      | https://doi.org/10.5555/87654321 |
      | https://doi.org/10.46936/cpcy.proj.2019.50733/60006578 | 
