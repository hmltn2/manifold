Feature: Tracking Stale Status

  Ingesting a simple XML document about 10.5555/12345678 should result in its output being marked as stale, meaning it's
  rendered.

  Scenario: Ingesting a Simple XML document automatically creates rendered outputs

    Given an empty Item Graph database
    And UniXSD file "journal-article-10.5555-12345678.xml" registers Journal DOI "10.5555/1234567890"
    When UniXSD file "journal-article-10.5555-12345678.xml" is ingested
    Then Items are or aren't rendered
      | Identifier                       | Content Type              | Rendered? |
      | https://doi.org/10.5555/12345678 | application/citeproc+json | True      |

