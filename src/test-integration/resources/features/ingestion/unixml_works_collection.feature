Feature: UniXML Collections

  A COLLECTION is a named set of Items, e.g. "the set of Works", "the set of Members", "the set of Journal Articles".

  Collections enable us to have API endpoints such as /works and /members.

  Each Collection is itself represented in the Item Graph as an Item itself, and membership of a Collection is
  represented with the 'collection' relationship type.

  The UniXML ingester creates a Collection for every type (e.g. 'work') and subtype (e.g. 'journal-article'). In a given
  XML file, Every Item with a DOI is added to the Collection for its type and subtype.

  The Collection for the Works type is https://id.crossref.org/collections/work
  The Collection for Journal Articles is https://id.crossref.org/collections/work/journal-article
  etc

  Not every Collection is described here, because they are created automatically based on 'type' and 'subtype'
  properties.

  Scenario: Journal Article

    Given UniXSD file "journal-article-10.5555-12345678.xml" registers Journal DOI "10.5555/1234567890"
    And UniXSD file "journal-article-10.5555-12345678.xml" registers Article DOI "10.5555/12345678"
    When UniXSD file "journal-article-10.5555-12345678.xml" is ingested
    Then the following relationship statements are found in the item graph
      | Current | Asserted By               | Subject ID                         | Relationship Type | Object ID                                                |
      | true    | https://ror.org/02twcfp32 | https://doi.org/10.5555/12345678   | collection        | https://id.crossref.org/collections/work                 |
      | true    | https://ror.org/02twcfp32 | https://doi.org/10.5555/12345678   | collection        | https://id.crossref.org/collections/work/journal-article |
      | true    | https://ror.org/02twcfp32 | https://doi.org/10.5555/1234567890 | collection        | https://id.crossref.org/collections/work                 |
      | true    | https://ror.org/02twcfp32 | https://doi.org/10.5555/1234567890 | collection        | https://id.crossref.org/collections/work/journal         |

  Scenario: Book

    Given UniXSD file "book-10.7788-9783412516710.xml" registers Book DOI "10.7788/9783412516710"
    When UniXSD file "book-10.7788-9783412516710.xml" is ingested
    Then the following relationship statements are found in the item graph
      | Current | Asserted By               | Subject ID                            | Relationship Type | Object ID                                     |
      | true    | https://ror.org/02twcfp32 | https://doi.org/10.7788/9783412516710 | collection        | https://id.crossref.org/collections/work      |
      | true    | https://ror.org/02twcfp32 | https://doi.org/10.7788/9783412516710 | collection        | https://id.crossref.org/collections/work/book |


  Scenario: Book Component

    Given UniXSD file "book-chapter-10.2307-j.ctt1tg5gs2.19.xml" registers Book DOI "10.2307/j.ctt1tg5gs2"
    And UniXSD file "book-chapter-10.2307-j.ctt1tg5gs2.19.xml" registers Book Chapter DOI "10.2307/j.ctt1tg5gs2.19"
    When UniXSD file "book-chapter-10.2307-j.ctt1tg5gs2.19.xml" is ingested
    Then the following relationship statements are found in the item graph
      | Current | Asserted By               | Subject ID                              | Relationship Type | Object ID                                               |
      | true    | https://ror.org/02twcfp32 | https://doi.org/10.2307/j.ctt1tg5gs2    | collection        | https://id.crossref.org/collections/work                |
      | true    | https://ror.org/02twcfp32 | https://doi.org/10.2307/j.ctt1tg5gs2    | collection        | https://id.crossref.org/collections/work/book           |
      | true    | https://ror.org/02twcfp32 | https://doi.org/10.2307/j.ctt1tg5gs2.19 | collection        | https://id.crossref.org/collections/work                |
      | true    | https://ror.org/02twcfp32 | https://doi.org/10.2307/j.ctt1tg5gs2.19 | collection        | https://id.crossref.org/collections/work/book-component |

  Scenario: Proceedings Article

    Given UniXSD file "proceedings-article-10.1109-elinsl.1996.549297.xml" registers Conference Paper DOI "10.1109/ELINSL.1996.549297"
    When UniXSD file "proceedings-article-10.1109-elinsl.1996.549297.xml" is ingested
    Then the following relationship statements are found in the item graph
      | Current | Asserted By               | Subject ID                                 | Relationship Type | Object ID                                                    |
      | true    | https://ror.org/02twcfp32 | https://doi.org/10.1109/elinsl.1996.549297 | collection        | https://id.crossref.org/collections/work                     |
      | true    | https://ror.org/02twcfp32 | https://doi.org/10.1109/elinsl.1996.549297 | collection        | https://id.crossref.org/collections/work/proceedings-article |

  Scenario: Component

    Given UniXSD file "component-10.1371- journal.pone.0020476.s005.xml" registers Standalone Component DOI "10.1371/journal.pone.0020476.s005"
    When UniXSD file "component-10.1371- journal.pone.0020476.s005.xml" is ingested
    Then the following relationship statements are found in the item graph
      | Current | Asserted By               | Subject ID                                        | Relationship Type | Object ID                                          |
      | true    | https://ror.org/02twcfp32 | https://doi.org/10.1371/journal.pone.0020476.s005 | collection        | https://id.crossref.org/collections/work           |
      | true    | https://ror.org/02twcfp32 | https://doi.org/10.1371/journal.pone.0020476.s005 | collection        | https://id.crossref.org/collections/work/component |
