package org.crossref.manifold.modules.eventdata.bus

import org.crossref.manifold.modules.eventdata.invalidToken
import org.crossref.manifold.modules.eventdata.secrets
import org.crossref.manifold.modules.eventdata.support.bus.MultiJwtVerifier
import org.crossref.manifold.modules.eventdata.token
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

internal class MultiJwtVerifierTest {

    private val verifier = MultiJwtVerifier(secrets)

    @Test
    fun testVerify() {
        val jwt = verifier.verify(token)
        assertTrue(jwt?.subject.equals("crossref"))
        assertNull(verifier.verify(invalidToken))
        assertNull(verifier.verify(null))
    }
}