-- A batch of envelopes sent in response to user input.
CREATE TABLE envelope_batch (
    pk BIGSERIAL PRIMARY KEY NOT NULL,
    ext_trace VARCHAR NULL,
    user_agent VARCHAR NULL,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL
);

CREATE INDEX ON envelope_batch (ext_trace);

CREATE TABLE envelope (
    pk BIGSERIAL PRIMARY KEY NOT NULL,
    envelope_batch_pk BIGINT NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL,
    FOREIGN KEY (envelope_batch_pk) REFERENCES envelope_batch (pk)
);

CREATE INDEX ON envelope (envelope_batch_pk);

CREATE TABLE ingestion_message (
    pk BIGSERIAL PRIMARY KEY NOT NULL,
    envelope_batch_pk BIGINT NOT NULL,
    envelope_pk BIGINT NULL,
    stage INT NOT NULL,
    status BOOLEAN NOT NULL,
    message TEXT NULL,
    updated_at TIMESTAMP WITH TIME ZONE NOT NULL,
    FOREIGN KEY (envelope_batch_pk) REFERENCES envelope_batch (pk),
    FOREIGN KEY (envelope_pk) REFERENCES envelope (pk)
);

CREATE INDEX ON ingestion_message (envelope_batch_pk, envelope_pk);
CREATE INDEX ON ingestion_message (envelope_pk);

-- An Item is any 'thing' that exists, including Research Objects.
CREATE TABLE item (
    pk BIGSERIAL PRIMARY KEY NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL
);

-- A vocabulary of relationship types.
CREATE TABLE identifier_type (
    -- This is a smallint (max value 32,767) because the values in this table will have low cardinality:
    -- they are driven by a curated schema file. The Identifiers table has a PK to this, and the repeated extra 2 bytes
    -- adds up (half a gigabyte of difference)
    pk SMALLSERIAL PRIMARY KEY NOT NULL,
    prefix VARCHAR(190) NOT NULL,
    is_blank BOOLEAN NOT NULL DEFAULT FALSE,
    UNIQUE (prefix)
);

-- An Item Identifier is a string, which is associated with exactly one Item.
-- Identifiers from arbitrary websites can get big, hence no length constraint on suffix.
-- There is no performance difference with PostgreSQL.
CREATE TABLE item_identifier (
    pk BIGSERIAL PRIMARY KEY NOT NULL,
    suffix VARCHAR NOT NULL,
    identifier_type_pk SMALLINT NOT NULL,
    item_pk BIGINT NOT NULL,
    CONSTRAINT identifier_type_fk FOREIGN KEY (identifier_type_pk) REFERENCES identifier_type (pk),
    CONSTRAINT item_fk FOREIGN KEY (item_pk) REFERENCES item (pk),
    CONSTRAINT item_identifier_un UNIQUE (identifier_type_pk, suffix)
);

CREATE INDEX item_identifier_dup_lookup_in ON item_identifier (identifier_type_pk, suffix, pk);
CREATE INDEX item_identifier_item_in ON item_identifier (item_pk);

-- A vocabulary of relationship types.
CREATE TABLE relationship_type (
    pk SERIAL PRIMARY KEY NOT NULL,
    value VARCHAR(190) NOT NULL,
    UNIQUE (value)
);

CREATE TABLE relationship (
    pk BIGSERIAL PRIMARY KEY NOT NULL,
    first_ingested_at TIMESTAMP WITH TIME ZONE NOT NULL,
    last_ingested_at TIMESTAMP WITH TIME ZONE NOT NULL,
    times_ingested INT NOT NULL,
    first_linked_at TIMESTAMP WITH TIME ZONE NULL,
    last_linked_at TIMESTAMP WITH TIME ZONE NULL,
    times_linked INT NULL,
    from_item_pk BIGINT NOT NULL,
    to_item_pk BIGINT NOT NULL,
    relationship_type_pk INT NOT NULL,
    CONSTRAINT from_item_fk FOREIGN KEY (from_item_pk) REFERENCES item (pk),
    CONSTRAINT to_item_fk FOREIGN KEY (to_item_pk) REFERENCES item (pk),
    CONSTRAINT relationship_type_fk FOREIGN KEY (relationship_type_pk) REFERENCES relationship_type (pk),
    CONSTRAINT relationship_unique UNIQUE (from_item_pk, relationship_type_pk, to_item_pk)
);

CREATE TABLE relationship_assertion_buffer (
    pk BIGSERIAL PRIMARY KEY NOT NULL,
    asserted_at TIMESTAMP WITH TIME ZONE NOT NULL,
    state BOOLEAN NOT NULL,
    party_pk BIGINT NOT NULL,
    from_item_pk BIGINT NOT NULL,
    to_item_pk BIGINT NOT NULL,
    relationship_type_pk INT NOT NULL,
    envelope_pk BIGINT NOT NULL
);

CREATE TABLE relationship_assertion_current (
    pk BIGINT PRIMARY KEY NOT NULL,
    asserted_at TIMESTAMP WITH TIME ZONE NOT NULL,
    ingested_at TIMESTAMP WITH TIME ZONE NOT NULL,
    state BOOLEAN NOT NULL,
    party_pk BIGINT NOT NULL,
    from_item_pk BIGINT NOT NULL,
    from_item_is_blank BOOLEAN NOT NULL,
    to_item_pk BIGINT NOT NULL,
    to_item_is_blank BOOLEAN NOT NULL,
    relationship_type_pk INT NOT NULL,
    relationship_type_is_object BOOLEAN NOT NULL,
    envelope_pk BIGINT NOT NULL,
    CONSTRAINT party_fk FOREIGN KEY (party_pk) REFERENCES item (pk),
    CONSTRAINT from_item_fk FOREIGN KEY (from_item_pk) REFERENCES item (pk),
    CONSTRAINT to_item_fk FOREIGN KEY (to_item_pk) REFERENCES item (pk),
    CONSTRAINT envelope_fk FOREIGN KEY (envelope_pk) REFERENCES envelope (pk),
    CONSTRAINT relationship_type_fk FOREIGN KEY (relationship_type_pk) REFERENCES relationship_type (pk),
    CONSTRAINT relationship_assertion_unique UNIQUE (from_item_pk, relationship_type_pk, to_item_pk, party_pk)
);

-- Needed for quick lookup in set_stale_trees_from_items.
CREATE INDEX relationship_assertion_relationship_type ON relationship_assertion_current (relationship_type_pk);

CREATE INDEX relationship_assertion_simple_relationship ON relationship_assertion_current (from_item_pk, relationship_type_pk, to_item_pk)
    WHERE NOT from_item_is_blank
            AND NOT relationship_type_is_object
            AND NOT to_item_is_blank;

CREATE INDEX relationship_assertion_reified_from_item ON relationship_assertion_current (from_item_pk, relationship_type_pk, to_item_pk)
    WHERE NOT from_item_is_blank
            AND NOT relationship_type_is_object
            AND to_item_is_blank;

CREATE INDEX relationship_assertion_reified_to_item ON relationship_assertion_current (from_item_pk, to_item_pk)
    WHERE NOT from_item_is_blank
            AND relationship_type_is_object
            AND NOT to_item_is_blank;

CREATE TABLE relationship_assertion_history (
    pk BIGINT PRIMARY KEY NOT NULL,
    asserted_at TIMESTAMP WITH TIME ZONE NOT NULL,
    ingested_at TIMESTAMP WITH TIME ZONE NOT NULL,
    state BOOLEAN NOT NULL,
    party_pk BIGINT NOT NULL,
    from_item_pk BIGINT NOT NULL,
    from_item_is_blank BOOLEAN NOT NULL,
    to_item_pk BIGINT NOT NULL,
    to_item_is_blank BOOLEAN NOT NULL,
    relationship_type_pk INT NOT NULL,
    relationship_type_is_object BOOLEAN NOT NULL,
    envelope_pk BIGINT NOT NULL,
    CONSTRAINT party_fk FOREIGN KEY (party_pk) REFERENCES item (pk),
    CONSTRAINT from_item_fk FOREIGN KEY (from_item_pk) REFERENCES item (pk),
    CONSTRAINT to_item_fk FOREIGN KEY (to_item_pk) REFERENCES item (pk),
    CONSTRAINT envelope_fk FOREIGN KEY (envelope_pk) REFERENCES envelope (pk),
    CONSTRAINT relationship_type_fk FOREIGN KEY (relationship_type_pk) REFERENCES relationship_type (pk)
);

CREATE INDEX relationship_assertion_history_idx ON relationship_assertion_history (from_item_pk, relationship_type_pk, to_item_pk, party_pk);

CREATE VIEW relationship_assertion AS
SELECT *, TRUE AS current
FROM relationship_assertion_current
UNION ALL
SELECT *, FALSE AS current
FROM relationship_assertion_history;

CREATE FUNCTION process_buffered_relationship_assertions(IN batch_size INT, OUT count_processed INT, OUT count_remaining INT)
    LANGUAGE plpgsql AS
$$
BEGIN
    CREATE TEMP TABLE rab_processing ON COMMIT DROP AS
    SELECT *
    FROM relationship_assertion_buffer
    ORDER BY pk
    LIMIT batch_size;

    IF (SELECT COUNT(*) FROM rab_processing) > 0 THEN
        CREATE UNIQUE INDEX ON rab_processing (pk);

        CREATE INDEX ON rab_processing (from_item_pk, relationship_type_pk, to_item_pk, party_pk, pk DESC);

        /*
         * For each row in rab_processing, bring together the details needed to add a new relationship assertion.
           Also compare the pk of the current row to the maximum (i.e. latest) pk for the same relationship to deduce whether or not this row is the current assertion.
         */
        CREATE TEMP TABLE rap_select ON COMMIT DROP AS
        SELECT rap.*,
            NOW() AS ingested_at,
            rap.pk = latest_pk.pk AS current,
            fit.is_blank AS from_item_is_blank,
            tit.is_blank AS to_item_is_blank,
            rt.value = 'object' AS relationship_type_is_object
        FROM rab_processing rap
             JOIN item_identifier fii ON rap.from_item_pk = fii.item_pk
             JOIN identifier_type fit ON fii.identifier_type_pk = fit.pk
             JOIN item_identifier tii ON rap.to_item_pk = tii.item_pk
             JOIN identifier_type tit ON tii.identifier_type_pk = tit.pk
             JOIN relationship_type rt ON rap.relationship_type_pk = rt.pk
             JOIN LATERAL (
                 -- Find the maximum pk for this particular assertion.
                SELECT pk
                FROM rab_processing
                WHERE (from_item_pk, relationship_type_pk, to_item_pk, party_pk) =
                    (rap.from_item_pk, rap.relationship_type_pk, rap.to_item_pk, rap.party_pk)
                ORDER BY pk DESC
                LIMIT 1
                ) latest_pk ON TRUE;

        -- Moves current rows that are about to be superseded from current to history.
        WITH assertions_to_move AS (
            -- Delete and return from the current table
            DELETE FROM relationship_assertion_current rac
                USING rap_select rap
                WHERE (rac.from_item_pk, rac.relationship_type_pk, rac.to_item_pk, rac.party_pk) =
                    (rap.from_item_pk, rap.relationship_type_pk, rap.to_item_pk, rap.party_pk)
                        AND current
                RETURNING rac.*)
        -- And insert the returned deletions into history
        INSERT
            INTO relationship_assertion_history
            SELECT *
            FROM assertions_to_move;

        -- Insert buffered assertions that are already non-current straight into history.
        INSERT INTO relationship_assertion_history (
            pk,
            asserted_at,
            ingested_at,
            state,
            party_pk,
            from_item_pk,
            from_item_is_blank,
            to_item_pk,
            to_item_is_blank,
            relationship_type_pk,
            relationship_type_is_object,
            envelope_pk)
        SELECT pk,
            asserted_at,
            ingested_at,
            state,
            party_pk,
            from_item_pk,
            from_item_is_blank,
            to_item_pk,
            to_item_is_blank,
            relationship_type_pk,
            relationship_type_is_object,
            envelope_pk
        FROM rap_select
        WHERE NOT current;

        -- Insert new current buffered assertions.
        INSERT INTO relationship_assertion_current (
            pk,
            asserted_at,
            ingested_at,
            state,
            party_pk,
            from_item_pk,
            from_item_is_blank,
            to_item_pk,
            to_item_is_blank,
            relationship_type_pk,
            relationship_type_is_object,
            envelope_pk)
        SELECT pk,
            asserted_at,
            ingested_at,
            state,
            party_pk,
            from_item_pk,
            from_item_is_blank,
            to_item_pk,
            to_item_is_blank,
            relationship_type_pk,
            relationship_type_is_object,
            envelope_pk
        FROM rap_select
        WHERE current;

        -- normal relationships
        INSERT INTO relationship (
            from_item_pk,
            relationship_type_pk,
            to_item_pk,
            first_ingested_at,
            last_ingested_at,
            times_ingested,
            first_linked_at,
            last_linked_at,
            times_linked)
        -- This select only applies the first time a relationship is asserted. Otherwise the existing one gets updated by the conflict.
        SELECT from_item_pk,
            relationship_type_pk,
            to_item_pk,
            MIN(ingested_at),
            MAX(ingested_at),
            COUNT(*),
            NULL,
            NULL,
            NULL
        FROM rap_select
        WHERE NOT from_item_is_blank
            AND NOT relationship_type_is_object
            AND NOT to_item_is_blank
        GROUP BY from_item_pk,
            relationship_type_pk,
            to_item_pk
        ORDER BY from_item_pk,
            relationship_type_pk,
            to_item_pk
        ON CONFLICT ON CONSTRAINT relationship_unique
            DO UPDATE SET last_ingested_at = excluded.last_ingested_at,
            times_ingested = relationship.times_ingested + excluded.times_ingested;

        -- Assertions with blank object that is already linked.
        WITH
            new_blank_nodes AS (SELECT from_item_pk,
            relationship_type_pk,
            to_item_pk,
            MIN(ingested_at) AS first_ingested_at,
            MAX(ingested_at) AS last_ingested_at,
            COUNT(*) AS times_ingested
        FROM rap_select
        WHERE NOT from_item_is_blank
            AND NOT relationship_type_is_object
            AND to_item_is_blank
        GROUP BY from_item_pk,
            relationship_type_pk,
            to_item_pk),
            -- this intermediate join is required to aggregate multiple middle hops and avoid duplicate relationships.
            reified_assertions AS (SELECT nbn.from_item_pk,
                -- relationship type comes from the first hop
                nbn.relationship_type_pk,
                rac.to_item_pk,
                nbn.first_ingested_at,
                nbn.last_ingested_at,
                nbn.times_ingested,
                rac.ingested_at AS first_linked_at,
                rac.ingested_at AS last_linked_at,
                1 AS times_linked
            FROM new_blank_nodes nbn
                -- The relationship table is intentionally a simplified summary which does not care about assertion state.
                 JOIN relationship_assertion_current rac
                      ON nbn.to_item_pk = rac.from_item_pk
                              AND rac.from_item_is_blank
                              AND rac.relationship_type_is_object
                              AND NOT rac.to_item_is_blank)
        INSERT
        INTO relationship(
            from_item_pk,
            relationship_type_pk,
            to_item_pk,
            first_ingested_at,
            last_ingested_at,
            times_ingested,
            first_linked_at,
            last_linked_at,
            times_linked)
        SELECT from_item_pk,
            relationship_type_pk,
            to_item_pk,
            MIN(first_ingested_at),
            MAX(last_ingested_at),
            SUM(times_ingested),
            MIN(first_linked_at),
            MAX(last_linked_at),
            SUM(times_linked)
        FROM reified_assertions
        GROUP BY from_item_pk,
            relationship_type_pk,
            to_item_pk
        ORDER BY from_item_pk,
            relationship_type_pk,
            to_item_pk
        ON CONFLICT ON CONSTRAINT relationship_unique
            DO UPDATE SET last_ingested_at = excluded.last_ingested_at,
            times_ingested = relationship.times_ingested + excluded.times_ingested;

        -- Linking objects that can be matched to existing assertions.
        WITH new_object_relationships AS (SELECT from_item_pk,
            relationship_type_pk,
            to_item_pk,
            MIN(ingested_at) AS first_linked_at,
            MAX(ingested_at) AS last_linked_at,
            COUNT(*) AS times_linked
        FROM rap_select
        WHERE from_item_is_blank
            AND relationship_type_is_object
            AND NOT to_item_is_blank
        GROUP BY from_item_pk,
            relationship_type_pk,
            to_item_pk),
            -- this intermediate join is required to aggregate multiple middle hops and avoid duplicate relationships.
            reified_assertions AS (SELECT rac.from_item_pk,
                -- relationship type comes from the first hop
                rac.relationship_type_pk,
                nor.to_item_pk,
                rac.ingested_at AS first_ingested_at,
                rac.ingested_at AS last_ingested_at,
                1 AS times_ingested,
                nor.first_linked_at,
                nor.last_linked_at,
                nor.times_linked
            FROM new_object_relationships nor
                 JOIN relationship_assertion_current rac
                      ON nor.from_item_pk = rac.to_item_pk
                              AND NOT rac.from_item_is_blank
                              AND NOT rac.relationship_type_is_object
                              AND rac.to_item_is_blank)
        INSERT
        INTO relationship(
            from_item_pk,
            relationship_type_pk,
            to_item_pk,
            first_ingested_at,
            last_ingested_at,
            times_ingested,
            first_linked_at,
            last_linked_at,
            times_linked)
        SELECT from_item_pk,
            relationship_type_pk,
            to_item_pk,
            MIN(first_ingested_at),
            MAX(last_ingested_at),
            SUM(times_ingested),
            MIN(first_linked_at),
            MAX(last_linked_at),
            SUM(times_linked)
        FROM reified_assertions
        GROUP BY from_item_pk,
            relationship_type_pk,
            to_item_pk
        ORDER BY from_item_pk,
            relationship_type_pk,
            to_item_pk
        ON CONFLICT ON CONSTRAINT relationship_unique
            DO UPDATE SET last_linked_at = excluded.last_linked_at,
            times_linked = relationship.times_linked + excluded.times_linked;

        -- When relationship assertions are made against an item, mark that Item as stale
        -- and in need of a potential re-render.
        -- Only mark stale items with 'from_item_pk', not 'to_item_pk', as those are the
        -- changes that might result in needing a re-render.
        INSERT INTO item_render_status (
            item_pk,
            stale,
            updated_at)
        SELECT DISTINCT from_item_pk, TRUE, NOW()
        FROM rap_select
        ORDER BY from_item_pk
        ON CONFLICT (item_pk) DO UPDATE SET stale = TRUE, updated_at = NOW();

        -- Delete the processed rows from the buffer!
        DELETE
        FROM relationship_assertion_buffer rab
            USING rab_processing rap
        WHERE rab.pk = rap.pk;
    END IF;

    -- These counts are estimates based on pks as doing an actual count is too expensive.
    SELECT MAX(pk) - MIN(pk) + 1 FROM rab_processing INTO count_processed;

    SELECT MAX(pk) - MIN(pk) + 1 FROM relationship_assertion_buffer INTO count_remaining;
END;
$$;

CREATE TABLE property_assertion_buffer (
    pk BIGSERIAL PRIMARY KEY NOT NULL,
    asserted_at TIMESTAMP WITH TIME ZONE NOT NULL,
    values JSONB NOT NULL,
    party_pk BIGINT NOT NULL,
    item_pk BIGINT NOT NULL,
    envelope_pk BIGINT NOT NULL
);

CREATE TABLE property_assertion_current (
    pk BIGINT PRIMARY KEY NOT NULL,
    asserted_at TIMESTAMP WITH TIME ZONE NOT NULL,
    ingested_at TIMESTAMP WITH TIME ZONE NOT NULL,
    values JSONB NOT NULL,
    party_pk BIGINT NOT NULL,
    item_pk BIGINT NOT NULL,
    envelope_pk BIGINT NOT NULL,
    CONSTRAINT party_fk FOREIGN KEY (party_pk) REFERENCES item (pk),
    CONSTRAINT item_fk FOREIGN KEY (item_pk) REFERENCES item (pk),
    CONSTRAINT envelope_fk FOREIGN KEY (envelope_pk) REFERENCES envelope (pk),
    CONSTRAINT property_assertion_unique UNIQUE (item_pk, party_pk)
);

CREATE TABLE property_assertion_history (
    pk BIGINT PRIMARY KEY NOT NULL,
    asserted_at TIMESTAMP WITH TIME ZONE NOT NULL,
    ingested_at TIMESTAMP WITH TIME ZONE NOT NULL,
    values JSONB NOT NULL,
    party_pk BIGINT NOT NULL,
    item_pk BIGINT NOT NULL,
    envelope_pk BIGINT NOT NULL,
    CONSTRAINT party_fk FOREIGN KEY (party_pk) REFERENCES item (pk),
    CONSTRAINT item_fk FOREIGN KEY (item_pk) REFERENCES item (pk),
    CONSTRAINT envelope_fk FOREIGN KEY (envelope_pk) REFERENCES envelope (pk)
);

CREATE INDEX property_assertion_history_idx ON property_assertion_history (item_pk, party_pk);

CREATE VIEW property_assertion AS
SELECT *, TRUE AS current
FROM property_assertion_current
UNION ALL
SELECT *, FALSE AS current
FROM property_assertion_history;

CREATE FUNCTION process_buffered_property_assertions(IN batch_size INT, OUT count_processed INT, OUT count_remaining INT)
    LANGUAGE plpgsql AS
$$
BEGIN
    CREATE TEMP TABLE pab_processing ON COMMIT DROP AS
    SELECT *
    FROM property_assertion_buffer
    ORDER BY pk
    LIMIT batch_size;

    IF (SELECT COUNT(*) FROM pab_processing) > 0 THEN
        CREATE UNIQUE INDEX ON pab_processing (pk);

        CREATE INDEX ON pab_processing (item_pk, party_pk, pk DESC);

        /*
         * For each row in pab_processing, compare the pk of the row with the maximum (i.e. latest) pk for the same property to deduce whether or not this row is the current assertion.
         */
        CREATE TEMP TABLE pap_select ON COMMIT DROP AS
        SELECT pap.*, NOW() AS ingested_at, pap.pk = ipap.pk AS current
        FROM pab_processing pap
             JOIN LATERAL (
                -- Find the maximum pk for this particular assertion.
                SELECT pk
                FROM pab_processing
                WHERE (item_pk, party_pk) =
                    (pap.item_pk, pap.party_pk)
                ORDER BY pk DESC
                LIMIT 1
                ) ipap ON TRUE
        ORDER BY pap.pk;

        -- Moves current rows that are about to be superseded from current to history.
        WITH pa_history AS (
            -- Delete and return from the current table
            DELETE FROM property_assertion_current pa
                USING pap_select pap
                WHERE (pa.item_pk, pa.party_pk) =
                    (pap.item_pk, pap.party_pk)
                        AND current
                RETURNING pa.*)
            -- And insert the returned deletions into history
        INSERT
        INTO property_assertion_history
        SELECT *
        FROM pa_history;

        -- Insert buffered assertions that are already non-current straight into history.
        INSERT INTO property_assertion_history (
            pk,
            asserted_at,
            ingested_at,
            values,
            party_pk,
            item_pk,
            envelope_pk)
        SELECT pk,
            asserted_at,
            NOW(),
            values,
            party_pk,
            item_pk,
            envelope_pk
        FROM pap_select pap
        WHERE NOT current;

        -- Insert new current buffered assertions.
        INSERT INTO property_assertion_current (
            pk,
            asserted_at,
            ingested_at,
            values,
            party_pk,
            item_pk,
            envelope_pk)
        SELECT pk,
            asserted_at,
            NOW(),
            values,
            party_pk,
            item_pk,
            envelope_pk
        FROM pap_select pap
        WHERE current;

        -- When property assertions are made against an item, mark that Item as stale
        -- and in need of a re-render.
        INSERT INTO item_render_status (
            item_pk,
            stale,
            updated_at)
        SELECT DISTINCT item_pk, TRUE, NOW()
        FROM pap_select
        ORDER BY item_pk
        ON CONFLICT (item_pk) DO UPDATE SET stale = TRUE, updated_at = NOW();

        -- Delete the processed rows from the buffer!
        DELETE
        FROM property_assertion_buffer pab
            USING pab_processing pap
        WHERE pab.pk = pap.pk;
    END IF;

    -- These counts are estimates based on pks as doing an actual count is too expensive.
    SELECT MAX(pk) - MIN(pk) + 1 FROM pab_processing INTO count_processed;

    SELECT MAX(pk) - MIN(pk) + 1 FROM property_assertion_buffer INTO count_remaining;
END;
$$;

-- A collection of items and their current render status
CREATE TABLE item_render_status (
    pk SERIAL PRIMARY KEY NOT NULL,
    updated_at TIMESTAMP WITH TIME ZONE NOT NULL,
    stale BOOLEAN NOT NULL DEFAULT FALSE,
    item_pk BIGINT NOT NULL,
    CONSTRAINT item_fk FOREIGN KEY (item_pk) REFERENCES item (pk)
);

CREATE UNIQUE INDEX item_render_status_item_in ON item_render_status (item_pk);

CREATE TABLE open_cursors (
    cursor_name CHAR(6) PRIMARY KEY NOT NULL,
    row_count INT NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL,
    last_used_at TIMESTAMP WITH TIME ZONE NOT NULL,
    last_key INT NOT NULL
);

-- A collection of items and their rendered conent types
CREATE TABLE rendered_item (
    pk SERIAL PRIMARY KEY NOT NULL,
    item_pk BIGINT NOT NULL,
    content_type VARCHAR NOT NULL,
    pointer VARCHAR NOT NULL,
    hash VARCHAR NOT NULL,
    current BOOLEAN NOT NULL,
    updated_at TIMESTAMP WITH TIME ZONE NOT NULL,
    version BIGINT NOT NULL,
    CONSTRAINT item_fk FOREIGN KEY (item_pk) REFERENCES item (pk)
);

CREATE INDEX rendered_item_content_type_in ON rendered_item (content_type);
CREATE INDEX rendered_item_current_in ON rendered_item (current);
CREATE INDEX rendered_item_updated_at_in ON rendered_item (updated_at);
CREATE INDEX rendered_item_version_in ON rendered_item (version);

-- Some items, such as Articles and Members, are treated as root nodes of a tree. These are Items that will be rendered
-- into metadata documents.
-- This table stores the pre-calculated tree from each of those nodes. It allows the retrieval of the tree, but also
-- a quick index to find all trees that contain a given item.
CREATE TABLE item_tree_reachability (
    -- root of the tree
    root_item_pk BIGINT PRIMARY KEY NOT NULL,
    -- pks of all items found in the tree
    tree_item_pks BIGINT[]
);

-- Allow searching of a root node by an item that may be in the tree.
CREATE INDEX item_tree_reachability_contains ON item_tree_reachability USING gin (tree_item_pks);

-- Given an item_pk, find the tree that stems from it, and update the reachability index.
-- This can be called for any item but should be called only on designated root nodes.
CREATE FUNCTION build_reachability(changed_root_item_pk BIGINT, max_depth INT)
    RETURNS INTEGER
    LANGUAGE plpgsql AS
$$
BEGIN
    WITH
        RECURSIVE
        tree(from_item_pk, to_item_pk, depth) AS (SELECT from_item_pk, to_item_pk, (SELECT max_depth)
        FROM relationship_assertion_current
        WHERE from_item_pk = changed_root_item_pk
        UNION
        SELECT child.from_item_pk, child.to_item_pk, depth - 1
        FROM tree,
            relationship_assertion_current AS child
        WHERE tree.to_item_pk = child.from_item_pk
            AND depth > 0),
        item_pks AS (SELECT ARRAY_AGG(DISTINCT (to_item_pk)) AS tree_pks FROM tree)
    INSERT
    INTO
        item_tree_reachability (
        root_item_pk,
        tree_item_pks)
    VALUES (
        changed_root_item_pk,
        (SELECT tree_pks FROM item_pks))
    ON CONFLICT (root_item_pk) DO UPDATE SET tree_item_pks = (SELECT tree_pks FROM item_pks);
    RETURN 1;
END;
$$;

-- Given a set of items that have changed, find the trees that they are part of and update them.
-- Each tree will be marked as stale for re-rending.
-- The reachability index will also be updated because the tree structure may have changed.
CREATE FUNCTION set_stale_trees_from_items(changed_item_pks BIGINT[])
    RETURNS INTEGER
    LANGUAGE plpgsql AS
$$
BEGIN
    WITH
        -- Given an affected item changed_item_pk, find all of the tree roots that this is in.
        affected_root_items AS (SELECT root_item_pk FROM item_tree_reachability WHERE changed_item_pks && tree_item_pks)

    INSERT
    INTO item_render_status (
        updated_at,
        stale,
        item_pk)
    SELECT NOW(), TRUE, root_item_pk
    FROM affected_root_items
    ON CONFLICT (item_pk)
        DO UPDATE SET updated_at = NOW(), stale = TRUE;

    RETURN 1;
END;
$$;

-- Needed to enable set_stale_trees_from_relationship_type
-- Find any item that has the given relationship type and mark it as needing its item tree rebuilding.
-- This is designed to be used with the 'collections' relationship type because all root nodes live in a collection.
CREATE FUNCTION build_reachability_indexes_from_relationship_type(relationship_type_name TEXT, depth INTEGER)
    RETURNS INTEGER
    LANGUAGE plpgsql AS
$$
DECLARE
    -- Used for iteration.
    root_pk BIGINT;
BEGIN
    -- Constant of depth 3 for trees.
    FOR root_pk IN (SELECT from_item_pk
    FROM relationship_assertion_current
    WHERE relationship_type_pk = (SELECT pk FROM relationship_type WHERE value = relationship_type_name))
        LOOP
            PERFORM build_reachability(root_pk, depth);
        END LOOP;
    RETURN 1;
END;
$$;
