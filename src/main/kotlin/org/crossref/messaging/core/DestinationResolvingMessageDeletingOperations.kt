package org.crossref.messaging.core

import org.springframework.messaging.Message

/**
 * Extends [MessageDeletingOperations] and adds operations for deleting messages from a destination specified as a
 * (resolvable) String name.
 *
 * @param D the destination type.
 */
interface DestinationResolvingMessageDeletingOperations<D> : MessageDeletingOperations<D> {
    /**
     * Resolve the given destination name to a destination and delete the given message from it.
     *
     * @param destinationName the destination name to resolve.
     * @param message the message to delete.
     */
    fun delete(destinationName: String, message: Message<*>)

    /**
     * Resolve the given destination name to a destination and receive a message from it, without deleting the message.
     *
     * @param destinationName the destination name to resolve.
     *
     * @return the received message, possibly `null` if the message could not be received, for example due to a timeout.
     */
    fun receiveNoDelete(destinationName: String): Message<*>?
}
