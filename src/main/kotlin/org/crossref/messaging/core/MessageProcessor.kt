package org.crossref.messaging.core

import org.springframework.messaging.Message
import org.springframework.messaging.core.MessagePostProcessor

interface MessageProcessor {
    fun processMessage(message: Message<*>, messagePostProcessor: MessagePostProcessor? = null)
}
