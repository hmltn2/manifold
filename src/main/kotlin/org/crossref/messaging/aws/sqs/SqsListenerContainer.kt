package org.crossref.messaging.aws.sqs

import org.apache.commons.lang3.mutable.MutableBoolean
import org.crossref.messaging.core.QueuePoller
import org.springframework.beans.factory.DisposableBean
import org.springframework.beans.factory.InitializingBean
import org.springframework.boot.context.event.ApplicationStartedEvent
import org.springframework.context.ApplicationListener
import org.springframework.messaging.Message

/**
 * Container class for SQS message listeners that implements the lifecycle interfaces used by the Spring container to
 * create, initialise and start this container.
 *
 * Continually and asynchronously polls the SQS queues named in the [SqsListener] annotated functions registered
 * by Spring, handing off received messages to the SQS message handler and deleting processed messages as per the
 * configured [SqsDeletePolicy] for each queue.
 *
 * @param sqsTemplate used for performing messaging operations on the SQS queues.
 * @param sqsMessageHandler handles the messages received from SQS queues.
 * @param queuePoller asynchronously polls queues.
 * @param defaultParallelPollers the default number of pollers to start in parallel, per queue.
 */
class SqsListenerContainer(
    private val sqsTemplate: SqsTemplate,
    private val sqsMessageHandler: SqsMessageHandler,
    private val queuePoller: QueuePoller<String>,
    private val defaultParallelPollers: Int
) : ApplicationListener<ApplicationStartedEvent>, InitializingBean, DisposableBean {
    private lateinit var queueDeletePolicies: Map<String, SqsDeletePolicy>

    private lateinit var queueParallelPollers: Map<String, Int>

    override fun afterPropertiesSet() {
        queueDeletePolicies = sqsMessageHandler.handlerMethods.keys.associate { it.queueName to it.deletePolicy }
        queueParallelPollers = sqsMessageHandler.handlerMethods.keys.associate { it.queueName to it.parallelPollers }
    }

    override fun onApplicationEvent(event: ApplicationStartedEvent) {
        queuePoller.startPolling()
        queueDeletePolicies.keys.forEach { queueName ->
            val parallelPollers = queueParallelPollers[queueName] as Int
            repeat(
                if (parallelPollers > 0) {
                    parallelPollers
                } else {
                    defaultParallelPollers
                }
            ) {
                queuePoller.pollQueue(queueName) { message ->
                    applyDeletePolicy(message)
                }
            }
        }
    }

    override fun destroy() {
        queuePoller.stopPolling()
    }

    private fun applyDeletePolicy(message: Message<*>): Message<*> {
        val queueName = message.headers[SqsQueue.QUEUE_NAME] as String
        val deletePolicy = queueDeletePolicies[queueName]
        val wasSuccessful = (message.headers[SqsQueue.WAS_SUCCESSFUL] as MutableBoolean).booleanValue()
        if (deletePolicy != SqsDeletePolicy.NEVER && (
                    deletePolicy == SqsDeletePolicy.ALWAYS ||
                            (deletePolicy == SqsDeletePolicy.SUCCESS && wasSuccessful)
                            || (deletePolicy == SqsDeletePolicy.FAIL && !wasSuccessful))
        ) {
            sqsTemplate.delete(queueName, message)
        }
        return message
    }
}
