package org.crossref.manifold.db

object Constants {
    const val BUFFER = "buffer"
    const val RELATIONSHIP_ASSERTION = "relationship-assertion"
    const val PROPERTY_ASSERTION = "property-assertion"
}
