package org.crossref.manifold.db

import org.crossref.manifold.db.Constants.BUFFER
import org.crossref.manifold.db.Constants.RELATIONSHIP_ASSERTION
import org.crossref.manifold.util.Constants.BATCH_SIZE
import org.crossref.manifold.util.Constants.FIXED_DELAY
import org.crossref.manifold.util.Constants.INITIAL_DELAY
import org.crossref.manifold.util.logDuration
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Repository
import java.util.concurrent.TimeUnit

@Repository
@ConditionalOnProperty(prefix = BUFFER, name = [RELATIONSHIP_ASSERTION])
class RelationshipAssertionBufferDao(
    jdbcTemplate: JdbcTemplate,
    @Value("\${$BUFFER.$RELATIONSHIP_ASSERTION.$BATCH_SIZE:1000000}") private val batchSize: Int,
) : BufferDao(jdbcTemplate) {
    @Scheduled(
        initialDelayString = "\${$BUFFER.$RELATIONSHIP_ASSERTION.$INITIAL_DELAY:0}",
        fixedDelayString = "\${$BUFFER.$RELATIONSHIP_ASSERTION.$FIXED_DELAY:60}",
        timeUnit = TimeUnit.SECONDS
    )
    override fun processBuffer() {
        logDuration(logger, "processing relationship assertion buffer") {
            processBuffer("process_buffered_relationship_assertions", batchSize)
        }
    }
}
