package org.crossref.manifold.db

import org.crossref.manifold.util.logDuration
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.jdbc.core.JdbcTemplate

abstract class BufferDao(
    private val jdbcTemplate: JdbcTemplate,
) {
    protected val logger: Logger = LoggerFactory.getLogger(this::class.java)
    abstract fun processBuffer()

    /*
     * Compatible buffer functions must use the following signature.
     *
     * process_buffered_XXXX(IN batch_size INT, OUT count_processed INT, OUT count_remaining INT)
     */
    protected fun processBuffer(processFunction: String, batchSize: Int) {
        check(processFunction.startsWith("process_buffered_")) {"The name of the function should conform."}

        var countProcessed = 0
        var countRemaining = 0
        do {
            logDuration(logger, "running $processFunction with batch size $batchSize") {
                // The tests wait for this query to complete, based on the exact spelling of this query.
                jdbcTemplate.query("SELECT * FROM $processFunction($batchSize)") {
                    countProcessed = it.getInt("count_processed")
                    countRemaining = it.getInt("count_remaining")
                }
            }

            logger.info(
                "$processFunction reports $countProcessed rows processed, $countRemaining rows remaining. " + if (countRemaining >= batchSize) {
                    "Rerunning..."
                } else {
                    "Exiting..."
                }
            )

        } while (countRemaining >= batchSize)
    }
}
