package org.crossref.manifold.db

import org.crossref.manifold.db.Constants.BUFFER
import org.crossref.manifold.db.Constants.PROPERTY_ASSERTION
import org.crossref.manifold.util.Constants.BATCH_SIZE
import org.crossref.manifold.util.Constants.FIXED_DELAY
import org.crossref.manifold.util.Constants.INITIAL_DELAY
import org.crossref.manifold.util.logDuration
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Repository
import java.util.concurrent.TimeUnit

@Repository
@ConditionalOnProperty(prefix = BUFFER, name = [PROPERTY_ASSERTION])
class PropertyAssertionBufferDao(
    jdbcTemplate: JdbcTemplate,
    @Value("\${$BUFFER.$PROPERTY_ASSERTION.$BATCH_SIZE:1000000}") private val batchSize: Int,
) : BufferDao(jdbcTemplate) {
    @Scheduled(
        initialDelayString = "\${$BUFFER.$PROPERTY_ASSERTION.$INITIAL_DELAY:0}",
        fixedDelayString = "\${$BUFFER.$PROPERTY_ASSERTION.$FIXED_DELAY:60}",
        timeUnit = TimeUnit.SECONDS
    )
    override fun processBuffer() {
        logDuration(logger, "processing property assertion buffer") {
            processBuffer("process_buffered_property_assertions", batchSize)
        }
    }
}
