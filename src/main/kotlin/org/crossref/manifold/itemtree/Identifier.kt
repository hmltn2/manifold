package org.crossref.manifold.itemtree

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import org.crossref.manifold.registries.IdentifierTypeRegistry

/**
 * An Item Identifier within an Item Tree.
 * @param uri string value of the Identifier, usually a URI.
 * @param tokenized value represented as a tokenized (prefix, suffix) representation, according to the mappings in the [IdentifierTypeRegistry].
 * @param pk primary key of the ItemIdentifier (not its corresponding Item!) in the Item Graph. This is null until it's resolved.
 */
data class Identifier(

    @JsonProperty("uri")
    @get:JsonProperty("uri")
    val uri: String,

    @JsonProperty("tokenized")
    @get:JsonProperty("tokenized")
    val tokenized: Tokenized? = null,

    @JsonIgnore
    val pk: Long? = null
) {
    companion object;

    /**
     * Return a copy of this object with the given PK.
     */
    fun withPk(newPk: Long) = Identifier(uri, tokenized, newPk)

    /**
     * Associated a tokenized (prefix, suffix) representation of the [Identifier].
     */
    fun withTokenized(newTokenized: Tokenized) = Identifier(uri, newTokenized, pk)
}

data class Tokenized(val prefix: String, val suffix: String)
