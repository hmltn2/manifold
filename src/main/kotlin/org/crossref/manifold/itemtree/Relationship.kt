package org.crossref.manifold.itemtree

import com.fasterxml.jackson.annotation.JsonProperty

data class RelationshipType(val typ: String, val pk: Long? = null) {
    companion object;

    /**
     * Return a copy of this object with the given PK.
     */
    fun withPk(newPk: Long) = RelationshipType(typ, newPk)
}

/**
 * An item Tree that records a tree, but with the asserting party recorded at each point.
 * This based on [org.crossref.manifold.itemtree.Item] tree with assertion info grafted on.
 */
data class Relationship(
    /**
     * Type of this Relationship.
     */
    @JsonProperty("relationship-type")
    @get:JsonProperty("relationship-type")
    val relTyp: String,

    /**
     * The Item object that is the object of this Relationship.
     * This is the point of recursion in the tree.
     */
    @JsonProperty("object")
    @get:JsonProperty("object")
    val obj: Item,

    /**
     * Multiple parties making this assertion.
     * Allow this to be constructed with a default asserted by no-one.
     */
    @JsonProperty("asserted-by")
    @get:JsonProperty("asserted-by")
    val assertedBy: List<Item> = emptyList(),
) {
    companion object

    /**
     * Return a copy of this object with the Item.
     */
    fun withItem(newItem: Item) = Relationship(relTyp, newItem)
}
