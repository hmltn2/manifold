package org.crossref.manifold.modules.eventdata.support.bus

import com.auth0.jwt.JWT
import com.auth0.jwt.JWTVerifier
import com.auth0.jwt.algorithms.Algorithm
import com.auth0.jwt.interfaces.DecodedJWT

class MultiJwtVerifier(
    secrets: Set<String>
) {
    private val verifiers: Set<JWTVerifier> = secrets.map {
        JWT.require(Algorithm.HMAC256(it)).build()
    }.toSet()

    fun verify(token: String?): DecodedJWT? =
        token?.let {
            verifiers.mapNotNull { verifier ->
                try {
                    verifier.verify(token)
                } catch (e: Exception) {
                    null
                }
            }.firstOrNull()
        }
}
