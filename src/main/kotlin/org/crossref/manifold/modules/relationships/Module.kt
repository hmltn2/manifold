package org.crossref.manifold.modules.relationships

import org.crossref.manifold.itemgraph.ItemDao
import org.crossref.manifold.itemgraph.RelationshipDao
import org.crossref.manifold.itemgraph.Resolver
import org.crossref.manifold.modules.Manifest
import org.crossref.manifold.modules.ModuleRegistrar
import org.crossref.manifold.registries.IdentifierTypeRegistry
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration(Module.RELATIONSHIPS)
class Module(moduleRegistrar: ModuleRegistrar) {
    companion object {
        const val RELATIONSHIPS = "relationships"
        const val API = "api"
    }

    init {
        moduleRegistrar.register(
            Manifest(
                name = RELATIONSHIPS, description = "Crossref Relationships API"
            )
        )
    }

    @Bean
    @ConditionalOnProperty(prefix = RELATIONSHIPS, name = [API])
    fun relationshipController(relationshipService: RelationshipService): RelationshipController =
        RelationshipController(relationshipService)

    @Bean
    @ConditionalOnProperty(prefix = RELATIONSHIPS, name = [API])
    fun relationshipService(
        resolver: Resolver,
        itemDao: ItemDao,
        relationshipDao: RelationshipDao,
        identifierTypeRegistry: IdentifierTypeRegistry
    ): RelationshipService =
        RelationshipService(resolver, itemDao, relationshipDao, identifierTypeRegistry)
}
