package org.crossref.manifold.modules

import com.fasterxml.jackson.databind.PropertyNamingStrategies
import org.springframework.beans.factory.DisposableBean
import org.springframework.beans.factory.InitializingBean
import org.springframework.boot.context.event.ApplicationStartedEvent
import org.springframework.context.ApplicationListener
import org.springframework.core.MethodParameter
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Service
import org.springframework.web.context.request.NativeWebRequest
import org.springframework.web.servlet.HandlerInterceptor
import org.springframework.web.servlet.HandlerMapping
import org.springframework.web.servlet.mvc.method.annotation.ServletModelAttributeMethodProcessor
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * A module manifest describes the module and what it does.
 */
class Manifest(
    /**
     * Identifier for this module.
     */
    val name: String,

    /**
     * Human-readable short description.
     */
    val description: String,
)

@Service
class ModuleRegistrar {
    val manifests = mutableSetOf<Manifest>()

    /**
     * Register a module. Every Module should call this on startup.
     */
    fun register(manifest: Manifest) {
        manifests.add(manifest)
    }
}

/**
 * Represents a block of work to be carried out when Manifold starts.
 *
 * New instances of this class should be created by and returned from @Bean annotated functions in @Configuration classes so they are identified and managed by Spring.
 *
 * This ensures they will be run asynchronously and only after the Application Context has been fully populated.
 *
 */
class StartupTask(
    private val listener: () -> Unit,
    private val destructor: () -> Unit = {},
    private val initializer: () -> Unit = {}
) : ApplicationListener<ApplicationStartedEvent>, DisposableBean, InitializingBean {
    override fun afterPropertiesSet() {
        initializer()
    }

    @Async
    override fun onApplicationEvent(event: ApplicationStartedEvent) {
        listener()
    }

    override fun destroy() {
        destructor()
    }
}

/**
 * When a URL path is supplied with a non-URL DOI, redirect back to the same path but with the full DOI URL.
 */
class NonUrlDoiInterceptor : HandlerInterceptor {
    override fun preHandle(request: HttpServletRequest, response: HttpServletResponse, handler: Any): Boolean {
        request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE)?.let {
            val maybeDoi = ((it as Map<*, *>).getOrDefault("identifier", "~") as String).substring(1)
            return if (maybeDoi.startsWith("10.")) {
                response.status = HttpServletResponse.SC_MOVED_PERMANENTLY
                response.setHeader("Location", "${request.requestURI.removeSuffix(maybeDoi)}https://doi.org/$maybeDoi")
                false
            } else {
                true
            }
        }
        return true
    }
}

/**
 * Our REST API uses kebab case request parameters but Spring MVC does not support these out of the box when
 * instantiating [org.springframework.web.bind.annotation.ModelAttribute]s.
 *
 * In conjunction with [ApiParameter] this argument resolver adds that support.
 */
class ApiParameterArgumentResolver : ServletModelAttributeMethodProcessor(false) {
    override fun supportsParameter(parameter: MethodParameter): Boolean =
        parameter.hasParameterAnnotation(ApiParameter::class.java)

    override fun resolveConstructorArgument(paramName: String, paramType: Class<*>, request: NativeWebRequest): Any? =
        super.resolveConstructorArgument(paramName, paramType, request)
            ?: request.getParameter(PropertyNamingStrategies.KebabCaseStrategy().translate(paramName))
}

/**
 * To be used in place of [org.springframework.web.bind.annotation.ModelAttribute] when support for kebab case request
 * parameters is required.
 */
@Target(AnnotationTarget.VALUE_PARAMETER)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
annotation class ApiParameter
