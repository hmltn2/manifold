package org.crossref.manifold.modules.citeprocjson

import clojure.lang.Keyword
import clojure.lang.RT
import org.crossref.cayenne
import org.crossref.manifold.itemtree.Item
import org.crossref.manifold.rendering.Configuration.CITEPROC_ENABLED
import org.crossref.manifold.rendering.Configuration.RENDERING
import org.crossref.manifold.rendering.ContentType
import org.crossref.manifold.rendering.ContentTypeRenderer
import org.crossref.manifold.util.clojure.toClj
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.stereotype.Component


@Component
@ConditionalOnProperty(prefix = RENDERING, name = [CITEPROC_ENABLED], matchIfMissing = true)
class CiteprocRenderer : ContentTypeRenderer {
    private val logger: Logger = LoggerFactory.getLogger(this::class.java)

    init {
        cayenne.boot()
    }

    override fun internalContentType(): ContentType = ContentType.CITEPROC_JSON
    override fun collection(): String = "https://id.crossref.org/collections/work"

    override fun render(itemTree: Item): String {
        logger.info("Rendering ${itemTree.pk} using ${internalContentType()}")

        val fieldsToMakeValuesKeywords = setOf("type", "subtype", "kind")
        val converted = itemTree.toClj(fieldsToMakeValuesKeywords)
        val esDoc = cayenne.itemToEsDoc(converted)
        val cleanedEsDoc = RT.dissoc(esDoc, Keyword.intern("indexed"))

        val citeproc = cayenne.esDocToCiteproc(cleanedEsDoc)
            ?: throw Exception("Couldn't find that item.")

        return cayenne.writeJsonString(citeproc)
    }
}
