package org.crossref.manifold.modules.unixml

import org.crossref.manifold.itemgraph.ItemGraph
import org.crossref.manifold.modules.unixml.support.parseUniXmlToEnvelopes
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service

@Service
class XmlSingleIngester(
    private val itemGraph: ItemGraph,
) {
    private var logger: Logger = LoggerFactory.getLogger(this::class.java)


    fun ingest(trace: String, content: String) {
        logger.info("Ingest XML $trace: parsing.")
        val envelopeBatches = parseUniXmlToEnvelopes(
            trace,
            content,
        )
        logger.info("Ingest XML $trace: ${envelopeBatches.count()} batches...")
        // Ingest all envelope batches
        envelopeBatches.forEach {
            logger.info("Ingest XML $trace: ingest batch envelope ${it.provenance.externalTrace}")
            itemGraph.ingest(it)
        }
        logger.info("Ingest XML $trace: finished.")
    }
}