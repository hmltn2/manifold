package org.crossref.manifold.bulk

import org.crossref.manifold.util.logDuration
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.stereotype.Repository

@Repository
class BulkIngestionDao(
    private val jdbcTemplate: JdbcTemplate,
) {
    private var logger: Logger = LoggerFactory.getLogger(this::class.java)

    private var itemIdentifierConstraints = mapOf(
        "identifier_type_fk" to Pair(
            "ALTER TABLE item_identifier ADD CONSTRAINT identifier_type_fk " +
                    "FOREIGN KEY (identifier_type_pk) REFERENCES identifier_type (pk)",
            "ALTER TABLE item_identifier DROP CONSTRAINT identifier_type_fk"
        ),
        "item_fk" to Pair(
            "ALTER TABLE item_identifier ADD CONSTRAINT item_fk FOREIGN KEY (item_pk) REFERENCES item (pk)",
            "ALTER TABLE item_identifier DROP CONSTRAINT item_fk"
        ),
        "item_identifier_un" to Pair(
            "ALTER TABLE item_identifier ADD CONSTRAINT item_identifier_un UNIQUE (identifier_type_pk, suffix)",
            "ALTER TABLE item_identifier DROP CONSTRAINT item_identifier_un"
        )
    )

    private var itemIdentifierIndexes = mapOf(
        "item_identifier_dup_lookup_in" to Pair(
            "CREATE INDEX item_identifier_dup_lookup_in ON item_identifier (identifier_type_pk, suffix, pk)",
            "DROP INDEX item_identifier_dup_lookup_in"
        ),
        "item_identifier_item_in" to Pair(
            "CREATE INDEX item_identifier_item_in ON item_identifier (item_pk)",
            "DROP INDEX item_identifier_item_in"
        )
    )

    fun startOfflineIdentifierWarm() {
        dropConstraints("item_identifier", itemIdentifierConstraints)
        dropConstraints("item_identifier", itemIdentifierIndexes)
    }

    /**
     * This is not transactional in order to allow other threads to observe progress.
     * For the typical entire XML corpus there was a 30% duplicate rate.
     * If this crashes it can be restarted, so there's no reason to isolate it.
     */
    fun finishOfflineIdentifierWarm() {
        logDuration(logger, "complete offline warming") {
            val preItemCount = getItemCount()
            val preIdentifierCount = getIdentifierCount()

            logger.info("De-duplicating $preItemCount Item Identifiers...")

            addConstraints("item_identifier", itemIdentifierIndexes)

            logDuration(logger, "deleting duplicate item identifiers") {
                jdbcTemplate.update(
                    "DELETE FROM item_identifier i1 WHERE pk != (" +
                            "SELECT MIN(pk) FROM item_identifier i2 " +
                            "WHERE (i2.identifier_type_pk, i2.suffix) = (i1.identifier_type_pk, i1.suffix))"
                )
                jdbcTemplate.update("VACUUM FULL item_identifier")
                jdbcTemplate.update("ANALYZE item_identifier")
            }

            logDuration(logger, "deleting duplicate items") {
                jdbcTemplate.update(
                    "DELETE FROM item WHERE pk IN (" +
                            "SELECT i1.pk FROM item i1 " +
                            "LEFT JOIN item_identifier i2 ON i1.pk = i2.item_pk " +
                            "WHERE i2.item_pk IS NULL)"
                )
                jdbcTemplate.update("VACUUM FULL item")
                jdbcTemplate.update("ANALYZE item")
            }

            addConstraints("item_identifier", itemIdentifierConstraints)

            val postItemCount = getItemCount()
            val postIdentifierCount = getIdentifierCount()

            logger.info("Reduced $preIdentifierCount identifiers to $postIdentifierCount, a reduction of ${preIdentifierCount - postIdentifierCount}")
            logger.info("Reduced $preItemCount items to $postItemCount, a reduction of ${preItemCount - postItemCount}")
        }
    }

    private fun getItemCount(): Long =
        jdbcTemplate.queryForObject("SELECT COUNT(pk) FROM item", Long::class.java) as Long

    private fun getIdentifierCount(): Long =
        jdbcTemplate.queryForObject("SELECT COUNT(pk) FROM item_identifier", Long::class.java) as Long

    private fun addConstraints(tableName: String, constraints: Map<String, Pair<String, String>>) {
        logDuration(logger, "adding constraints to $tableName") {
            constraints.forEach {
                logDuration(logger, "adding constraint ${it.key}") {
                    jdbcTemplate.update(it.value.first)
                }
            }
        }
    }

    private fun dropConstraints(tableName: String, constraints: Map<String, Pair<String, String>>) {
        logDuration(logger, "dropping constraints from $tableName") {
            constraints.forEach {
                logDuration(logger, "dropping constraint ${it.key}") {
                    jdbcTemplate.update(it.value.second)
                }
            }
        }
    }
}
