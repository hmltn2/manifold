package org.crossref.manifold.bulk

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.json.JsonMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import kotlinx.coroutines.CoroutineName
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.crossref.manifold.ingestion.EnvelopeBatch
import org.crossref.manifold.ingestion.hasAmbiguousIdentifiers
import org.crossref.manifold.ingestion.scanTarGz
import org.crossref.manifold.itemgraph.ItemDao
import org.crossref.manifold.itemgraph.ItemGraph
import org.crossref.manifold.itemgraph.RelationshipDao
import org.crossref.manifold.itemtree.Identifier
import org.crossref.manifold.itemtree.getUnambiguousUnresolvedIdentifiers
import org.crossref.manifold.modules.unixml.support.PARSE_PARALLELISM
import org.crossref.manifold.registries.IdentifierTypeRegistry
import org.crossref.manifold.util.logDuration
import org.crossref.manifold.util.partitionChannel
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.io.File
import kotlin.math.abs
import kotlin.random.Random

/**
 * Bulk ingester can read EnvelopeBatch Batch files. This is optimised for offline ingestion of hundreds of millions of
 * Envelope Batches, so it needs a lock on the database.
 */
@Service
class BulkIngestionService(
    val itemDao: ItemDao,
    val bulkIngestionDao: BulkIngestionDao,
    val relationshipDao: RelationshipDao,
    val identifierTypeRegistry: IdentifierTypeRegistry,
    val itemGraph: ItemGraph
) {
    private companion object {
        /**
         * Size of a batch of Identifiers for deduplication.
         * At 2000000, this takes about 17 GB RAM for an UniXML snapshot.
         */
        private const val WARM_DEDUPLICATE_BATCH_SIZE = 2000000

        /**
         * There are this many batches of Identifiers running in parallel.
         * This value affects the breadth of a hash bucket, i.e. a smaller number means more items in the bucket, which means
         * we can de-duplicate fewer at once.
         * It also affects memory usage, as each bucket has a fixed size of [WARM_DEDUPLICATE_BATCH_SIZE] .
         */
        private const val IDENTIFIER_PARTITIONS = 8

        /**
         * Number of threads for insertion into the database. This must not be larger than the database pool size.
         */
        private const val INSERT_PARALLELISM = 20
    }

    private var logger: Logger = LoggerFactory.getLogger(this::class.java)

    /** Ingest a collection of archive files.
     * This is optimised for speed, so requires a lock on the database. For the whole corpus, this may take days.
     * This is not recommended for running on a server performing live ingestion.
     */
    fun ingestFiles(archiveFiles: List<File>) {
        warmIdentifiers(archiveFiles)
        ingest(archiveFiles)
    }

    /**
     * Warm the Items and Identifiers in the Item Graph from the archive file.
     * The optimization will collect all unambiguous Identifiers in the file. It passes through two stages of
     * deduplication. First in batches in RAM. Then these batches are sent into the Database and de-duplicated further.
     *
     * We can parallelize the database index build so long as we don't create data dependencies between concurrent
     * transactions. In this case this means that we can insert batches of identifiers into the index in parallel so
     * long as a given Identifier will never appear in more than one concurrent batch.
     *
     * This is achieved by hash-partitioning on Identifiers, which are then put into parallel buckets. These are flushed
     * when full.
     *
     */
    private fun warmIdentifiers(archiveFiles: List<File>) {
        logDuration(logger, "Warm Identifiers from batch file") {

            // Prepare the database to let us inserting Identifiers without the unique constraint.
            bulkIngestionDao.startOfflineIdentifierWarm()

            // Within a coroutine scope, set up channels to asynchronously read the file, parse it, deduplicate and insert
            // into the database.
            runBlocking {

                // A stream of Identifiers that we find.
                // Give the channel size some overhead over the parallelism to allow buffering between stages.
                val identifiersChan = Channel<Identifier>(PARSE_PARALLELISM * 2)

                // Scan the TAR file and emit a stream of Identifiers that are used in an unambiguous context.
                val readTask = launch(CoroutineName("read-gz") + Dispatchers.IO) {
                    archiveFiles.forEach { archiveFile ->
                        readEnvelopeBatchArchive(archiveFile) {
                            sendUnambiguousIdentifiers(it, identifiersChan)
                        }
                    }
                }

                // Listen for Identifiers on the channel so they can be de-duplicated.
                val warmTask = launch(CoroutineName("warm-snapshot") + Dispatchers.IO) {
                    logger.info("Start warming identifiers across $IDENTIFIER_PARTITIONS partitions...")

                    // Hash-partition the Identifiers for further de-duplication.
                    // This function will run a number of parallel channel consumers.
                    partitionChannel(identifiersChan,
                        IDENTIFIER_PARTITIONS,
                        WARM_DEDUPLICATE_BATCH_SIZE * 2,
                        { i -> abs((i.tokenized?.hashCode() ?: 0) % IDENTIFIER_PARTITIONS) }) { index, chan ->

                        logger.info("Insert identifiers, partition $index start...")
                        insertIdentifiersFromChannel(chan)
                        logger.info("Insert identifiers, partition $index complete!")
                    }

                    logger.info("Finish ingesting identifiers from all partitions!")
                }

                readTask.join()

                identifiersChan.close()

                warmTask.join()
                logger.info("Ingestion finished for warming!")
            }

            logger.info("Indexing and deuping warming...")

            bulkIngestionDao.finishOfflineIdentifierWarm()

        }
    }

    /**
     * Ingest all the Envelope Batches as quickly as possible.
     * This relies on all Item Identifiers having been pre-warmed, which will remove transaction interdependencies.
     */
    private fun ingest(archiveFiles: List<File>) {
        try {
            logDuration(logger, "ingest assertions from archive batch file") {
                runBlocking {
                    val envelopeBatchChan = Channel<Collection<EnvelopeBatch>>(PARSE_PARALLELISM * 10)

                    val readTask = launch(CoroutineName("read-gz") + Dispatchers.IO) {
                        archiveFiles.forEach { archiveFile ->
                            readEnvelopeBatchArchive(archiveFile) {
                                // We can't ingest any ambiguously identified Items through EnvelopeBatches.
                                // More importantly, the optimization relies on this, so make errors explicit.
                                if (it.any(::hasAmbiguousIdentifiers)) {
                                    logger.warn("Found ambiguous! $it")
                                } else {
                                    envelopeBatchChan.send(it)
                                }
                            }
                        }

                    }

                    val ingestTasks = (0..INSERT_PARALLELISM).map {
                        launch(CoroutineName("ingest-xml") + Dispatchers.IO) {
                            ingestEnvelopeBatchesFromChannel(envelopeBatchChan)
                        }
                    }

                    logger.info("Wait for read to finish...")
                    readTask.join()
                    envelopeBatchChan.close()

                    logger.info("Read finished. Waiting for insert to finish...")
                    ingestTasks.forEach { it.join() }

                    logger.info("Ingest finished!")

                }
            }
        } catch (e: Exception) {
            logger.warn("Error! $e")
            e.printStackTrace()
        }
    }

    /**
     * Given a channel of [EnvelopeBatch]es ingest into the Item Graph in large batches.
     */
    private suspend fun ingestEnvelopeBatchesFromChannel(
        envelopeBatchChan: Channel<Collection<EnvelopeBatch>>,
    ) {
        logger.info("Start ingesting batches from channel $envelopeBatchChan...")

        var batches = mutableListOf<EnvelopeBatch>()

        try {
            for (envelopeBatch in envelopeBatchChan) {
                batches.addAll(envelopeBatch)
                if (batches.count() > 1000) {
                    logger.debug("Ingest batch of ${batches.count()}...")
                    assertStatements(batches)
                    logger.debug("Finish ingest batch.")
                    batches = mutableListOf()
                }
            }
            logger.info("Ingest last batch of ${batches.count()}...")
            assertStatements(batches)

        } catch (e: Exception) {
            logger.warn("Error! $e")
            e.printStackTrace()
        }

        logger.info("Finish ingesting batches from channel $envelopeBatchChan!")
    }

    /**
     * Send all Identifiers being used in an unambiguous context.
     * Looks for Items in the Provenance and Item Tree.
     */
    private suspend fun sendUnambiguousIdentifiers(
        batches: List<EnvelopeBatch>,
        identifiersChan: Channel<Identifier>
    ) {
        for (envelopeBatch in batches) {
            for (envelope in envelopeBatch.envelopes) {

                getUnambiguousUnresolvedIdentifiers(envelope.assertion.assertingParty)
                    .forEach {
                        identifiersChan.send(identifierTypeRegistry.tokenize(it))
                    }

                for (itemTree in envelope.itemTrees) {
                    getUnambiguousUnresolvedIdentifiers(itemTree).forEach {

                        // Need to tokenize first because we use that for the partitioning.
                        // Otherwise, URIs that appear different but tokenize to the same thing might
                        // find themselves in different partitions, which could cause transaction clashes.
                        val tokenized = identifierTypeRegistry.tokenize(it)

                        identifiersChan.send(tokenized)
                    }
                }
            }
        }
    }

    /**
     * Given a channel of [Identifier]s, warm them the Item Graph in chunks.
     */
    private suspend fun insertIdentifiersFromChannel(
        identifiersChan: Channel<Identifier>
    ) {
        val threadName = Thread.currentThread().name

        // First batch size is random to introduce some jitter because this is used across threads.
        // After that batch sizes will be the same.
        var batchSize = Random.nextInt(WARM_DEDUPLICATE_BATCH_SIZE)

        logger.info("> warmIdentifiersFromChannel in $threadName ")
        try {

            var identifiers = mutableSetOf<Identifier>()
            for (identifier in identifiersChan) {
                identifiers.add(identifier)

                // Although the SQL insertion is further batched, the benefit of collecting into a chunk this size
                // is that we deduplicate within this window.
                // The larger this value is, the more we deduplicate before inserting into the database. The database will
                // deduplicate the batch against the pre-existing index. Buffering here ensures that we avoid introducing
                // duplicates within the batch. It's not a problem if it does, but this helps with efficiency.
                // Specifically, we expect repeat identifiers for member ids etc.
                // Identifiers are on average 40 characters long (sample of 100 million identifiers) so assuming 16 bytes
                // per character, a buffer of 1000000 means 40 MB strings in RAM plus overhead.
                val count = identifiers.count()

                // Wait until there's a batch-worth.
                if (count >= batchSize) {
                    logger.info("Warm chunk of $count identifiers in $threadName ...")

                    val startTime = System.currentTimeMillis()

                    insertIdentifiers(identifiers)
                    val duration = (System.currentTimeMillis() - startTime) / 1000

                    // Don't divide by zero!
                    val rate = count / duration.toFloat()

                    logger.info("Warmed chunk of $count identifiers in $threadName. Took $duration seconds, $rate per second!")

                    identifiers = mutableSetOf()

                    // The first batch was a random size for jitter between parallel batches, but for every
                    // subsequent batch, it's the same size.
                    batchSize = WARM_DEDUPLICATE_BATCH_SIZE
                }
            }
            logger.info("Warm last chunk of ${identifiers.count()} identifiers in $threadName...")
            insertIdentifiers(identifiers)
            logger.info("Warmed last chunk of identifiers in $threadName!")

        } catch (e: Exception) {
            logger.warn("Error! $e")
            e.printStackTrace()
        }

        logger.info("< warmIdentifiersFromChannel in $threadName... ")
    }

    /**
     * Reader for an EnvelopeBatch Archive, as created with [org.crossref.manifold.ingestion.EnvelopeBatchArchiveWriter].
     */
    private suspend fun readEnvelopeBatchArchive(file: File, f: suspend (batches: List<EnvelopeBatch>) -> Unit) {
        /** Mapper that can read JSON files from the Tar file.
         * AUTO_CLOSE_SOURCE is important for tar streams as they are files of files, therefore have specific file-handling
         * semantics. Normal parsing would close the stream prematurely.
         */
        val mapper = JsonMapper.builder()
            .addModule(JavaTimeModule())
            .configure(JsonParser.Feature.AUTO_CLOSE_SOURCE, false)
            .build().registerKotlinModule()

        scanTarGz(file) { _, reader ->
            val myObjects: List<EnvelopeBatch?>? =
                mapper.readValue(reader, object : TypeReference<List<EnvelopeBatch?>?>() {})

            val batches = myObjects?.mapNotNull { it }
            if (batches != null) {
                f(batches)
            }
        }
    }

    /** Insert Identifiers within an Identifier warming session.
     */
    private fun insertIdentifiers(identifiers: Set<Identifier>) {
        val tokenized = identifiers.mapNotNull {
            identifierTypeRegistry.tokenize(it).tokenized
        }
        return itemDao.insertIdentifierBatch(tokenized)
    }

    /**
     * Insert assertions from a list of EnvelopeBatches within a Statement ingestion session.
     */
    private fun assertStatements(batches: List<EnvelopeBatch>) {
        for (batch in batches) {
            // Call out to ItemGraph as it's optimised for this use case too. It handles resolution.
            itemGraph.assert(batch)
        }
    }
}
