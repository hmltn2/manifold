package org.crossref.manifold.retrieval.statements

import org.crossref.manifold.itemgraph.ItemDao
import org.crossref.manifold.registries.IdentifierTypeRegistry
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException

data class PropertyStatementFilter(
    val itemPk: Long? = null,
    val partyPk: Long? = null
)

/**
 * Map query parameters into a filter, looking up the various items in the process.
 */
@Service
class PropertyStatementFilterBuilder(
    private val identifierTypeRegistry: IdentifierTypeRegistry,
    private val itemDao: ItemDao
) {
    /**
     * Given a set of optional filters (which use ItemIdentifiers and RelationshipTypes, to be resolved) construct an AssertionFilter.
     */
    fun build(
        itemIdentifier: String?,
        assertedBy: String?
    ): PropertyStatementFilter {
        val fromPk = if (itemIdentifier == null) {
            null
        } else {
            val response = itemDao.findItemAndIdentifierPk(
                identifierTypeRegistry.tokenize(
                    itemIdentifier
                )

            )?.itemPk
                ?: throw ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    "subject filter supplied but didn't recognise it"
                )
            response
        }

        val partyPk = if (assertedBy == null) {
            null
        } else {
            val response = itemDao.findItemAndIdentifierPk(

                identifierTypeRegistry.tokenize(
                    assertedBy
                )
            )
                ?.itemPk
                ?: throw ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    "assertedBy filter supplied but didn't recognise it"
                )
            response
        }

        return PropertyStatementFilter(
            itemPk = fromPk,
            partyPk = partyPk
        )
    }
}