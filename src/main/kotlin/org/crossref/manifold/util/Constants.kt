package org.crossref.manifold.util

object Constants {
    const val BATCH_SIZE = "batch-size"
    const val INITIAL_DELAY = "initial-delay"
    const val FIXED_DELAY = "fixed-delay"
}
