package org.crossref.manifold.util

import kotlinx.coroutines.CoroutineName
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.slf4j.Logger
import org.slf4j.LoggerFactory

var logger: Logger = LoggerFactory.getLogger("org.crossref.manifold.util")

/**
 * Hash-partition a channel into @param[partitions] many partitions, according to hash function @param[partition].
 * A coroutine is created for each partition and these run in parallel. The callback function @param[f] is called for
 * each message within the appropriate coroutine.
 * This function returns when the input channel is closed and each function returns. Closes the output channel when
 * done.
 */
suspend fun <I, O> partitionChannel(
    input: Channel<I>,
    partitions: Int, bufferSize: Int,
    partition: (I) -> Int,
    f: suspend (partitionNumber: Int, Channel<I>) -> O
) {
    logger.info("Setting up partition channel processing for $partitions partitions...")
    runBlocking {

        val channels = (0 until partitions).map {

            // this is needed to let the distribution function fill channels whilst their respective workers are working on them
            // otherwise the backpressure from one channel will block another
            // accumulate at least enough to fill another batch
            Channel<I>(bufferSize)
        }

        val tasks = (0 until partitions).map {
            launch(CoroutineName("partition-worker-$it") + Dispatchers.IO) {
                val channel = channels[it]
                try {

                    f(it, channel)
                } catch (e: Exception) {
                    logger.error("Error in partition $it! $e.getMessage")
                }

            }
        }

        // Keep going until input is closed.
        for (message in input) {
            val partitionI = partition(message)
            check(partitionI in 0 until partitions) { "Partitioning function may only return values in the range of the partition size, was $partitionI." }
            channels[partitionI].send(message)
        }

        // Then close channels to signal functions should stop.
        channels.forEach { it.close() }

        // Wait for all fs to finish.
        tasks.forEach { it.join() }

        logger.info("Complete up partition channel processing!")
    }
}