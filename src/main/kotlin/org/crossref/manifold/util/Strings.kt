package org.crossref.manifold.util

fun String.Companion.randomString(): String {
    val charPool = ('a'..'z') + ('A'..'Z')
    return List(6) { charPool.random() }.joinToString("")
}
