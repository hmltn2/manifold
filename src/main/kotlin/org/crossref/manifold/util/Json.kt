package org.crossref.manifold.util

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import java.io.FileNotFoundException

/** Load JSON file from resource filename or error.
 */
fun loadJsonResource(fileName: String) : JsonNode {
    val mapper = ObjectMapper()
    val inputStream = Thread.currentThread().contextClassLoader
        .getResourceAsStream(fileName) ?: throw FileNotFoundException("File $fileName missing")
    return mapper.readTree(inputStream)
}
