package org.crossref.manifold.rendering

import java.time.OffsetDateTime

data class RenderStatus(
    val pk: Long? = null,
    val itemPk: Long,
    val stale: Boolean
)

enum class ContentType(val mimeType: String) {
    CITEPROC_JSON("application/citeproc+json"),

    // This is the elastic document before the points of conversion
    // to Citeproc json
    CITEPROC_JSON_ELASTIC("application/vnd.crossref.citeproc.elastic+json"),
    CITATION_JSON("application/vnd.crossref.matching.citation+json"), ;

    override fun toString(): String = mimeType

    companion object {
        fun fromMimeType(mimeType: String): ContentType? = when (mimeType) {
            "application/citeproc+json" -> CITEPROC_JSON
            "application/vnd.crossref.citeproc.elastic+json" -> CITEPROC_JSON_ELASTIC
            "application/vnd.crossref.matching.citation+json" -> CITATION_JSON
            else -> null
        }

        /**
         * Weights the supplied @param[contentType], higher weighting is
         * deemed less applicable
         */
        fun weight(contentType: ContentType): Int = when (contentType) {
            CITEPROC_JSON_ELASTIC -> 2
            else -> 1
        }
    }
}

data class RenderedItem(
    val pk: Long? = null,
    val itemPk: Long,
    val contentType: ContentType,
    val pointer: String,
    val current: Boolean,
    val updatedAt: OffsetDateTime?,
    val hash: String? = null,
    var content: String? = null,
    val version: Long? = updatedAt?.toInstant()?.toEpochMilli(),
)
