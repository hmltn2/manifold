package org.crossref.manifold.rendering

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.PreparedStatementSetter
import org.springframework.jdbc.core.RowMapper
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional
import java.sql.Connection
import java.sql.ResultSet
import java.sql.Types

@Repository
class RenderStatusDao(
    private val jdbcTemplate: JdbcTemplate,
) {
    private val npTemplate: NamedParameterJdbcTemplate = NamedParameterJdbcTemplate(jdbcTemplate)

    private val renderStatusMapper: RowMapper<RenderStatus> = RowMapper { resultSet, _ ->
        rowMapper(resultSet)
    }

    fun rowMapper(resultSet: ResultSet): RenderStatus {
        return RenderStatus(
            itemPk = resultSet.getLong("item_pk"),
            stale = resultSet.getBoolean("stale"),
            pk = resultSet.getLong("pk"),
        )
    }

    var logger: Logger = LoggerFactory.getLogger(this::class.java)


    fun markStale(
        items: Collection<Long>,
        staleness: Boolean = true
    ) {
        val statuses = items.map { item ->
            RenderStatus(
                itemPk = item,
                stale = staleness,
            )
        }
        val props = statuses.map { renderStatus ->
            MapSqlParameterSource(
                sortedMapOf(
                    "item_pk" to renderStatus.itemPk,
                    "stale" to renderStatus.stale
                ),
            )
        }.toTypedArray()

        npTemplate.batchUpdate(
            """
            INSERT INTO item_render_status (item_pk, stale, updated_at)
            VALUES (:item_pk, :stale, NOW())
            ON CONFLICT (item_pk) DO UPDATE SET stale = :stale, updated_at = NOW()
            """,
            props,
        )
    }

    /**
     * Get the render status for an item by its @param[itemPk].
     *
     * @return The RenderStatus of @param[itemPk]. If an item_render_status row
     * does not exist for the @param[itemPk], then a RenderStatus with a default
     * stale value of false is returned
     */
    fun getRenderStatus(itemPk: Long): RenderStatus {
        val status = npTemplate.query(
            "select pk, item_pk, stale from item_render_status where item_pk = :item_pk",
            mapOf("item_pk" to itemPk),
            renderStatusMapper,
        )

        if (status.isNotEmpty()) {
            return status.first()
        }

        return RenderStatus(itemPk = itemPk, stale = false)
    }

    /**
     * Gets a list of stale items from item_render_status and applies
     * @param[f] per row, uses setFetchSize to avoid loading too many rows into
     * memory at once
     */
    @Transactional(readOnly = true)
    fun getStaleItems(f: ((renderStatus: RenderStatus) -> Unit)) {
        val preparedStatementSetter = PreparedStatementSetter { ps -> ps.fetchSize = 100 }
        jdbcTemplate.queryForStream(
            "select pk, item_pk, stale from item_render_status where stale = true",
            preparedStatementSetter,
        ) { rs: ResultSet, _: Int -> rowMapper(rs) }.forEach { f(it) }
    }

    /**
     * For a set of item PKs, trigger the staleness of all item trees that it's part of.
     * Use the Reachability index.
     */
    fun triggerStaleFromIndex(itemPks: Set<Long>) {
        logger.info("Trigger stale trees for items: $itemPks")

        val params = MapSqlParameterSource("itemPks", jdbcTemplate.execute { c: Connection ->
            c.createArrayOf("BIGINT", itemPks.toTypedArray())
        }).apply {
            registerSqlType("itemPks", Types.ARRAY)
        }

        npTemplate.query(
            "SELECT set_stale_trees_from_items(:itemPks :: BIGINT[])",
            params
        ) { rs -> rs.getInt(1) }
    }

    /**
     * Return the subset of provided Item Pks that are marked as stale.
     */
    fun filterStale(itemPks: Collection<Long>): Set<Long> =
        if (itemPks.isEmpty()) {
            emptySet()
        } else {
            npTemplate.query(
                "SELECT item_pk FROM item_render_status " +
                        "WHERE item_pk IN (:item_pks) AND stale = TRUE",
                mapOf("item_pks" to itemPks)
            ) { rs, _ -> rs.getLong("item_pk") }
        }.toSet()
}
