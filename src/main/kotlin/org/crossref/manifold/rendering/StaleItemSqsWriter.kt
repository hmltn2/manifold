package org.crossref.manifold.rendering

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.crossref.manifold.rendering.Configuration.RENDERING
import org.crossref.manifold.rendering.Configuration.STALE_QUEUE_PUSH
import org.crossref.manifold.util.Constants.BATCH_SIZE
import org.crossref.manifold.util.Constants.FIXED_DELAY
import org.crossref.manifold.util.Constants.INITIAL_DELAY
import org.crossref.messaging.aws.sqs.SqsQueue
import org.crossref.messaging.aws.sqs.SqsTemplate
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.messaging.support.MessageBuilder
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import java.util.concurrent.TimeUnit

@Component
@ConditionalOnProperty(prefix = RENDERING, name = [STALE_QUEUE_PUSH])
class StaleItemSqsWriter(
    private val renderStatusDao: RenderStatusDao,
    private val sqsTemplate: SqsTemplate,
    @Value("\${$RENDERING.$STALE_QUEUE_PUSH}") private val queue: String,
    @Value("\${$RENDERING.$BATCH_SIZE:100}") private val batchSize: Int,
) {
    var logger: Logger = LoggerFactory.getLogger(this::class.java)
    val mapper = jacksonObjectMapper()

    @Scheduled(
        initialDelayString = "\${$RENDERING.$STALE_QUEUE_PUSH.$INITIAL_DELAY:0}",
        fixedDelayString = "\${$RENDERING.$STALE_QUEUE_PUSH.$FIXED_DELAY:60}",
        timeUnit = TimeUnit.SECONDS
    )
    fun write() {
        // sqsTemplate always has a configured destination resolver.
        val sqsQueue = sqsTemplate.destinationResolver!!.resolveDestination(queue)
        val items: MutableList<Long> = mutableListOf()

        logger.info("Checking for stale items that need to be published to $queue")
        renderStatusDao.getStaleItems { rs ->
            if (items.count() < batchSize) {
                logger.trace("Batch is still less than $batchSize, adding to batch")
                items.add(rs.itemPk)
            } else {
                publish(sqsQueue, items)
                items.clear()
            }
        }

        // Process any remaining items, there may not have been enough to
        // fill the batch
        if (items.isNotEmpty()) {
            publish(sqsQueue, items)
        }
    }

    private fun publish(queue: SqsQueue, items: Collection<Long>) {
        val message = mapper.writeValueAsString(items)
        logger.debug("Publishing $message to ${queue.queueName}")
        sqsTemplate.send(queue, MessageBuilder.withPayload(message).build())
    }
}
