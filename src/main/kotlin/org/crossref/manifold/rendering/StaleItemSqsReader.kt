package org.crossref.manifold.rendering

import org.crossref.manifold.itemgraph.Collections
import org.crossref.manifold.itemgraph.Consts.DEFAULT_TREE_DEPTH
import org.crossref.manifold.itemgraph.Resolver
import org.crossref.manifold.registries.AuthorityRegistry
import org.crossref.manifold.rendering.Configuration.RENDERING
import org.crossref.manifold.rendering.Configuration.STALE_QUEUE_PULL
import org.crossref.manifold.retrieval.itemtree.ItemFetchStrategy
import org.crossref.messaging.aws.sqs.SqsListener
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.messaging.MessagingException
import org.springframework.stereotype.Component

@Component
@ConditionalOnProperty(prefix = RENDERING, name = [STALE_QUEUE_PULL])
class StaleItemSqsReader(
    private val itemTreeRenderer: ItemTreeRenderer,
    itemResolver: Resolver,
    authorityRegistry: AuthorityRegistry,
    private val collections: Collections,
    private val renderStatusDao: RenderStatusDao,
) {
    private val logger: Logger = LoggerFactory.getLogger(this::class.java)
    private val strategy = ItemFetchStrategy.fromPerspective(
        ItemFetchStrategy.DEFAULT,
        authorityRegistry.getAuthorityRootPks(itemResolver),
        authorityRegistry.getAuthorityPks(itemResolver),
        DEFAULT_TREE_DEPTH,
    )

    @SqsListener("$RENDERING.$STALE_QUEUE_PULL")
    fun onMessageReceived(itemsToRender: Collection<Long>) {
        try {
            /** Another renderer may have already covered these, or they might have been pushed to the queue more than
             * once. Filter those out so we only render when stale.
             */
            val stillStale = renderStatusDao.filterStale(itemsToRender)

            /**
             * We'll get a batch of ItemPks, which were put on the queue by StaleItemSqsWriter.
             *
             * Any kind of Item that was mentioned in a Property or Relationship assertion might
             * be put on this queue. Only those that are Collection items will have renderers, so
             * we can ignore non-Collection items when choosing renderers.
             */
            val (collectionItems, nonCollectionItems) = collections.partitionItemPks(stillStale)

            logger.info("Processing stale items: $itemsToRender from SQS. Of which still stale: $stillStale. Of which Collection Items: $collectionItems and non-Collection: $nonCollectionItems")

            // Only render Collection items.
            for (itemPk in collectionItems) {
                try {
                    itemTreeRenderer.renderAll(itemPk, strategy)
                } catch (e: Exception) {
                    // There could be errors caused by drift between database and queue.
                    // Report errors but don't mark the message as having failed.
                    logger.error("Failed to process $itemPk: ${e.message}")
                }
            }

            /**
             * All stale Items (Collection or non-Collection) may be part of the Item Trees for *other* Collection
             * items. Based on their Item Tree reachability indexes, set those stale.
             *
             * Don't render them immediately, as that might lead to parallel processors duplicating work. Instead, send
             * into the queue. They will be picked up in a scan of StaleItemSqsWriter.
             */
            renderStatusDao.triggerStaleFromIndex(stillStale)

            // Whatever happened we can set these all false.
            renderStatusDao.markStale(stillStale, false)

        } catch (exception: Exception) {
            throw MessagingException("Processing $itemsToRender failed", exception)
        }
    }
}
