package org.crossref.manifold.rendering

object Configuration {
    const val RENDERING = "rendering"
    const val RENDERER_ENABLED = "renderer-enabled"
    const val CITEPROC_ENABLED = "citeproc-enabled"
    const val WORKS_API = "works-api"
    const val ITEMS_API = "items-api"
    const val STALE_QUEUE_PULL = "stale-queue-pull"
    const val STALE_QUEUE_PUSH = "stale-queue-push"
    const val RENDER_S3_BUCKET = "render-s3-bucket"
}
