package org.crossref.manifold.rendering

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.RowMapper
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional
import java.sql.Types
import java.time.OffsetDateTime

@Repository
class RenderedItemDao(
    jdbcTemplate: JdbcTemplate,
) {
    private val npTemplate: NamedParameterJdbcTemplate = NamedParameterJdbcTemplate(jdbcTemplate)

    private val renderedItemMapper: RowMapper<RenderedItem> = RowMapper { rs, _ ->
        RenderedItem(
            pk = rs.getLong("pk"),
            itemPk = rs.getLong("item_pk"),
            contentType = ContentType.fromMimeType(rs.getString("content_type"))!!,
            current = rs.getBoolean("current"),
            pointer = rs.getString("pointer"),
            hash = rs.getString("hash"),
            updatedAt = rs.getObject("updated_at", OffsetDateTime::class.java),
            version = rs.getLong("version"),
        )
    }

    var logger: Logger = LoggerFactory.getLogger(this::class.java)

    @Transactional
    fun createRenderedItems(
        renderedItems: Collection<RenderedItem>,
    ) {
        val props = renderedItems.map { renderedItem ->
            MapSqlParameterSource(
                mapOf(
                    "item_pk" to renderedItem.itemPk,
                    "current" to renderedItem.current,
                    "content_type" to renderedItem.contentType.mimeType,
                    "pointer" to renderedItem.pointer,
                    "hash" to renderedItem.hash,
                    "updated_at" to renderedItem.updatedAt,
                    "version" to renderedItem.version,
                ),
            ).apply { registerSqlType("updated_at", Types.TIMESTAMP_WITH_TIMEZONE) }
        }.toTypedArray()

        npTemplate.batchUpdate(
            """
            UPDATE rendered_item 
            SET current = false
            WHERE item_pk = :item_pk and content_type = :content_type
            """,
            props,
        )

        npTemplate.batchUpdate(
            """
            INSERT INTO rendered_item ( item_pk, content_type, pointer, hash, current, updated_at, version )
            VALUES ( :item_pk, :content_type, :pointer, :hash, :current, :updated_at, :version );
            """,
            props,
        )
    }

    fun getRenderedItem(itemPk: Long, contentType: ContentType): RenderedItem? {
        val renderedItems = npTemplate.query(
            """
            select pk, item_pk, content_type, pointer, hash, current, updated_at, version
            from rendered_item 
            where item_pk = :item_pk and content_type = :content_type and current = true
            """,
            mapOf(
                "item_pk" to itemPk,
                "content_type" to contentType.mimeType,
            ),
            renderedItemMapper,
        )

        return renderedItems.firstOrNull()
    }

    fun getRenderedItemVersion(itemPk: Long, contentType: ContentType, version: Long): RenderedItem? {
        val renderedItems = npTemplate.query(
            """
            select pk, item_pk, content_type, pointer, hash, current, updated_at, version
            from rendered_item 
            where item_pk = :item_pk and content_type = :content_type and version = :version
            """,
            mapOf(
                "item_pk" to itemPk,
                "content_type" to contentType.mimeType,
                "version" to version,
            ),
            renderedItemMapper,
        )

        return renderedItems.firstOrNull()
    }

    /**
     * Gets all current rendered items for @param[itemPk], where current
     * means the latest version, there should only be one per content type
     */
    fun getRenderedItems(itemPk: Long): Collection<RenderedItem?> {
        return npTemplate.query(
            """
            select pk, item_pk, content_type, pointer, hash, current, updated_at, version
            from rendered_item
            where item_pk = :item_pk and current = true
            """,
            mapOf(
                "item_pk" to itemPk,
            ),
            renderedItemMapper,
        )
    }

    /**
     * Gets all rendered items (current or historical) for @param[itemPk], where current
     * means the latest version, there should only be one per content type
     */
    fun getHistoricalRenderedItems(itemPk: Long, contentType: String): Collection<RenderedItem> {
        return npTemplate.query(
            """
            select pk, item_pk, content_type, pointer, hash, current, updated_at, version
            from rendered_item
            where item_pk = :item_pk and content_type = :content_type
            """,
            mapOf(
                "item_pk" to itemPk,
                "content_type" to contentType
            ),
            renderedItemMapper,
        )
    }


    /**
     * Gets the render history of @param[itemPk] by optional @param[contentType] and ordered
     * by the renders last updated date. Use optional @param[cursor] and @param[size] to page through render
     * items
     */
    fun getRenderedItemHistory(
        itemPk: Long,
        contentType: ContentType? = null,
        cursor: Long? = null,
        size: Int? = null,
    ): Collection<RenderedItem> {
        val query = StringBuilder()
        query.append(
            """
            select pk, item_pk, content_type, pointer, hash, current, updated_at, version
            from rendered_item 
            where item_pk = :item_pk
            """,
        )

        contentType?.let { query.append(" and content_type = :content_type ") }
        cursor?.let { query.append(" and pk < :cursor ") }

        query.append(" order by updated_at desc")

        size?.let { query.append(" limit :size") }

        logger.info(query.toString())

        return npTemplate.query(
            query.toString(),
            mapOf(
                "item_pk" to itemPk,
                "content_type" to contentType?.mimeType,
                "cursor" to cursor,
                "size" to size,
            ),
            renderedItemMapper,
        )
    }

    /**
     * Gets @param[size] of the most recent rendered items by
     * optional @param[contentType]. Use optional @param[cursor] to page through
     * render items
     */
    fun getRecentRenderedItems(
        size: Int,
        contentType: ContentType? = null,
        cursor: Long? = null,
    ): Collection<RenderedItem> {
        val query = StringBuilder()
        query.append(
            """
            select pk, item_pk, content_type, pointer, hash, current, updated_at, version
            from rendered_item 
            """,
        )

        logger.info(cursor?.toString())

        contentType?.let {
            query.append(" where content_type = :content_type ")
        }

        cursor?.let {
            val operator = if (contentType == null) "where" else "and"
            query.append(" $operator pk < :cursor ")
        }

        query.append(" order by updated_at desc limit :size")

        return npTemplate.query(
            query.toString(),
            mapOf(
                "content_type" to contentType?.mimeType,
                "size" to size,
                "cursor" to cursor,
            ),
            renderedItemMapper,
        )
    }
}
