package org.crossref.manifold.ingestion

/**
 * The various stages of ingesting an [EnvelopeBatch] into the Item Graph.
 * These numbers are logged in the `ingestion_message` SQL table, so they must be stable.
 */
enum class Stage(val value: Int) {
    /**
     * Started work on a batch of Envelopes.
     */
    START_BATCH(1),

    /**
     * Completed work on a batch of Envelopes.
     */
    COMPLETE_BATCH(10),
}