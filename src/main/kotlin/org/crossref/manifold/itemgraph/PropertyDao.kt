package org.crossref.manifold.itemgraph

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ObjectNode
import org.crossref.manifold.ingestion.EnvelopeBatchProvenance
import org.crossref.manifold.retrieval.statements.PropertyStatementFilter
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.RowMapper
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Repository
import java.sql.Types
import java.time.OffsetDateTime

/**
 * DAO for Property statements within the Item Graph.
 */
@Repository
class PropertyDao(
    jdbcTemplate: JdbcTemplate
) {
    private val npTemplate: NamedParameterJdbcTemplate = NamedParameterJdbcTemplate(jdbcTemplate)

    private val propStmtMapper: RowMapper<PropertyAssertionStatement> = RowMapper { rs, _ ->
        val pk = rs.getLong("property_assertion_pk")
        PropertyAssertionStatement(
            pk = if (rs.wasNull()) null else pk,
            itemPk = rs.getLong("item_pk"),
            assertingPartyPk = rs.getLong("party_pk"),
            values = parseJson(rs.getString("values")),
            assertedAt = rs.getObject("asserted_at", OffsetDateTime::class.java)
        )
    }

    /**
     * Insert [PropertyAssertionStatement] into the Item Graph as the latest.
     * These can be from multiple envelopes.
     * Input is pairs of Statement and the Envelope PK.
     */
    fun assertPropertyStatements(
        statements: Collection<Pair<PropertyAssertionStatement, Long>>
    ) {
        val params = statements.map { (statement, envelopePk) ->
            MapSqlParameterSource(
                mapOf(
                    "asserted_at" to statement.assertedAt,
                    "party_pk" to statement.assertingPartyPk,
                    "item_pk" to statement.itemPk,
                    "values" to statement.serialized(),
                    "envelope_pk" to envelopePk
                )
            ).apply { registerSqlType("asserted_at", Types.TIMESTAMP_WITH_TIMEZONE) }
        }.toTypedArray()

        npTemplate.batchUpdate(
            "insert into property_assertion_buffer (" +
                    "asserted_at, party_pk, item_pk, values, envelope_pk " +
                    ") values (" +
                    ":asserted_at, :party_pk, :item_pk, :values ::jsonb, :envelope_pk" +
                    ")",
            params
        )
    }

    /**
     * For a set of Item PKs, get all Property statements asserted by the given Party PKs.
     */
    fun getCurrentSubjPropertyStatements(partyPks: Collection<Long>, itemPks: Set<Long>): Set<PropertyAssertionStatement> =
        // SQL can't cope with empty lists and the result would be empty in any case.
        if (partyPks.isEmpty() || itemPks.isEmpty()) {
            emptySet()
        } else {
            npTemplate.query(
                "SELECT asserted_at, party_pk, item_pk, values, NULL AS property_assertion_pk " +
                        "FROM property_assertion_current " +
                        "WHERE item_pk IN (:item_pks) " +
                        "AND party_pk IN (:party_pks) ",
                mapOf("party_pks" to partyPks, "item_pks" to itemPks), propStmtMapper
            ).toSet()
        }

    /**
     * For a set of Item PKs, get all Property statements asserted by any Party.
     */
    fun getCurrentSubjPropertyStatements(itemPks: Set<Long>): Set<PropertyAssertionStatement> =
        // SQL can't cope with empty lists and the result would be empty in any case.
        if (itemPks.isEmpty()) {
            emptySet()
        } else {
            npTemplate.query(
                "SELECT asserted_at, party_pk, item_pk, values, NULL AS property_assertion_pk " +
                        "FROM property_assertion_current " +
                        "WHERE item_pk IN (:item_pks) ",
                mapOf("item_pks" to itemPks), propStmtMapper
            ).toSet()
        }

    /**
     * For a set of Item PKs, get all Property statements asserted by any Party.
     */
    fun getAllStatements(itemPk: Long, partyPk: Long, current: Boolean = false): Set<PropertyAssertionStatement> =
        npTemplate.query(
            "SELECT asserted_at, party_pk, item_pk, values, NULL AS property_assertion_pk " +
                    "FROM property_assertion " +
                    "WHERE current = :current " +
                    "AND item_pk IN (:item_pks) ",
            mapOf(
                "current" to current,
                "item_pks" to itemPk,
                "party_pk" to partyPk,
            ), propStmtMapper
        ).toSet()

    /**
     * Fetch a page of Statement Items starting from the given Statement Pk.
     * Return an optional starting Statement Pk if there are more items.
     */
    fun getPropertyStatementRange(
        startPk: Long,
        count: Int,
        filter: PropertyStatementFilter
    ): List<Pair<PropertyAssertionStatement, EnvelopeBatchProvenance>> =
        npTemplate.query(
            "SELECT pa.pk AS property_assertion_pk, item_pk, party_pk, values, asserted_at, " +
                    "eb.pk as envelope_batch_pk, user_agent, ext_trace " +
                    "FROM property_assertion pa " +
                    "JOIN envelope e ON pa.envelope_pk = e.pk " +
                    "JOIN envelope_batch eb ON e.envelope_batch_pk = eb.pk " +
                    "WHERE pk >= :start_pk " +
                    (if (filter.itemPk != null) {
                        "AND item_pk = :item_pk "
                    } else "") +
                    (if (filter.partyPk != null) {
                        "AND party_pk = :party_pk "
                    } else "") +
                    "LIMIT :count",
            mapOf(
                "start_pk" to startPk,
                "count" to count,
                "item_pk" to filter.itemPk,
                "party_pk" to filter.partyPk
            )
        ) { rs, rowNum ->
            // These are our own RowMapper implementations and we know they don't return null.
            Pair(propStmtMapper.mapRow(rs, rowNum)!!, envBatchProvMapper.mapRow(rs, rowNum)!!)
        }

    private fun parseJson(input: String): ObjectNode = ObjectMapper().reader().readTree(input).deepCopy()
}
