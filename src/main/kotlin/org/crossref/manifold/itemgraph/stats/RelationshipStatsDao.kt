package org.crossref.manifold.itemgraph.stats

import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.stereotype.Repository
import java.io.File

@Repository
class RelationshipStatsDao(
    private val jdbcTemplate: JdbcTemplate,
) {
    /**
     * Get the current statistics around the data stored in the graph.
     */
    fun getStats(): ManifoldStats =
        ManifoldStats(
            numItems = requireNotNull(jdbcTemplate.queryForObject("select count(pk) from item;", Long::class.java)),

            totalRelationshipStatements = requireNotNull(
                jdbcTemplate.queryForObject(
                    "select count(pk) from relationship_assertion;",
                    Long::class.java
                )
            ),

            totalRelationshipStatementSummaries = requireNotNull(
                jdbcTemplate.queryForObject(
                    "select count(pk) from relationship_assertion_current",
                    Long::class.java
                )
            ),

            totalPropertyStatements = requireNotNull(
                jdbcTemplate.queryForObject(
                    "select count(pk) from property_assertion;",
                    Long::class.java
                )
            ),

            totalPropertyStatementSummaries = requireNotNull(
                jdbcTemplate.queryForObject(
                    "select count(pk) from property_assertion_current",
                    Long::class.java
                )
            ),

            numItemIdentifiers = requireNotNull(
                jdbcTemplate.queryForObject(
                    "select count(pk) from item_identifier;",
                    Long::class.java
                )
            ),

            numRelationshipTypes = requireNotNull(
                jdbcTemplate.queryForObject(
                    "select count(pk) from relationship_type;",
                    Long::class.java
                )
            ),

            numEnvelopes = requireNotNull(
                jdbcTemplate.queryForObject(
                    "select count(pk) from envelope;",
                    Long::class.java
                )
            ),

            numEnvelopeBatches = requireNotNull(
                jdbcTemplate.queryForObject(
                    "select count(pk) from envelope_batch;",
                    Long::class.java
                )
            ),
        )

    /**
     * For debugging purposes, dump the DB to a file.
     */
    fun dumpSql(filename: String) =
        jdbcTemplate.execute("SCRIPT to '${filename}'")

    /**
     * For debugging purposes, dump the DB to a GraphViz DOT file.
     * This dumps all Items and Relationships, not Statements.
     */
    fun dbToGraphViz(filename: String) {
        File(filename).bufferedWriter().use { out ->
            out.write("digraph relations {")

            out.write("rankdir = \"LR\"")

            jdbcTemplate.query("SELECT pk from item") { rs ->
                val pk = rs.getLong("pk")
                out.write("  I${pk} [shape=circle, label=\"Item ${pk}\"] \n")
            }

            jdbcTemplate.query("SELECT from_item_pk, to_item_pk, relationship_type.value as relationship_type_name from relationship join relationship_type on relationship_type_pk = relationship_type.pk") { rs ->
                out.write(
                    "  I${rs.getLong("from_item_pk")} -> I${rs.getLong("to_item_pk")} [shape=circle, label=\"${
                        rs.getString(
                            "relationship_type_name"
                        )
                    }\"] \n"
                )
            }

            jdbcTemplate.query("SELECT pk, value, item_pk from item_identifier") { rs ->
                val identifierPk = rs.getString("pk")
                out.write("  ID${identifierPk} [shape=rect, label=\"${rs.getString("value")}\"] \n")
                out.write("  ID${identifierPk} -> I${rs.getLong("item_pk")} \n")
            }

            out.write("}")
        }
    }
}
