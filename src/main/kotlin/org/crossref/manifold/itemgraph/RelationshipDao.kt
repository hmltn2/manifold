package org.crossref.manifold.itemgraph

import org.crossref.manifold.api.RelationshipTypesView
import org.crossref.manifold.db.Fetched
import org.crossref.manifold.ingestion.EnvelopeBatchProvenance
import org.crossref.manifold.itemgraph.Configuration.MAX_RETURNED_ROWS
import org.crossref.manifold.registries.RelationshipTypeRegistry
import org.crossref.manifold.retrieval.statements.RelationshipStatementFilter
import org.crossref.manifold.util.randomString
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.RowMapper
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.jdbc.core.simple.SimpleJdbcInsert
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional
import java.sql.Types
import java.time.OffsetDateTime
import kotlin.math.max
import kotlin.math.min

/**
 * DAO for Property statements within the Item Graph.
 * Contains a cache of Relationship Types to map them in and out of the database because the vocabulary is small and
 * well known, and this is a very hot data path.
 */
@Repository
class RelationshipDao(
    private val jdbcTemplate: JdbcTemplate,
    @Value("\${${MAX_RETURNED_ROWS}:1000}") private val maxReturnedRows: Int
) {
    private val npTemplate: NamedParameterJdbcTemplate = NamedParameterJdbcTemplate(jdbcTemplate)

    private val relAssStmtMapper: RowMapper<RelationshipAssertionStatement> = RowMapper { rs, _ ->
        val pk = rs.getLong("relationship_assertion_pk")
        RelationshipAssertionStatement(
            fromItemPk = rs.getLong("from_item_pk"),
            relationshipType = rs.getString("relationship_type_value"),
            toItemPk = rs.getLong("to_item_pk"),
            assertingPartyPk = rs.getLong("party_pk"),
            status = rs.getBoolean("state"),
            assertedAt = rs.getObject("asserted_at", OffsetDateTime::class.java),
            pk = if (rs.wasNull()) null else pk
        )
    }

    private val relStmtMapper: RowMapper<RelationshipStatement> = RowMapper { rs, _ ->
        val pk = rs.getLong("relationship_pk")
        RelationshipStatement(
            fromItemPk = rs.getLong("from_item_pk"),
            relationshipType = rs.getString("relationship_type_value"),
            toItemPk = rs.getLong("to_item_pk"),
            pk = if (rs.wasNull()) null else pk
        )
    }

    private val logger: Logger = LoggerFactory.getLogger(this::class.java)

    /**
     * Retrieve the pk for this RelType, if known.
     */
    fun resolveRelationshipTypeRO(relationshipType: String): Long? {
        val params = mapOf("value" to relationshipType)

        return npTemplate.queryForList(
            "select pk from relationship_type where value = :value",
            params,
            Long::class.java
        ).firstOrNull()?.toLong()
    }

    /**
     * Retrieve the pk for this RelType.
     * Create if necessary.
     */
    fun resolveRelationshipType(relationshipType: String): Long {
        val params = mapOf("value" to relationshipType)

        return resolveRelationshipTypeRO(relationshipType)
            ?: SimpleJdbcInsert(jdbcTemplate).withTableName("relationship_type")
                .apply { usingGeneratedKeyColumns("pk") }
                .executeAndReturnKey(
                    params
                ).toLong()
    }

    /** Insert the given [RelationshipAssertionStatement]s into the Item Graph. These can be from more than one Envelope.
     * @param[relationshipAssertionStatements] collection of Pairs of Relationship Statement and their corresponding Envelope PK.
     */
    fun assertRelationships(
        relationshipAssertionStatements: Collection<Pair<RelationshipAssertionStatement, Long>>,
        relationshipTypeRegistry: RelationshipTypeRegistry,
    ) {

        // Resolve the relationship types into known non-null relationship type PKs.
        // If there are any relationship types that aren't recognised then they are discarded at this point.
        // Carry the Envelope Batches through.
        val withRelTypePks: List<Triple<RelationshipAssertionStatement, Long?, Long>> =
            relationshipAssertionStatements.mapNotNull { (statement, envelopePk) ->

                val resolved = relationshipTypeRegistry.resolve(statement.relationshipType)
                if (resolved == null) {
                    logger.error("Didn't recognise relationship type: ${statement.relationshipType}")
                    null
                } else {
                    Triple(statement, envelopePk, resolved)
                }
            }

        val props = withRelTypePks.map { (statement, envelopePk, relationshipTypePk) ->
            MapSqlParameterSource(
                mapOf(
                    "asserted_at" to statement.assertedAt,
                    "party_pk" to statement.assertingPartyPk,
                    "from_item_pk" to statement.fromItemPk,
                    "to_item_pk" to statement.toItemPk,
                    "relationship_type_pk" to relationshipTypePk,
                    "envelope_pk" to envelopePk,
                    "state" to statement.status,
                )
            ).apply {
                registerSqlType("asserted_at", Types.TIMESTAMP_WITH_TIMEZONE)
            }
        }.toTypedArray()
        npTemplate.batchUpdate(
            """
                INSERT INTO relationship_assertion_buffer (
                    asserted_at, 
                    party_pk, 
                    from_item_pk, 
                    to_item_pk, 
                    relationship_type_pk, 
                    state, 
                    envelope_pk
                ) VALUES (
                    :asserted_at,
                    :party_pk,
                    :from_item_pk,
                    :to_item_pk,
                    :relationship_type_pk,
                    :state,
                    :envelope_pk
                )
            """,
            props
        )
    }

    fun getRelationshipTypes(): RelationshipTypesView =
        RelationshipTypesView(jdbcTemplate.queryForList("select value from relationship_type", String::class.java))

    /**
     * For a set of Relationship PKs, retrieve those that are currently already asserted by the Party Item PK.
     */
    fun getCurrentSubjRelationshipStatements(
        partyPks: Collection<Long>,
        subjectItemPks: Set<Long>,
    ): Set<RelationshipAssertionStatement> =
        // SQL can't cope with empty lists.
        if (partyPks.isEmpty() || subjectItemPks.isEmpty()) {
            emptySet()
        } else {
            npTemplate.query(
                "SELECT asserted_at, from_item_pk, rt.value AS relationship_type_value, to_item_pk, party_pk, " +
                        "state, NULL AS relationship_assertion_pk " +
                        "FROM relationship_assertion_current ra " +
                        "JOIN relationship_type rt ON rt.pk = ra.relationship_type_pk " +
                        "WHERE from_item_pk IN (:from_item_pks) " +
                        "AND party_pk IN (:party_pks) " +
                        "AND state",
                mapOf("party_pks" to partyPks, "from_item_pks" to subjectItemPks), relAssStmtMapper
            ).toSet()
        }

    /**
     * For a set of Relationship PKs, get those asserted by anyone.
     */
    fun getCurrentSubjRelationshipStatements(subjectItemPks: Set<Long>): Set<RelationshipAssertionStatement> =
        // SQL can't cope with empty lists.
        if (subjectItemPks.isEmpty()) {
            emptySet()
        } else {
            npTemplate.query(
                "SELECT ra.pk AS relationship_assertion_pk, asserted_at, from_item_pk, " +
                        "rt.value AS relationship_type_value, to_item_pk, party_pk, state " +
                        "FROM relationship_assertion_current ra " +
                        "JOIN relationship_type rt ON rt.pk = ra.relationship_type_pk " +
                        "WHERE from_item_pk in (:from_item_pks) " +
                        "AND state",
                mapOf("from_item_pks" to subjectItemPks), relAssStmtMapper
            ).toSet()
        }

    /** Get all statements, current or not.
     */
    fun getAllStatements(
        partyPk: Long,
        fromPk: Long,
        relationshipTypePk: Long,
        toPk: Long,
        state: Boolean = true,
        current: Boolean = true,
    ): Set<RelationshipAssertionStatement> =
        npTemplate.query(
            "SELECT asserted_at, from_item_pk, rt.value AS relationship_type_value, to_item_pk, party_pk, " +
                    "state, NULL AS relationship_assertion_pk " +
                    "FROM relationship_assertion ra " +
                    "JOIN relationship_type rt ON rt.pk = ra.relationship_type_pk " +
                    "WHERE from_item_pk = :from_item_pk " +
                    "AND to_item_pk = :to_item_pk " +
                    (if (current) "AND " else "AND NOT ") + "current " +
                    "AND relationship_type_pk = :relationship_type_pk " +
                    "AND party_pk = :party_pk " +
                    "AND state = :state",
            mapOf(
                "party_pk" to partyPk,
                "from_item_pk" to fromPk,
                "relationship_type_pk" to relationshipTypePk,
                "to_item_pk" to toPk,
                "state" to state
            ), relAssStmtMapper
        ).toSet()

    /**
     * Find current Relationship Statements to the Object, asserted by the Party.
     * I.e. Match triples < subjects , relationships , * >
     */
    fun getCurrentSubjRelationshipStatements(
        partyPks: Collection<Long>,
        subjectItemPks: Set<Long>,
        relationshipTypePks: Set<Long>,
    ): Set<RelationshipAssertionStatement> {
        // SQL can't cope with empty lists.
        return if (partyPks.isEmpty() || subjectItemPks.isEmpty() || relationshipTypePks.isEmpty()) {
            emptySet()
        } else {
            npTemplate.query(
                "SELECT asserted_at, from_item_pk, rt.value AS relationship_type_value, to_item_pk, party_pk, " +
                        "state, NULL AS relationship_assertion_pk " +
                        "FROM relationship_assertion_current ra " +
                        "JOIN relationship_type rt ON rt.pk = ra.relationship_type_pk " +
                        "WHERE from_item_pk in (:from_item_pks) " +
                        "AND relationship_type_pk IN (:relationship_type_pks) " +
                        "AND party_pk IN (:party_pks) " +
                        "AND state",
                mapOf(
                    "party_pks" to partyPks,
                    "from_item_pks" to subjectItemPks,
                    "relationship_type_pks" to relationshipTypePks
                ), relAssStmtMapper
            ).toSet()
        }
    }

    /**
     * Fetch a page of Statement Items starting from the given Statement Pk.
     * Return an optional starting Statement Pk if there are more items.
     */
    fun getRelationshipStatementRange(
        startPk: Long,
        count: Int,
        filter: RelationshipStatementFilter,
    ): List<Pair<RelationshipAssertionStatement, EnvelopeBatchProvenance>> =
        npTemplate.query(
            "SELECT ra.pk as relationship_assertion_pk, from_item_pk, to_item_pk, party_pk, state, asserted_at, " +
                    "value as relationship_type_value, eb.pk as envelope_batch_pk, user_agent, ext_trace " +
                    "FROM relationship_assertion ra " +
                    "JOIN envelope e ON ra.envelope_pk = e.pk " +
                    "JOIN envelope_batch eb ON e.envelope_batch_pk = eb.pk " +
                    "JOIN relationship_type rt ON rt.pk = ra.relationship_type_pk " +
                    "WHERE ra.pk >= :start_pk " +
                    (if (filter.subjectPk != null) {
                        "AND from_item_pk = :from_item_pk "
                    } else "") +
                    (if (filter.objectPk != null) {
                        "AND to_item_pk = :to_item_pk "
                    } else "") +
                    (if (filter.relationshipTypePk != null) {
                        "AND relationship_type_pk = :relationship_type_pk "
                    } else "") +
                    (if (filter.partyPk != null) {
                        "AND party_pk = :party_pk "
                    } else "") +
                    "ORDER BY relationship_assertion_pk LIMIT :count",
            mapOf(
                "start_pk" to startPk,
                "count" to count,
                "from_item_pk" to filter.subjectPk,
                "to_item_pk" to filter.objectPk,
                "relationship_type_pk" to filter.relationshipTypePk,
                "party_pk" to filter.partyPk
            )
        ) { rs, rowNum ->
            // These are our own RowMapper implementations and we know they don't return null.
            Pair(relAssStmtMapper.mapRow(rs, rowNum)!!, envBatchProvMapper.mapRow(rs, rowNum)!!)
        }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    fun fetchRelationships(
        cursor: String?,
        itemPk: Long?,
        fromItemPk: Long?,
        toItemPk: Long?,
        relationshipType: String?,
        rows: Int?,
    ): Fetched<RelationshipStatement> {
        val sql =
            """
            SELECT r.pk  AS relationship_pk,
                r.from_item_pk,
                r.to_item_pk,
                rt.value AS relationship_type_value
            FROM relationship r
                     JOIN relationship_type rt ON r.relationship_type_pk = rt.pk
            """ +
                    (if (itemPk != null) "AND (r.from_item_pk = :itemPk OR r.to_item_pk = :itemPk) " else "") +
                    (if (fromItemPk != null) "AND r.from_item_pk = :fromItemPk " else "") +
                    (if (toItemPk != null) "AND r.to_item_pk = :toItemPk " else "") +
                    (if (relationshipType != null) "AND rt.value = :relationshipType " else "") +
                    (if (cursor != null && cursor != "*") "AND r.pk > :lastKey " else "")
        val rowsToFetch =  min(max(rows ?: maxReturnedRows, 0), maxReturnedRows)
        if (cursor == null || cursor == "*") {
            val params = getParams(itemPk, fromItemPk, toItemPk, relationshipType)
            // A count query will not return null.
            val totalRows = npTemplate.queryForObject(
                "SELECT count(*) FROM ($sql) counted", params, Integer::class.java,
            )?.toInt()!!
            val fetched = npTemplate.query("$sql ORDER by relationship_pk LIMIT $rowsToFetch", params, relStmtMapper)
            if (fetched.isEmpty()) {
                return Fetched(emptyList(), 0, rowsToFetch, null)
            }
            var nextCursor: String? = null
            if (cursor == "*") {
                // Retrieved relationship statements will always have a primary key.
                val lastKey = fetched.last().pk!!
                nextCursor = String.randomString()
                npTemplate.update(
                    "INSERT INTO open_cursors (cursor_name, row_count, created_at, last_used_at, last_key) " +
                            "VALUES (:nextCursor, :totalRows, NOW(), NOW(), :lastKey)",
                    mapOf(
                        "nextCursor" to nextCursor,
                        "totalRows" to totalRows,
                        "lastKey" to lastKey
                    )
                )
            }
            return Fetched(fetched, totalRows, rowsToFetch, nextCursor)
        } else {
            var fetched: List<RelationshipStatement> = emptyList()
            var totalRows = 0
            jdbcTemplate.query(
                "SELECT row_count, created_at, last_key FROM open_cursors WHERE cursor_name = ? FOR UPDATE",
                { rs ->
                    // Only one row will be returned as cursor_name is unique.
                    val params = getParams(itemPk, fromItemPk, toItemPk, relationshipType).apply {
                        addValue("lastKey", rs.getLong("last_key"))
                    }
                    totalRows = rs.getInt("row_count")
                    fetched =
                        npTemplate.query("$sql ORDER by relationship_pk LIMIT $rowsToFetch", params, relStmtMapper)
                },
                cursor
            )
            if (fetched.isEmpty()) {
                return Fetched(emptyList(), 0, rowsToFetch, null)
            }
            // Retrieved relationship statements will always have a primary key.
            val lastKey = fetched.last().pk!!
            val nextCursor = String.randomString()
            npTemplate.update(
                "UPDATE open_cursors " +
                        "SET cursor_name = :nextCursor, last_used_at = NOW(), last_key = :lastKey " +
                        "WHERE cursor_name = :cursor",
                mapOf(
                    "nextCursor" to nextCursor,
                    "lastKey" to lastKey,
                    "cursor" to cursor
                )
            )
            return Fetched(fetched, totalRows, rowsToFetch, nextCursor)
        }
    }

    private fun getParams(
        itemPk: Long?,
        fromItemPk: Long?,
        toItemPk: Long?,
        relationshipType: String?,
    ): MapSqlParameterSource = MapSqlParameterSource().apply {
        if (itemPk != null) {
            addValue("itemPk", itemPk)
        }
        if (fromItemPk != null) {
            addValue("fromItemPk", fromItemPk)
        }
        if (toItemPk != null) {
            addValue("toItemPk", toItemPk)
        }
        if (relationshipType != null) {
            addValue("relationshipType", relationshipType)
        }
    }
}
