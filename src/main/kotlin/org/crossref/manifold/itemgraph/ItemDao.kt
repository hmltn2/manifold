package org.crossref.manifold.itemgraph

import org.crossref.manifold.itemtree.Tokenized
import org.crossref.manifold.registries.IdentifierTypeRegistry
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.jdbc.support.GeneratedKeyHolder
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional
import java.sql.ResultSet
import java.sql.Types
import java.time.OffsetDateTime

/** Pair of Identifier PK and corresponding Item PK.
 * A [Pair<Long, Long>] would be confusing and error-prone.
 */
data class IdentifierPkItemPk(val identifierPk: Long, val itemPk: Long)

/** DAO for Items and their Identifiers. Because Items are retrieved by their Identifiers these are grouped into the
 * same DAO.
 */
@Repository
class ItemDao(
    jdbcTemplate: JdbcTemplate,
    private val identifierTypeRegistry: IdentifierTypeRegistry
) {
    private val npTemplate: NamedParameterJdbcTemplate = NamedParameterJdbcTemplate(jdbcTemplate)
    private val logger: Logger = LoggerFactory.getLogger(this::class.java)

    /**
     * Create a new Item and return its PK.
     * No other information stored (yet).
     * @return The new Item PK.
     */
    @Transactional(propagation = Propagation.SUPPORTS)
    fun insertNewItem(): Long {
        val keyHolder = GeneratedKeyHolder()

        val paramSource = MapSqlParameterSource().apply {
            addValue(
                "created_at",
                OffsetDateTime.now(),
                Types.TIMESTAMP_WITH_TIMEZONE
            )
        }

        npTemplate.update(
            "INSERT INTO item (created_at) VALUES (:created_at)",
            paramSource,
            keyHolder,
            arrayOf("pk")
        )

        // If this returns a null there was a problem creating the row, which is a showstopper.
        return keyHolder.key!!.toLong()
    }

    /**
     * If the ItemIdentifier exists, add its PK and return, along with its Item's PK.
     * If not, return the ItemIdentifier unchanged.
     * @return The Identifier and Item PKs, if found.
     */
    @Transactional(propagation = Propagation.SUPPORTS)
    fun findItemAndIdentifierPk(tokenized: Tokenized): IdentifierPkItemPk? =
        npTemplate.query(
            "select pk, item_pk from item_identifier where identifier_type_pk = :identifier_type_pk and suffix = :suffix",

            MapSqlParameterSource().apply {
                addValue(
                    "identifier_type_pk",
                    identifierTypeRegistry.resolvePrefix(tokenized.prefix),
                    Types.SMALLINT
                )
                addValue(
                    "suffix",
                    tokenized.suffix,
                    Types.VARCHAR
                )
            }

        ) { rs: ResultSet, _: Int ->
            IdentifierPkItemPk(identifierPk = rs.getLong("pk"), itemPk = rs.getLong("item_pk"))
        }.firstOrNull()

    /**
     * Create a new ItemIdentifier connected with the Item PK, and return the updated ItemIdentifier.
     */
    @Transactional(propagation = Propagation.SUPPORTS)
    fun createItemIdentifier(itemPk: Long, tokenized: Tokenized): Long {

        val keyHolder = GeneratedKeyHolder()

        val paramSource = MapSqlParameterSource(
            mapOf(
                "identifier_type_pk" to identifierTypeRegistry.resolvePrefix(tokenized.prefix),
                "suffix" to tokenized.suffix,
                "item_pk" to itemPk
            )
        )

        npTemplate.update(
            "INSERT INTO item_identifier (identifier_type_pk, suffix, item_pk) VALUES (:identifier_type_pk, :suffix, :item_pk)",
            paramSource,
            keyHolder,
            arrayOf("pk")
        )

        // If this returns a null there was a problem creating the row, which is a showstopper.
        return keyHolder.key!!.toLong()
    }


    /**
     * For a collection of Identifier values, return a map that can be used to resolve them.
     * Read only, so will retrieve only those Identifiers already known. Those not yet known are not returned.
     * @return map of pairs of tokenized Identifier PK and corresponding Item PK.
     */
    fun getIdentifierMappingsRO(
        identifiers: Collection<Tokenized>
    ): Map<Tokenized, IdentifierPkItemPk> = if (identifiers.isEmpty()) {
        // Special case, the SQL templating doesn't like an empty list.
        emptyMap()
    } else {
        // Start with a with the all keys present but unresolved ItemIdentifiers.
        val blank: MutableMap<Tokenized, IdentifierPkItemPk> = mutableMapOf()


        // The number of identifiers can vary, which means that the SQL 'IN' clause would be templated to a sequence
        // of random lengths. Very bad for query cache. Also, given the input length is arbitrary, a potential vector
        // for a pathologically large SQL query.
        // So batching them up into batches of size N means that there is only a maximum of N permutations of the query.
        // And for large batch sizes, we still get the benefit of querying a large number of ItemIdentifiers in a
        // reduced number of SQL queries.
        val batches = identifiers.chunked(10)
        for (batch in batches) {
            // Look up those we could find.
            // Note that :values is a list of tuples.
            val found =
                npTemplate.query(
                    "select pk, identifier_type_pk, suffix, item_pk from item_identifier where (identifier_type_pk, suffix) in (:values)",
                    mapOf("values" to batch.map {
                        arrayOf(identifierTypeRegistry.resolvePrefix(it.prefix), it.suffix)
                    })
                ) { rs, _ ->
                    val prefixVal = identifierTypeRegistry.reversePrefix(rs.getInt("identifier_type_pk"))
                    val tokenized = Tokenized(prefixVal, rs.getString("suffix"))
                    val itemIdentifierPk = rs.getLong("pk")
                    val itemPk = rs.getLong("item_pk")

                    tokenized to IdentifierPkItemPk(itemIdentifierPk, itemPk)

                }.toMap()

            // And replace them.
            blank.putAll(found)
        }

        blank
    }

    /**
     * For a set of ItemPks, retrieve a mapping of each Item PK to its Item PK and tokenized pair.
     */
    fun fetchItemIdentifierPairsForItemPks(itemPks: Set<Long>): Collection<Pair<Long, Tokenized>> =
        // Need to guard, Postgres doesn't like an empty list.
        if (itemPks.isEmpty()) {
            emptyList()
        } else {
            npTemplate.query(
                "select pk, identifier_type_pk, suffix, item_pk from item_identifier where item_pk in (:item_pks)",
                mapOf("item_pks" to itemPks)
            ) { rs, _ ->
                Pair(
                    rs.getLong("item_pk"),
                    Tokenized(
                        identifierTypeRegistry.reversePrefix(rs.getInt("identifier_type_pk")),
                        rs.getString("suffix")
                    )
                )
            }
        }

    /**
     * This adds a batch of identifiers. That may be a large batch in bulk or a small batch in a single item tree.
     * Either way we don’t need to do error handling row-by-row because:
     * When this is run in bulk there’s no constraint so we can add a large number in bulk and know that none will fail.
     * When this is run in normal ingestion, an envelope-batch worth will be done in a transaction.
     * In that transaction, already-extant identifiers will have been filtered out so we shouldn’t get duplicates.
     * If there was a duplicate (e.g. due to a transaction race) then the whole transaction would be retried anyway.
     */
    fun insertIdentifierBatch(
        identifiers: Collection<Tokenized>
    ) {

        // SQL params don't like empty lists.
        if (identifiers.isEmpty()) {
            return
        }

        val totalCount = identifiers.count()

        logger.info("Buffer $totalCount identifiers...")

        // Each batch makes one SQL statement.
        // At average 40 bytes per identifier suffix, a chunk of 1,000 adds 400 KB to the SQL statement.
        // In a test of 1 million identifiers, a chunk size of 10,000 results in ingestion at 16,000 identifiers per
        // second. Size 1000 gives the same figure. Batch size 100 gives 15,000 per second.

        val itemParamSource = MapSqlParameterSource().apply {
            addValue(
                "created_at",
                OffsetDateTime.now(),
                Types.TIMESTAMP_WITH_TIMEZONE
            )
            addValue("num", totalCount, Types.INTEGER)
        }


        val newItemPks =
            npTemplate.query(
                "INSERT INTO item(created_at) \n" +
                        "SELECT created_at FROM GENERATE_SERIES(0, :num), (SELECT :created_at ::date AS created_at) b\n" +
                        "RETURNING pk\n",
                itemParamSource
            ) { rs, _ ->
                rs.getLong("pk")
            }

        // Each new Identifier combined with its Item.
        val combined = identifiers.zip(newItemPks)

        // Each chunk maps to a SQL insert statement. We want them to be large for performance, but not too large.
        // For UniXML data, at 10,000 the average size is around 400,000
        // At 1,000 the size is 40,000 which is more reasonable.
        val combinedChunks = combined.chunked(1000)

        // Avoid divide by zero.
        val numChunks = combinedChunks.count()
        if (numChunks > 0) {
            val averageSize = combinedChunks.sumOf { batch ->
                batch.sumOf {
                    // Account 4 bytes for Long to store the prefix.
                    // This figure is only used for logging, but for large batches, this could be a
                    // significant chunk of memory.
                    it.first.suffix.length + 4
                }
            } / numChunks
            logger.info("Insert $totalCount identifiers in $numChunks chunks. Average size of chunk: $averageSize characters")
        }

        // Splits large-ish batches, creating a corresponding Item for each, then zips the new Item PKs with Identifiers
        // in code.
        // This can achieve 10,000 to 20,000 insertions per second. More exotic approaches, such as a single Common
        // Table Expression or a temporary unlogged table, don't improve on that.
        for (chunk in combinedChunks) {

            val identifierParamSource = MapSqlParameterSource().apply {
                addValue("values", chunk.map { (identifier, itemPk) ->
                    arrayOf(identifierTypeRegistry.resolvePrefix(identifier.prefix), identifier.suffix, itemPk)
                })
            }

            // Note that :values is a list of tuples.
            npTemplate.update(
                "INSERT INTO item_identifier(identifier_type_pk, suffix, item_pk) \n" +
                        "   VALUES :values",
                identifierParamSource
            )

        }
    }
}
