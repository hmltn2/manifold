package org.crossref.manifold.itemgraph

import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Repository

@Repository

/**
 * Maintain Collections and the Items they contain.
 */
class CollectionsDao(
    private val jdbcTemplate: JdbcTemplate,
) {
    private val npTemplate: NamedParameterJdbcTemplate = NamedParameterJdbcTemplate(jdbcTemplate)

    /**
     * What collections is this item in?
     */
    fun getCollectionsForItem(itemPk: Long, collectionRelationshipType: String): Set<Long> =
        npTemplate.query(
            """
            WITH
                collection_type_pk AS (SELECT pk FROM relationship_type WHERE VALUE = :relationship_type)
            SELECT to_item_pk 
            FROM relationship_assertion_current 
            WHERE from_item_pk = :item_pk AND relationship_type_pk = (SELECT pk FROM collection_type_pk)
            """,
            mapOf(
                "item_pk" to itemPk,
                "relationship_type" to collectionRelationshipType,
            ),
        ) { rs, _ -> rs.getLong("to_item_pk") }.toSet()

    /**
     * Given a set of ItemPks, return those that are a member of any Collection.
     */
    fun findCollectionItems(itemPks: Set<Long>, collectionRelationshipType: String) : Set<Long> =
        if (itemPks.isEmpty()) {
            // SQL can't cope with empty lists and the result would be empty in any case.
            emptySet()
        } else {
            npTemplate.query(
                """
                WITH
                    collection_type_pk AS (SELECT pk FROM relationship_type WHERE VALUE = :relationship_type)
                SELECT DISTINCT (from_item_pk) 
                    FROM relationship_assertion_current 
                WHERE from_item_pk IN (:item_pks) AND relationship_type_pk = (SELECT pk FROM collection_type_pk)
                """.trimIndent(),
                mapOf("item_pks" to itemPks, "relationship_type" to collectionRelationshipType)
            ) { rs, _ -> rs.getLong("from_item_pk") }.toSet()
        }

}