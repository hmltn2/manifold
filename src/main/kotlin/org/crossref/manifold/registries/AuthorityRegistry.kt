package org.crossref.manifold.registries

import com.fasterxml.jackson.databind.JsonNode
import org.crossref.manifold.itemgraph.Resolver
import org.crossref.manifold.itemtree.Identifier
import org.crossref.manifold.itemtree.Item
import org.crossref.manifold.util.loadJsonResource
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import javax.annotation.PostConstruct

/**
 * Registry of Parties that are considered authoritative, represented as resolved Identifiers.
 * Contains 'authorities', which are parties considered authoritative to make metadata assertions.
 * Also contains 'authority roots', which are parties considered authoritative to make authority assertions about parties.
 * See the 'perspectives' feature specification.
 *
 * The authorityRoots and authorities are not resolved to Items at boot time, resolution is deferred to query time.
 * To cache the item pks might end up with mappings out of sync with the database.
 */
@Service
class AuthorityRegistry(
    private val identifierTypeRegistry: IdentifierTypeRegistry,
    private val fileName: String = AUTHORITY_RESOURCE_FILENAME
) {
    private val logger: Logger = LoggerFactory.getLogger(this::class.java)

    /**
     * Set of Items that are authority roots.
     * These are stored as un-resolved, but they are tokenized.
     */
    private lateinit var authorityRoots: Set<Item>

    /**
     * Set of Items that are authorities.
     * These are stored as un-resolved, but they are tokenized.
     */
    private lateinit var authorities: Set<Item>

    private companion object {
        /**
         * The default resource filename for where the authority file should be found.
         */
        const val AUTHORITY_RESOURCE_FILENAME = "authority/authority.json"
    }

    /**
     * Return a list of Items from the named node in the JSON document.
     */
    fun loadItems(tree: JsonNode, key: String): Set<Item> {
        val entries = tree.findPath(key)
        if (entries.isEmpty) {
            val error = "Expected '$key' in authority file."
            logger.error(error)
            throw Exception(error)
        }

        return entries.map {
            val name = it.get("identifier")
            if (name == null) {
                val error = "Expected 'identifier' in authority file entry."
                logger.error(error)
                throw Exception(error)
            }

            Item().withIdentifier(identifierTypeRegistry.tokenize(Identifier(name.asText())))
        }.toSet()
    }

    fun isUriAuthorityRoot(uri: String): Boolean = authorityRoots.any { item ->
        item.identifiers.any { identifier ->
            identifier.uri == uri
        }
    }

    fun isUriAuthority(uri: String): Boolean = authorities.any { item ->
        item.identifiers.any { identifier ->
            identifier.uri == uri
        }
    }

    /**
     * Load the registry of authorities.
     */
    @PostConstruct
    fun load() {
        logger.info("Load authority file")

        val tree = loadJsonResource(fileName)

        this.authorityRoots = loadItems(tree, "authorityRoots")
        this.authorities = loadItems(tree, "authorities")
    }

    fun getAuthorityRootPks(resolver: Resolver): Set<Long> = authorityRoots.mapNotNull {
        resolver.resolveRW(it).pk
    }.toSet()

    fun getAuthorityPks(resolver: Resolver): Set<Long> = authorities.mapNotNull {
        resolver.resolveRW(it).pk
    }.toSet()
}
