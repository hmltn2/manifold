package org.crossref.manifold.api.relationship

import org.crossref.manifold.api.ApiMessage
import org.crossref.manifold.api.RelationshipView

class RelationshipsMessage(
    nextCursor: String?,
    totalResults: Int,
    itemsPerPage: Int,
    val relationships: List<RelationshipView>,
) : ApiMessage(nextCursor, totalResults, itemsPerPage) {
    constructor() : this(null, 0, 0, emptyList())
}
