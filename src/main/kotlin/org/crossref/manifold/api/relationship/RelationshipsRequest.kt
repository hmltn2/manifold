package org.crossref.manifold.api.relationship

import org.crossref.manifold.api.ApiRequest

class RelationshipsRequest(
    cursor: String?,
    rows: Int?,
    val itemId: String?,
    val subjectId: String?,
    val objectId: String?,
    val relationshipType: String?
) : ApiRequest(cursor, rows)
