package org.crossref.manifold.views

import org.crossref.manifold.itemtree.Tokenized
import org.crossref.manifold.modules.consts.IdentifierTypes
import org.crossref.manifold.registries.IdentifierTypeRegistry
import org.crossref.manifold.retrieval.IdentifierRetriever


/**
 * A set of Identifiers broken down by prefix (i.e. identifier type).
 * The resulting structure allows e.g. "x.doi", "x.orcid" etc.
 */
typealias ItemizedIdentifiers = Map<String, List<String>>

/** With a pre-primed [IdentifierRetriever], construct an ItemizedIdentifiers view object for the given Item PK.
 */
fun buildItemizedIdentifiers(
    itemPk: Long,
    identifierRetriever: IdentifierRetriever,
    identifierTypeRegistry: IdentifierTypeRegistry,
): ItemizedIdentifiers {
    // Include the implicit Item Identifier.
    val itemIdentifier =
        identifierTypeRegistry.tokenizedToIdentifier(Tokenized(IdentifierTypes.ITEM_PREFIX, itemPk.toString()), itemPk)

    val identifiers = identifierRetriever.getOrEmpty(itemPk) + itemIdentifier

    // Take the Identifiers and group them by their prefix
    return identifiers.groupBy(
        keySelector = {
            check(it.tokenized != null) { "Identifier was constructed without a tokenized component." }
            it.tokenized.prefix
        },
        valueTransform = { it.uri })
}
